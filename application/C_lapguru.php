<?php 

class C_lapguru extends CI_controller{

	public function __construct(){
		parent::__construct();
			// if($this->session->userdata('udhmasuk') != TRUE){
	  //           $url=base_url();
	  //           redirect($url);
	  //       }
			// elseif($this->session->userdata('ses_id') && $this->session->userdata('akses')=='2') {
			// 	redirect(base_url('guru/C_dashboard'));	
			// }
			$this->load->model('admin/M_dataguru');
			// $this->load->library('form_validation');
	}

	public function index(){
		$datatampilguru['datanya'] = $this->M_dataguru->query_tampilguru()->result();
		$this->load->view('laporan/laporan_guru',$datatampilguru);
	}
  
	public function input(){
		// $product = $this->product_model;
  //       $validation = $this->form_validation;
  //       $validation->set_rules($product->rules());

  //       if ($validation->run()) {
  //           $product->save();
  //           $this->session->set_flashdata('success', 'Berhasil disimpan');
  //       }
		$this->M_dataguru->tambah();
        $this->session->set_flashdata('sukses', 'Data Berhasil disimpan');
        redirect(base_url('admin/C_dataguru'));
	}

	public function edit($id = null){
        if (!isset($id)) redirect(base_url('admin/C_dataguru'));
       
        $model = $this->M_dataguru;
        

        $data["datanya"] = $modal->edit_data();	

    }

    public function update($id){
    	$validation = $this->form_validation;
        $validation->set_rules($model->rules());
        if (!$data["datanya"]) show_404();
        if ($validation->run()) {
            $modal->update();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
            redirect(base_url('admin/C_dataguru'));
        }
    }

    public function delete($id=null){
        if (!isset($id)) show_404();
        
        if ($this->M_dataguru->delete($id)) {
            redirect(base_url('admin/C_dataguru'));
        }
    }

	public function keluar(){
		session_destroy();
		redirect(base_url('C_login'));
	}

}
