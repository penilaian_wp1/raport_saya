<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Raport Saya | Siswa</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/font-awesome/css/font-awesome.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/Ionicons/css/ionicons.min.css">
     <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>dist/css/AdminLTEs.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>dist/css/skins/_all-skinsss.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<!-- ngasih animasi -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>dist/css/animate.css">

  <link rel="stylesheet" href="<?php echo base_url('assets/');?>skin/default.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link rel="shortcut icon" href="<?php echo base_url('assets/');?>images/favicon.png" />
</head>
<body class="hold-transition baner-5 skin-black sidebar-mini">
<div class="wrapper">

 <header class="main-header">  
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top fixed-top">
      <div class="imglogo-cont col-lg-6 mar-bot30">
         <a href="<?php echo base_url().'siswa/C_dashboard'?>"><img class="imglogo" src="<?php echo base_url('assets/');?>gambar/logo.png"></a>
         </div>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url('assets/');?>dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $this->session->userdata('ses_nama')?></span>
            </a>
            <ul class="dropdown-menu wow animated slideInDown" data-wow-offset="0" data-wow-delay="0">
              <!-- User image -->
              <li class="user-header wow animated fadeIn" data-wow-offset="0" data-wow-delay="0.1s">
                <img src="<?php echo base_url('assets/');?>dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p class="wow animated slideInUp" data-wow-offset="0" data-wow-delay="0.2s">
                  <?php echo $this->session->userdata('ses_nama')?>
                  <small>Member Sebagai Siswa</small>
                </p>
              <!-- Menu Body -->
             
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left wow animated fadeInRight" data-wow-offset="0" data-wow-delay="0.5s">
                  <a href="<?php echo base_url().'siswa/C_dashboard/profil'?>" class="btn btn-default btn-flat">Profil</a>
                </div>
                <div class="pull-right wow animated fadeInLeft" data-wow-offset="0" data-wow-delay="0.5s">
                  <a href="<?php echo base_url().'siswa/C_dashboard/keluar' ?>" class="btn btn-default btn-flat">Keluar</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <section class="header">
  <!-- Left side column. contains the logo and sidebar -->
<div class="baner text-center">
  <div class="baner-cont">
     <div class="wow animated jackInTheBox" data-wow-offset="0" data-wow-delay="0.3s">
      <h1 class="text-bold txt1"><span class="bg-green span1">SELAMAT</span><span class="bg-orange span1">DATANG</span></h1>
      </div>
     <h1 class="wow animated fadeInUp" data-wow-offset="0" data-wow-delay="0.6s"><?php echo $this->session->userdata('ses_nama')?></h1>
     <h3 class="wow animated fadeInDown" data-wow-offset="0" data-wow-delay="0.9s">NIS.<?php echo $this->session->userdata('ses_id')?></h3>

     <label class="wow animated fadeInRight text-light hh" data-wow-offset="0" data-wow-delay="1.2s"><i class="glyphicon glyphicon-envelope"></i> <?php echo $this->session->userdata('ses_email')?></label>
     <label class="wow animated fadeInLeft hh" data-wow-offset="0" data-wow-delay="1.2s"><i class="glyphicon glyphicon-phone"></i> <?php echo $this->session->userdata('ses_telp')?></label>
     <br>
      <form class="form-inline cont-btn-prof">
                  <div class="form-group" >
                    <a href="#lihat-nilai"><span class="btn wow animated rotateInUpRight line-btn light-blue btn-lg"data-wow-offset="0" data-wow-delay="1.5s"> <i class="fa fa-book"></i>Lihat Nilai</span></a>
                  </div>
                  <div class="form-group">
                    <a href=""><button class="btn wow animated rotateInUpLeft line-btn light-blue btn-lg"data-wow-offset="0" data-wow-delay="1.8s"> <i class="fa fa-save"></i> SIMPAN</button></a>
                  </div>
                </form>   
     </div>
                  
</div>
</section>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       <i class="fa fa-home"></i> Beranda
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url().'siswa/C_dashboard'?>"><i class="fa fa-home"></i>Beranda</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box box-success">
            <div class="box-header bg-orange text-center wow flipInX ">
              <h3 class="box-title section-heading wow animated fadeInUp" data-wow-offset="0" data-wow-delay="0.6s" data-animation="bounceInUp">Data Nilai Siswa</h3>
            </div>
            <div class="box-body">
                
            <section id="lihat-nilai">
              <div class="table-responsive no-padding section-heading wow animated fadeIn" data-wow-offset="0" data-wow-delay="0.9s">
              <table id="example1"class="table table-bordered table-hover">
               <thead> 
                <tr>            
                  <th>MATA KULIAH</th>
                  <th>TUGAS1</th>
                  <th>TUGAS2</th>
                  <th>TUGAS3</th>
                  <th>ULANGAN1</th>
                  <th>ULANGAN2</th>
                  <th>UTS</th>
                  <th>UAS</th>
                </tr>
              </thead>
                <tbody><tr>
                  <td>John Doe</td>
                  <td>89</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                
                </tr>
                <tr>
                  <td>Alexander Pierce</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                  <td>89</td>
                 
                </tr>
                <tr>
                  <td>Bob Doe</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                  <td>89</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                  
                 
                </tr>
                <tr>
                  <td>Mike Doe</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                  <td>89</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                 
                </tr>
                <tr>
                  <td>Mike Doe</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                  <td>89</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                 
                </tr>
                <tr>
                  <td>Mike Doe</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                  <td>89</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                 
                </tr>
                <tr>
                  <td>Mike Doe</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                  <td>89</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                 
                </tr>
                <tr>
                  <td>Mike Doe</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                  <td>89</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                 
                </tr>
                <tr>
                  <td>Mike Doe</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                  <td>89</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                 
                </tr>
                <tr>
                  <td>Mike Doe</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                  <td>89</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                 
                </tr>
                <tr>
                  <td>Mike Doe</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                  <td>89</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                 
                </tr>
                <tr>
                  <td>Mike Doe</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                  <td>89</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                 
                </tr>
                <tr>
                  <td>Mike Doe</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                  <td>89</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                 
                </tr>
                <tr>
                  <td>Mike Doe</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                  <td>89</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                 
                </tr>
                <tr>
                  <td>Mike Doe</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                  <td>89</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                 
                </tr>
                <tr>
                  <td>Mike Doe</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                  <td>89</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                 
                </tr>
                <tr>
                  <td>Mike Doe</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                  <td>89</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                 
                </tr>
                <tr>
                  <td>Mike Doe</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                  <td>89</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                 
                </tr>
                <tr>
                  <td>Mike Doe</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                  <td>89</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                 
                </tr>
                <tr>
                  <td>Mike Doe</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                  <td>89</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                 
                </tr>
                <tr>
                  <td>Mike Doe</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                  <td>89</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                 
                </tr>
                <tr>
                  <td>Mike Doe</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                  <td>89</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                 
                </tr>
                <tr>
                  <td>Mike Doe</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                  <td>89</td>
                  <td>89</td>
                  <td>90</td>
                  <td>89</td>
                 
                </tr>
                </tbody>
              </table>
            </div>
            </section>
                        <div class="form-group">
                 <button class="btn line-btn light submit-btn btn-block wow animated fadeInDown" data-wow-offset="0" data-wow-delay="1.6s"><i class="fa fa-print"></i> CETAK</button>
                </div>
            <!-- /.box-body -->
          </div>
        </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
            
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer bg-black">
     <div class="container text-center ">
      <br>
        <label class="wow animated fadeInRight hh" data-wow-offset="0" data-wow-delay="0.5s">
          <i class="fa fa-envelope"></i> raportsiswa@gmail.com</label>
        <label class="wow animated fadeInLeft hh" data-wow-offset="0" data-wow-delay="0.5s">
          <i <i class="fa fa-phone-square"></i> ( 0251-111-22-33 )</label>
      <br>
          <p class="wow animated fadeInUp hh" data-wow-offset="0" data-wow-delay="1s">&copy; Raport Saya</p>
          <p class="wow animated fadeInDown hh" data-wow-offset="0" data-wow-delay="1s">
            2018 </p>
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
     
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">


      </div>
  
      <div class="tab-pane" id="control-sidebar-settings-tab">
      
    
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<a href="#header" class="scrollup"><i class="fa fa-chevron-up"></i></a>
<!-- jQuery 3 -->
<script src="<?php echo base_url('assets/');?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url('assets/');?>bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assets/');?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url('assets/');?>bower_components/datatables.net/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url('assets/');?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url('assets/');?>bower_components/raphael/raphael.min.js"></script>
<script src="<?php echo base_url('assets/');?>bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('assets/');?>bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('assets/');?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url('assets/');?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('assets/');?>bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url('assets/');?>bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url('assets/');?>bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url('assets/');?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('assets/');?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url('assets/');?>bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/');?>bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/');?>dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url('assets/');?>dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assets/');?>dist/js/demo1.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>

  <script src="<?php echo base_url('assets/');?>js/responsive-slider.js"></script>
  <script src="<?php echo base_url('assets/');?>js/jquery.appear.js"></script>
  <script src="<?php echo base_url('assets/');?>js/grid.js"></script>
  <script src="<?php echo base_url('assets/');?>js/main.js"></script>
  <script src="<?php echo base_url('assets/');?>js/skrollr.min.js"></script>
  <script src="<?php echo base_url('assets/');?>js/jquery.scrollTo.js"></script>
  <script src="<?php echo base_url('assets/');?>js/jquery.localScroll.js"></script>
<script src="<?php echo base_url('assets/');?>dist/js/wow.min.js"></script>
  <script>
    wow = new WOW({}).init();
  </script>
</body>
</html>
