<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Raport Saya | Siswa</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/font-awesome/css/font-awesome.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/Ionicons/css/ionicons.min.css">
     <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>dist/css/AdminLTEs.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>dist/css/skins/_all-skinsss.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<!-- ngasih animasi -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>css/animate.css">

  <link rel="stylesheet" href="<?php echo base_url('assets/');?>skin/default.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link rel="shortcut icon" href="<?php echo base_url('assets/');?>gambar/favicon.png" />
</head>
<body class="hold-transition baner-5 skin-black sidebar-mini">
<div class="wrapper">

 <header class="main-header">  
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top fixed-top">
      <div class="imglogo-cont col-lg-6 mar-bot30">
         <a href="<?php echo base_url().'siswa/C_dashboard'?>"><img class="imglogo" src="<?php echo base_url('assets/');?>gambar/logo.png"></a>
         </div>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
             <b> <span class="hidden-xs"><i class="fa fa-sign-out"> </i>Keluar</span></b>
            </a>
            <ul class="dropdown-menu wow animated slideInDown" data-wow-offset="0" data-wow-delay="0">
              <!-- User image -->
              <li class="user-header wow animated fadeIn" data-wow-offset="0" data-wow-delay="0.3s">
               <p class="wow animated slideInUp" data-wow-offset="0" data-wow-delay="0.2s">
                <h3><span class="label label-warning">Klik Keluar, jika ingin keluar</span></h3>
                </p>
                <br>
                <div class="text-center wow animated fadeInUp" data-wow-offset="0" data-wow-delay="0.5s">
                  <a href="<?php echo base_url().'siswa/C_dashboard/keluar' ?>" class="btn line-btn light btn-flat">Keluar</a>
                </div>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <section class="header">
  <!-- Left side column. contains the logo and sidebar -->

</section>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <br><br>
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary section-heading wow animated fadeInUp" data-wow-offset="0" data-wow-delay="0.3s">
            <div class="box-body box-profile">
              <div class="wow animated bounceIn" data-wow-offset="0" data-wow-delay="0.6s">
              <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url('assets/');?>img/_MG_5447.jpg" alt="foto profile"data-toggle="tooltip" data-placement="right" title="Foto Profi">
              </div>
              <div class="wow animated bounceIn" data-wow-offset="0" data-wow-delay="0.9s">
              <h3 class="profile-username text-center"><?php echo $this->session->userdata('ses_nama')?></h3>
              </div>
              <div class="wow animated bounceIn" data-wow-offset="0" data-wow-delay="1.1s">
              <p class="text-muted text-center">NIS.<?php echo $this->session->userdata('ses_id'); ?></p>
            </div>
              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <div class="wow animated bounceIn" data-wow-offset="0" data-wow-delay="1.3s"><b>Kelas</b>
                  </div>
                   
                  <div class="wow animated bounceIn" data-wow-offset="0" data-wow-delay="1.3s"><?php echo $this->session->userdata('ses_kelas'); ?>
                  </div>
                </li>
              </ul>
              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <div class="wow animated bounceIn" data-wow-offset="0" data-wow-delay="1.3s"><b>Tahun Ajaran</b>
                  </div>
                   
                  <div class="wow animated bounceIn" data-wow-offset="0" data-wow-delay="1.3s"><?php echo $this->session->userdata('ses_thnajrn'); ?>
                  </div>
                </li>
              </ul>
            </div>

                
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary section-heading wow animated fadeInRight" data-wow-offset="0" data-wow-delay="0s">
            <div class="box-header with-border">
              <h3 class="box-title">Raport Saya</h3>
              <div class="box-tools">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body wow animated bounceIn" data-wow-offset="0" data-wow-delay="0.3s">
             <video class="video-responsive img-responsive" width="" height="" autoplay="" loop="" controls="" data-toggle="tooltip" data-placement="bottom" title="Raport Saya">
               <source src="<?php echo base_url('assets/img/main.mp4');?>" type="video/mp4">
                <source src="<?php echo base_url('assets/img/main.ogg');?>" type="video/ogg">
                 </video>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9 ">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active wow animated fadeInDown" data-wow-offset="0" data-wow-delay="0.4s" ><a href="#datanilai" data-toggle="tab">Data Nilai</a></li>
              <li class="wow animated fadeInDown" data-wow-offset="0" data-wow-delay="0.6s"><a href="#biodata" data-toggle="tab">Biodata</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="datanilai">
               <section id="lihat-nilai">
                 <div class="row mar-bot10">
                  <div class="col-md-offset-3 col-md-7 wow animated jackInTheBox"data-wow-offset="0" data-wow-delay="0s">
                <a href="#datanilai-2"><img class="img-responsive wow animated bounceIn" src="<?php echo base_url('assets/img/data_nilai.png') ?>" data-toggle="tooltip" data-placement="top" title="Lihat Data Nilai"></a>
                </div>
              </div>
              <section id="datanilai-2" class="appear">
              <br><hr><br>
               
              <div class="table-responsive no-padding section-heading wow animated fadeIn" data-wow-offset="0" data-wow-delay="0.3s">
              <table id="example1"class="table table-bordered table-hover">
               <thead> 
                <tr>            
                  <th><h4><span class="label label-primary" >MATA PELAJARAN</span></h3></th>
                  <th><h4><span class="label label-info" >TUGAS1</span></h4></th>
                  <th><h4><span class="label label-info" >TUGAS2</span></h4></th>
                  <th><h4><span class="label label-info" >TUGAS3</span></h4></th>
                  <th><h4><span class="label label-success" >ULANGAN1</span></h4></th>
                  <th><h4><span class="label label-success" >ULANGAN2</span></h4></th>
                  <th><h4><span class="label label-warning" >UTS</span></h4></th>
                  <th><h4><span class="label label-danger" >UAS</span></h4></th>
                  <th><h4><span class="label label-danger" >CETAK</span></h4></th>
                </tr>
              </thead>
                <tbody>
                  <?php foreach($datanya as $b) { ?>
                      <tr>
                        <td><?php echo $b->nm_mapel ?> </td> 
                        <td><?php echo $b->tugas1 ?> </td>
                        <td><?php echo $b->tugas2 ?> </td>
                        <td><?php echo $b->tugas3 ?></td>
                        <td><?php echo $b->ulangan1 ?></td>
                        <td><?php echo $b->ulangan2 ?></td>
                        <td><?php echo $b->uts ?></td>
                        <td><?php echo $b->uas ?></td>
                        <td><div class=" with-hover-text wow animated bounce"  data-wow-offset="0" data-wow-delay="0s" >
                      
                      <a href="<?php echo base_url('laporan/C_lapsiswa/lap_nilai_permapel/'.$b->kd_nilai) ?>" target="_blank"><i  class="btn btn-warning pensil icon icon-print" style="font-size: 20px;"></i></a>
                      </p>

                    </div></td>
                  
                  </tr>
                <?php } ?>
                </tbody>
              </table>
            </div>
            
            
            </section>
             <div class="form-group">
                 <a href="<?php echo base_url('laporan/C_lapsiswa') ?>" target="_blank"  class="btn line-btn light submit-btn btn-block wow animated fadeInDown" data-wow-offset="0" data-wow-delay="0s" data-toggle="tooltip" data-placement="top" title="Cetak Data Nilai"><i class="fa fa-print"></i> CETAK</a>
                </div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="biodata">
                 <div class="row mar-bot10">
                  <div class="col-md-offset-3 col-md-7 ">
            <a href="#profil"><img class="img-responsive wow animated bounceIn" src="<?php echo base_url('assets/img/biodata.png') ?>" data-toggle="tooltip" data-placement="top" title="Lihat Data Nilai"></a>
                </div>
              </div>
              <section id="profil" class="appear"></section>
              <br><hr><br>
               <div class="container">
      <div class="row mar-bot50">
        <div class="col-lg-4 col-md-offset-1">
          <div class="wow animated bounceIn" data-wow-offset="0" data-wow-delay="0.5s">
            <div class="text-center">

              <div class="wow rotateIn">
                <div class="service-col">
                  <div class="service-icon">
                    <figure><i class="fa fa-heart"></i></figure>
                  </div>
                  <h2>Agama</h2>
                  <p>Saya Beragama <?php echo $this->session->userdata('ses_agama')?></p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-4">
          <div class="align-center">
            <div class="wow animated bounceIn" data-wow-offset="0" data-wow-delay="0.7s">
              <div class="text-center">
              <div class="wow rotateIn">
                <div class="service-col">
                  <div class="service-icon">
                    <figure><i class="fa fa-genderless"></i></figure>
                  </div>
                  <h2>Jenis Kelamin</h2>
                  <p>Saya Seorang <?php echo $this->session->userdata('ses_jekel')?></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
       <div class="col-lg-4 col-md-offset-1">
          <div class="wow animated bounceIn" data-wow-offset="0" data-wow-delay="0.9s">
            <div class="text-center">

              <div class="wow rotateIn">
                <div class="service-col">
                  <div class="service-icon">
                    <figure><i class="fa fa-calendar"></i></figure>
                  </div>
                  <h2>Tanggal Lahir</h2>
                  <p>Saya Lahir Pada <?php echo $this->session->userdata('ses_tgl_lahir')?></p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-4">
          <div class="align-center">
            <div class="wow animated bounceIn" data-wow-offset="0" data-wow-delay="1.1s">
              <div class="text-center">
              <div class="wow rotateIn">
                <div class="service-col">
                  <div class="service-icon">
                    <figure><i class="fa fa-home"></i></figure>
                  </div>
                  <h2>Tempat Lahir</h2>
                  <p>Saya Lahir di <?php echo $this->session->userdata('ses_tmpt_lahir')?></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        <div class="col-lg-4 col-md-offset-1">
          <div class="wow animated bounceIn" data-wow-offset="0" data-wow-delay="0.9s">
            <div class="text-center">

              <div class="wow rotateIn">
                <div class="service-col">
                  <div class="service-icon">
                    <figure><i class="fa fa-phone"></i></figure>
                  </div>
                  <h2>Nomer Telp/hp</h2>
                  <p>Nomor Saya <?php echo $this->session->userdata('ses_telp')?></p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-4">
          <div class="align-center">
            <div class="wow animated bounceIn" data-wow-offset="0" data-wow-delay="1.1s">
              <div class="text-center">
              <div class="wow rotateIn">
                <div class="service-col">
                  <div class="service-icon">
                    <figure><i class="fa fa-envelope"></i></figure>
                  </div>
                  <h2>Email</h2>
                  <p>Email Saya <?php echo $this->session->userdata('ses_email')?></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>  
      <div class="col-lg-4 col-md-offset-1">
          <div class="wow animated bounceIn" data-wow-offset="0" data-wow-delay="1.3s">
            <div class="text-center">

              <div class="wow rotateIn">
                <div class="service-col">
                  <div class="service-icon">
                    <figure><i class="fa fa-map-marker"></i></figure>
                  </div>
                  <h2>Alamat</h2>
                  <p>Saya tinggal di <?php echo $this->session->userdata('ses_alamat')?></p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-4">
          <div class="align-center">
            <div class="wow animated bounceIn" data-wow-offset="0" data-wow-delay="1.6s">
              <div class="text-center">
              <div class="wow rotateIn">
                <div class="service-col">
                  <div class="service-icon">
                    <figure><i class="glyphicon glyphicon-ok-sign"></i></figure>
                  </div>
                  <h2>Status</h2>
                  <p>Saya masih <?php echo $this->session->userdata('ses_status')?> sebagai siswa</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> 
    </div>
             
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer bg-black">
     <div class="container text-center ">
      <br>
        <label class="wow animated fadeInRight hh" data-wow-offset="0" data-wow-delay="0.5s">
          <i class="fa fa-envelope"></i> raportsiswa@gmail.com</label>
        <label class="wow animated fadeInLeft hh" data-wow-offset="0" data-wow-delay="0.5s">
          <i <i class="fa fa-phone-square"></i> ( 0251-111-22-33 )</label>
      <br>
          <p class="wow animated fadeInUp hh" data-wow-offset="0" data-wow-delay="1s">&copy; Raport Saya</p>
          <p class="wow animated fadeInDown hh" data-wow-offset="0" data-wow-delay="1s">
            2018 </p>
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
     
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">


      </div>
  
      <div class="tab-pane" id="control-sidebar-settings-tab">
      
    
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

<a href="#header" class="scrollup"><i class="fa fa-chevron-up"></i></a>
</div>

<!-- ./wrapper -->
<!-- jQuery 3 -->
<script src="<?php echo base_url('assets/');?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url('assets/');?>bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assets/');?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url('assets/');?>bower_components/datatables.net/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url('assets/');?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url('assets/');?>bower_components/raphael/raphael.min.js"></script>
<script src="<?php echo base_url('assets/');?>bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('assets/');?>bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('assets/');?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url('assets/');?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('assets/');?>bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url('assets/');?>bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url('assets/');?>bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url('assets/');?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('assets/');?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url('assets/');?>bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/');?>bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/');?>dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url('assets/');?>dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assets/');?>dist/js/demo1.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>

  <script src="<?php echo base_url('assets/');?>js/responsive-slider.js"></script>
  <script src="<?php echo base_url('assets/');?>js/jquery.appear.js"></script>
  <script src="<?php echo base_url('assets/');?>js/grid.js"></script>
  <script src="<?php echo base_url('assets/');?>js/main.js"></script>
  <script src="<?php echo base_url('assets/');?>js/skrollr.min.js"></script>
  <script src="<?php echo base_url('assets/');?>js/jquery.scrollTo.js"></script>
  <script src="<?php echo base_url('assets/');?>js/jquery.localScroll.js"></script>
<script src="<?php echo base_url('assets/');?>dist/js/wow.min.js"></script>
  <script>
    wow = new WOW({}).init();
  </script>
</body>
</html>
