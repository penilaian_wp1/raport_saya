<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('admin/bagian/header'); ?>
</head>
<body class="hold-transition skin-blue fixed">
<div class="wrapper">

<?php $this->load->view('admin/bagian/navbar'); ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('admin/bagian/sidebar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Import data dari Excel
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="<?php echo base_url('C_importexcel/formguru') ?>">Import Data</a></li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header" align="center">
              <h1 class="box-title">Import Data Guru </h1>
            </div>
            <hr>
            <div class="invoice">
              <a href="<?php echo base_url("excel/Format_guru.xlsx"); ?>">Download Format</a><br>
              <!-- Buat sebuah tag form dan arahkan action nya ke controller ini lagi -->
              <form method="post" action="<?php echo base_url("C_importexcel/formguru"); ?>" enctype="multipart/form-data">
                <!-- 
                -- Buat sebuah input type file
                -- class pull-left berfungsi agar file input berada di sebelah kiri
                -->
                <input type="file"  name="file"><br>
                
                <!--
                -- BUat sebuah tombol submit untuk melakukan preview terlebih dahulu data yang akan di import
                -->
                <input type="submit" name="preview" value="Pratinjau">
              </form>
            </div>
              <!-- kondisi -->
              
            <?php
            if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form 
              if(isset($upload_error)){ // Jika proses upload gagal
                echo "<div style='color: red;'>".$upload_error."</div>";
                die;  // stop skrip
              } 
              // echo"<div class='row'> ";
              // Buat sebuah tag form untuk proses import data ke database
              echo "<form method='post' action='".base_url("C_importexcel/importguru")."'>";
              
              // Buat sebuah div untuk alert validasi kosong
              echo "<div style='color: red;' id='kosong'>
              Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
              </div>";
              
              echo "<table class='table table-bordered table-striped table-hover' id='table1' width='100%' cellspacing='0'>
              <tr>
                <th colspan='11'>Pratinjau Data</th>
              </tr>
              <tr>
                <th>NIP</th>
                <th>Nama</th>
                <th>TEMPAT LAHIR</th>
                <th>TANGGAL LAHIR</th>
                <th>JENIS KELAMIN</th>
                <th>AGAMA</th>
                <th>TELEPON</th>
                <th>EMAIL</th>
                <th>PASSWORD</th>
                <th>Alamat</th>
                <th>STATUS</th>
              </tr>";
              
              $numrow = 1;
              $kosong = 0;
              
              // Lakukan perulangan dari data yang ada di excel
              // $sheet adalah variabel yang dikirim dari controller
              foreach($sheet as $row){ 
                // Ambil data pada excel sesuai Kolom
                $nip = $row['A']; // Ambil data NIS
                $nama = $row['B']; // Ambil data nama
                $tempat_lahir = $row['C']; // Ambil data jenis kelamin
                $tanggal_lahir = $row['D']; // Ambil data alamat
                $jekel = $row['E']; // Ambil data alamat
                $agama = $row['F']; // Ambil data alamat
                $telp = $row['G']; // Ambil data alamat
                $email = $row['H']; // Ambil data alamat
                $password = $row['I']; // Ambil data alamat
                $alamat = $row['J']; // Ambil data alamat
                $status = $row['K']; // Ambil data alamat
                
                // Cek jika semua data tidak diisi
                if(empty($nip)
                  && empty($nama) 
                  && empty($tempat_lahir) 
                  && empty($tanggal_lahir) 
                  && empty($jekel)
                  && empty($agama)
                  && empty($telp)
                  && empty($email)
                  && empty($password)
                  && empty($alamat)
                  && empty($status)
                  )
                  continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)
                
                // Cek $numrow apakah lebih dari 1
                // Artinya karena baris pertama adalah nama-nama kolom
                // Jadi dilewat saja, tidak usah diimport
                if($numrow > 1){
                  // Validasi apakah semua data telah diisi
                  $nip_td = ( ! empty($nip))? "" : " style='background: #E07171;'"; // Jika NIS kosong, beri warna merah
                  $nama_td = ( ! empty($nama))? "" : " style='background: #E07171;'"; // Jika Nama kosong, beri warna merah
                  $tempatlahir_td = ( ! empty($tempat_lahir))? "" : " style='background: #E07171;'"; // Jika Jenis Kelamin kosong, beri warna merah
                  $tanggallahir_td = ( ! empty($tanggal_lahir))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
                  $jekel_td = ( ! empty($jekel))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
                  $agama_td = ( ! empty($agama))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
                  $telp_td = ( ! empty($telp))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
                  $email_td = ( ! empty($email))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
                  $password_td = ( ! empty($password))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
                  $alamat_td = ( ! empty($alamat))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
                  $status_td = ( ! empty($status))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
                  
                  // Jika salah satu data ada yang kosong
                  if(empty($nip) 
                    or empty($nama) 
                    or empty($tempat_lahir) 
                    or empty($tanggal_lahir)
                    or empty($jekel)
                    or empty($agama)
                    or empty($telp)
                    or empty($email)
                    or empty($password)
                    or empty($alamat)
                    or empty($status)){
                    $kosong++; // Tambah 1 variabel $kosong
                  }
                  
                  echo "<tr>";
                  echo "<td".$nip_td.">".$nip."</td>";
                  echo "<td".$nama_td.">".$nama."</td>";
                  echo "<td".$tempatlahir_td.">".$tempat_lahir."</td>";
                  echo "<td".$tanggallahir_td.">".$tanggal_lahir."</td>";
                  echo "<td".$jekel_td.">".$jekel."</td>";
                  echo "<td".$agama_td.">".$agama."</td>";
                  echo "<td".$telp_td.">".$telp."</td>";
                  echo "<td".$email_td.">".$email."</td>";
                  echo "<td".$password_td.">".$password."</td>";
                  echo "<td".$alamat_td.">".$alamat."</td>";
                  echo "<td".$status_td.">".$status."</td>";
                  echo "</tr>";
                }
                
                $numrow++; // Tambah 1 setiap kali looping
              }
              
              echo "</table>";
              
              // Cek apakah variabel kosong lebih dari 0
              // Jika lebih dari 0, berarti ada data yang masih kosong
              if($kosong > 0){
              ?>  
                <script>
                $(document).ready(function(){
                  // Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
                  $("#jumlah_kosong").html('<?php echo $kosong; ?>');
                  
                  $("#kosong").show(); // Munculkan alert validasi kosong
                });
                </script>
              <?php
              }else{ // Jika semua data sudah diisi
                echo "<hr>";
                echo "<div class='row no-print'>";
                echo "<div class='col-xs-12'>";
                // Buat sebuah tombol untuk mengimport data ke database
                echo"<button type='submit' class='btn btn-primary' data-placement='bottom' title='Import Data' data-toggle='tooltip'>IMPORT DATA</button>";
                // echo "<button class='btn btn-secondary' type='submit' name='import'>Import</button>";
                echo "<a href='".base_url("admin/C_dataguru")."' class='btn btn-secondary' type='button'>Cancel</a>";
                echo "</div><br>";
              }
              
              echo "</form>";
              // echo "</div>";
            }
            ?>
            </div>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('admin/bagian/footer'); ?>


</div>
<!-- ./wrapper -->

<?php $this->load->view('admin/bagian/modal'); ?>

<?php $this->load->view('admin/bagian/javascript'); ?>

</body>
</html>