<!DOCTYPE html>
<html>
<head>
  <!-- Header -->
  <?php $this->load->view('admin/bagian/header'); ?>
  <!-- /Header -->
</head>
<body class="hold-transition skin-blue fixed">
<div class="wrapper">

  <!-- Navbar -->
  <?php $this->load->view('admin/bagian/navbar'); ?>
  <!-- /Navbar -->

  <!-- Sidebar -->
  <?php $this->load->view('admin/bagian/sidebar'); ?>
  <!-- /Sidebar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Kelas Siswa
        <small>Kelola Data Kelas Siswa</small>
      </h1>
      <ol class="breadcrumb">
        <li><a><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active"><a href="<?= base_url('admin/C_kelassiswa'); ?>">Data Kelas Siswa</a></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <?php if ($this->session->flashdata('sukses')): ?>
        <div>
          <?php echo $this->session->flashdata('sukses'); ?>
        </div>
      <?php endif; ?>
      <?php if ($this->session->flashdata('notif')) { ?>
        <div>
          <?php echo $this->session->flashdata('notif'); ?>
        </div>
      <?php }?>
      <?php if ($this->session->flashdata('hapus')) { ?>
        <div>
          <?php echo $this->session->flashdata('hapus'); ?>
        </div>
      <?php }?>
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header" align="center">
              <h1 class="box-title">Tabel Data Kelas Siswa </h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
            	<table id="example2" class="table table-bordered table-hover">
              <table class="table table-bordered table-striped table-hover" id="table1" width="100%" cellspacing="0" >
                <thead>
                <tr align="text-center">
                  <th>KODE DATA KELAS</th>
                  <th>NAMA SISWA</th>
                  <th>NAMA KELAS</th>
                  <th>UBAH</th>
                  <th>HAPUS</th>
                </tr>
                </thead>
                <tbody>
                  <?php foreach($datanya as $u) {     ?>
                      <tr>
                        <td><?php echo $u->kd_datakelas ?></td>
                        <td><?php echo $u->nama_siswa ?></td>
                        <td><?php echo $u->nm_kelas ?> </td>
                        <td>
                          <button data-toggle="modal" class="btn btn-info glyphicon glyphicon-pencil edit" data-target="#edit<?php echo $u->kd_datakelas ?>" data-Toggle="modal"></button>
                           <!--  <?php echo anchor('admin/C_dataguru/edit/'.$u->nip,'<i class="btn btn-info glyphicon glyphicon-pencil" data-toggle="tooltip" data-placement="bottom" title="Edit data"></i>'); ?> -->
                        </td>
                            
                        <td>
                          <button data-toggle="modal" class="btn btn-danger glyphicon glyphicon-trash" data-target="#hapus<?php echo $u->kd_datakelas ?>" data-Toggle="modal"></button>
                          <!-- <?php echo anchor('admin/C_dataguru/hapus/'.$u->nip,'<i class="btn btn-danger glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="bottom" title="Hapus data"></i>'); ?> -->
                          <!-- <i class="btn btn-danger glyphicon glyphicon-trash"  data-toggle="modal" data-target="#modalhapus" data-placement="bottom" data-toggle="tooltip" title="Hapus data"></i> -->
                        </td>
                      <!-- Modaledit -->
                        <div class="modal modal-primary fade" id="edit<?php echo $u->kd_datakelas ?>">
                          <div class="modal-dialog modal-md">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title">Ubah Data Kelas Siswa</h3>
                              </div>
                              <div class="modal-body">
                              <!-- Form -->
                              <div>
                                <div class="box-header with-border">
                                </div>
                                <!-- /.box-header -->
                                    <!-- form start -->
                                    <form action="<?php echo base_url('admin/C_kelassiswa/update'); ?>" class="form-horizontal" method="POST" enctype="multipart/data-form">
                                      <div class="box-body">
                                        
                                        <div class="form-group">
                                          <label for="inputName" class="col-sm-2 control-label">NIS</label>
                                          <div class="col-sm-10">
                                            <input type="hidden" class="form-control" id="inputKdkelassiswa" name="KDDATAKELAS" value="<?php echo $u->kd_datakelas ?>" required>
                                            <input type="text" class="form-control"  id="inputNama" name="NAMA" required value="<?php echo $u->nis ?>">
                                          </div>
                                        </div><br><br>

                                        <div class="form-group">
                                          <label for="inputNis" class="col-sm-2 control-label">Kode Kelas</label>
                                          <div class="col-sm-4">
                                          <select type="text" class="form-control select2" style="width: 100%;" id="inputAgama" name="KDKELAS" required>
                                              <?php foreach($datakelas as $w) {?>
                                                <option><?php echo $w->kd_kelas ?></option>
                                              <?php }?>
                                              </select> 
                                          </div>
                                        </div><br><br>

                                      </div>
                                      <div class="box-header with-border">
                                      </div>
                                      <div class="modal-footer modal-primary">
                                        <button type="submit" class="btn btn-primary">Ubah</button>
                                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Batal</button>
                                      </div>
                                    </form>
                                    </div>
                              </div>
                              
                            </div>
                            <!-- /.modal-content -->
                          </div>
                          <!-- /.modal-dialog -->
                        </div>
                      <!-- /modal -->

                      <!-- Hapus Modal-->
                        <div class="modal modal-primary fade" id="hapus<?php echo $u->kd_datakelas ?>">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Peringatan Hapus</h4>
                              </div>
                              <?php echo form_open('admin/C_kelassiswa/hapus/'.$u->kd_datakelas); ?>
                              <div class="modal-body">
                                <p>Apakah anda yakin ingin Menghapus kelas siswa dengan kode <b><?php echo $u->kd_datakelas ?></b> ?</p>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Batal</button>
                                <!-- <?php echo anchor('admin/C_kelassiswa/hapus/'.$u->nip,'<a class="btn btn-outline" data-toggle="tooltip" data-placement="bottom" title="Hapus data">Hapus</a>'); ?> -->
                                <button type="submit" class="btn btn-outline" data-toggle="tooltip" data-placement="bottom" title="Hapus data">Hapus
                                </button>
                              </div>
                              <?php echo form_close(); ?>
                            </div>
                            <!-- /.modal-content -->
                          </div>
                          <!-- /.modal-dialog -->
                        </div>
                      <!-- /.modal -->
                      </tr>

                <?php } ?>
                  
                </tbody>
              </table><br>  
              <a class="btn btn-primary btn-block" data-placement="bottom" title="Menambahkan data kelas siswa" data-toggle="modal" data-target="#modalinputkelassiswa">TAMBAH DATA KELAS SISWA</a>
              <!-- <a class="btn btn-primary btn-block" data-placement="bottom" title="Menambahkan data kelas siswa" data-toggle="tooltip" href="<?php echo base_url('admin/C_kelassiswa/form'); ?>">TAMBAH DATA KELAS SISWA</a> -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>  
      <!-- /.row -->
    </section>
    
    <!-- /.content -->       
  </div>
  <!-- /.content-wrapper -->
  
  <!-- Footer -->
  <?php $this->load->view('admin/bagian/footer'); ?>
  <!-- /Footer -->
</div>
<!-- ./wrapper -->

<!-- Modal -->
<?php $this->load->view('admin/bagian/modal'); ?>
<!-- /Modal -->

<!-- Javascript -->
<?php $this->load->view('admin/bagian/javascript'); ?>
<!-- /Javascript -->

</body>
</html>
