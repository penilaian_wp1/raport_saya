<!DOCTYPE html>
<html>
<head>
  <!-- Header -->
  <?php $this->load->view('admin/bagian/header'); ?>
  <!-- /Header -->
</head>
<body class="hold-transition skin-blue fixed">
<div class="wrapper">

  <!-- Navbar -->
  <?php $this->load->view('admin/bagian/navbar'); ?>
  <!-- /Navbar -->

  <!-- Sidebar -->
  <?php $this->load->view('admin/bagian/sidebar'); ?>
  <!-- /Sidebar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Siswa
        <small>Kelola Data Siswa</small>
      </h1>
      <ol class="breadcrumb">
        <li><a><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active"><a href="<?= base_url('admin/C_datasiswa'); ?>">Data Siswa</a></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <?php if ($this->session->flashdata('sukses')): ?>
        <div>
          <?php echo $this->session->flashdata('sukses'); ?>
        </div>
      <?php endif; ?>
      <?php if ($this->session->flashdata('notif')) { ?>
        <div>
          <?php echo $this->session->flashdata('notif'); ?>
        </div>
      <?php }?>
      <?php if ($this->session->flashdata('hapus')) { ?>
        <div>
          <?php echo $this->session->flashdata('hapus'); ?>
        </div>
      <?php }?>
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header" align="center">
              <h1 class="box-title">Tabel Data Siswa </h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <a class="btn btn-primary" data-placement="bottom" title="Import data siswa dari excel" data-toggle="tooltip" href="<?php echo base_url('C_importexcel/formsiswa'); ?>">IMPORT DATA SISWA</a>
              <a class="btn btn-primary" data-placement="bottom" title="Menambahkan data siswa" data-toggle="modal" data-target="#modalinputsiswa">TAMBAH DATA SISWA</a>
            	<table id="example2" class="table table-bordered table-hover">
              <table class="table table-bordered table-striped table-hover" id="table1" width="100%" cellspacing="0" >
                <thead>
                <tr>
                  <th>FOTO</th>
                  <th>NIS</th>
                  <th>NAMA</th>
                  <th>JENIS KELAMIN</th>
                  <th>AGAMA</th>
                  <th>TEMPAT LAHIR</th>
                  <th>TANGGAL LAHIR</th>
                  <th>NO TELEPON</th>
                  <th>EMAIL</th>
                  <th>STATUS</th>
                  <th>ALAMAT</th>
                  <th>UBAH</th>
                  <th>HAPUS</th>
                </tr>
                </thead>
                <tbody>
                  <?php foreach($datanya as $u) {   $nis = $u->nis;      ?>
                      <tr>
                        <td><img src="<?php echo base_url('assets/dist/img/'.$u->foto) ?>" class="img-responsive"></td>
                        <td><?php echo $u->nis ?> </td> 
                        <td><?php echo $u->nama_siswa ?> </td>
                        <td><?php echo $u->jekel ?> </td>
                        <td><?php echo $u->agama ?></td>
                        <td><?php echo $u->tempat_lahir ?></td>
                        <td><?php echo $u->tanggal_lahir ?></td>
                        <td><?php echo $u->telp ?></td>
                        <td><?php echo $u->email ?></td>
                        <td><?php echo $u->status ?></td>
                        <td><?php echo $u->alamat ?></td>
                        <td>
                          <button data-toggle="modal" class="btn btn-info glyphicon glyphicon-pencil edit" data-target="#edit<?php echo $u->nis ?>" data-Toggle="modal"></button>
                           <!--  <?php echo anchor('admin/C_dataguru/edit/'.$u->nip,'<i class="btn btn-info glyphicon glyphicon-pencil" data-toggle="tooltip" data-placement="bottom" title="Edit data"></i>'); ?> -->
                        </td>
                            
                        <td>
                          <button data-toggle="modal" class="btn btn-danger glyphicon glyphicon-trash" data-target="#hapus<?php echo $u->nis ?>" data-Toggle="modal"></button>
                          <!-- <?php echo anchor('admin/C_dataguru/hapus/'.$u->nip,'<i class="btn btn-danger glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="bottom" title="Hapus data"></i>'); ?> -->
                          <!-- <i class="btn btn-danger glyphicon glyphicon-trash"  data-toggle="modal" data-target="#modalhapus" data-placement="bottom" data-toggle="tooltip" title="Hapus data"></i> -->
                        </td>
                      <!-- Modaledit -->
                        <div class="modal modal-primary fade" id="edit<?php echo $u->nis ?>">
                          <div class="modal-dialog modal-md">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title">Ubah Data Siswa</h3>
                              </div>
                              <div class="modal-body">
                              <!-- Form -->
                              <div>
                                <div class="box-header with-border">
                                </div>
                                <!-- /.box-header -->
                                    <!-- form start -->
                                    <form action="<?php echo base_url('admin/C_datasiswa/update'); ?>" class="form-horizontal" method="POST" enctype="multipart/form-data">
                                      <div class="box-body">
                                        
                                        <div class="form-group">
                                          <label for="inputName" class="col-sm-2 control-label">Nama</label>
                                          <div class="col-sm-10">
                                            <input type="hidden" class="form-control" id="inputNip" name="NIS" value="<?php echo $u->nis ?>" required>
                                            <input type="text" class="form-control" id="inputName" name="NAMA" placeholder="Nama" value="<?php echo $u->nama_siswa ?>"  required> 
                                          </div>
                                        </div><br><br>

                                        <div class="form-group">
                                          <label for="inputTmptlahir" class="col-sm-2 control-label">Tempat Lahir</label>
                                          <div class="col-sm-4">
                                            <input type="text" class="form-control" id="inputTmptlahir" name="TEMPATLAHIR" value="<?php echo $u->tempat_lahir ?>" required>
                                          </div>

                                          <label for="inputTgllahir" class="col-sm-2 control-label">Tanggal Lahir</label>
                                            <div class="col-sm-4">
                                              <div class="input-group date" >
                                                <input type="text" class="form-control pull-right" id="datepicker" name="TANGGALLAHIR" value="<?php echo $u->tanggal_lahir ?>" required>
                                              </div>
                                            </div>
                                        </div><br><br>

                                        <div class="form-group">
                                          <label for="inputAgama" class="col-sm-2 control-label">Agama</label>
                                            <div class="col-sm-4">
                                              <select type="text" class="form-control select2" style="width: 100%;" id="inputAgama" name="AGAMA" required value="<?php echo $u->agama ?>">
                                                <option <?php if($u->agama == 'Islam'){echo "selected='selected'";} ?>>ISLAM</option>
                                                <option <?php if($u->agama == 'Kristen'){echo "selected='selected'";} ?>>Kristen</option>
                                                <option <?php if($u->agama == 'Hindu'){echo "selected='selected'";} ?>>Hindu</option>
                                                <option <?php if($u->agama == 'Budha'){echo "selected='selected'";} ?>>Budha</option>
                                                <option <?php if($u->agama == 'Konghucu'){echo "selected='selected'";} ?>>Konghucu</option>
                                                <option <?php if($u->agama == 'Yahudi'){echo "selected='selected'";} ?>>Yahudi</option>
                                              </select> <br><br>
                                            </div>

                                          <label for="optionsRadios1" class="col-sm-2 control-label">Jenis Kelamin</label>
                                          <div class="col-sm-4">
                                            <div class="radio">
                                              <label>
                                                  <input type="radio" name="JEKEL" id="optionsRadios1" value="Laki-laki" <?php if($u->jekel == 'Laki-laki'){echo 'checked';} ?>>Laki-laki
                                              </label>
                                              <label>
                                                  <input type="radio" name="JEKEL" id="optionsRadios1" value="Perempuan" <?php if($u->jekel == 'Perempuan'){echo 'checked';} ?>>Perempuan
                                              </label>
                                            </div>
                                          </div>
                                        </div><br><br>

                                        <div class="form-group">
                                          <label for="inputTelepon" class="col-sm-2 control-label">Email</label>
                                            <div class="col-sm-6">
                                              <input type="email" class="form-control" id="inputEmail" name="EMAIL" value="<?php echo $u->email ?>" required>
                                            </div>
                                        </div><br><br>
                                        
                                        <div class="form-group">
                                          <label for="inputTelepon" class="col-sm-2 control-label">Telepon</label>
                                            <div class="col-sm-4">
                                              <input type="text" class="form-control" id="inputTelepon" name="TELEPON" value="<?php echo $u->telp ?>" required>
                                            </div>

                                          <label for="inputStatus" class="col-sm-2 control-label">Status</label>
                                            <div class="col-sm-4">
                                              <select type="text" class="form-control select2" style="width: 100%;" id="inputStatus" name="STATUS" required value="<?php echo $u->status ?>">
                                                <option <?php if($u->status == 'Aktif'){echo "selected='selected'";} ?>>Aktif</option>
                                                <option <?php if($u->status == 'Non Aktif'){echo "selected='selected'";} ?>>Non Aktif</option>
                                              </select> <br><br>
                                            </div>
                                        </div><br><br>

                                        <div class="form-group">
                                          <label for="GAMBAR" class="col-sm-2 control-label">Foto</label>
                                            <div class="col-sm-4">
                                              <input type="file" name="GAMBAR" />
                                              <input type="hidden" name="old_image" value="<?php echo $u->foto ?>" />
                                            </div>
                                        </div><br><br>

                                        <div class="form-group">
                                          <label for="inputAlamat" class="col-sm-2 control-label">Alamat</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" id="inputAlamat" name="ALAMAT" rows="3" placeholder="Masukan Alamat" required><?php echo $u->alamat ?></textarea>
                                            </div>
                                        </div>

                                      </div>
                                      <div class="box-header with-border">
                                      </div>
                                      <div class="modal-footer modal-primary">
                                        <button type="submit" class="btn btn-primary">Ubah</button>
                                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Batal</button>
                                      </div>
                                    </form>
                                    </div>
                              </div>
                              
                            </div>
                            <!-- /.modal-content -->
                          </div>
                          <!-- /.modal-dialog -->
                        </div>
                      <!-- /modal -->

                      <!-- Hapus Modal-->
                        <div class="modal modal-primary fade" id="hapus<?php echo $u->nis ?>">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Peringatan Hapus</h4>
                              </div>
                              <?php echo form_open('admin/C_datasiswa/hapus/'.$u->nis); ?>
                              <div class="modal-body">
                                <p>Apakah anda yakin ingin Menghapus data siswa <b><?php echo $u->nama ?></b> ?</p>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Batal</button>
                                <!-- <?php echo anchor('admin/C_dataguru/hapus/'.$u->nip,'<a class="btn btn-outline" data-toggle="tooltip" data-placement="bottom" title="Hapus data">Hapus</a>'); ?> -->
                                <button type="submit" class="btn btn-outline" data-toggle="tooltip" data-placement="bottom" title="Hapus data">Hapus
                                </button>
                              </div>
                              <?php echo form_close(); ?>
                            </div>
                            <!-- /.modal-content -->
                          </div>
                          <!-- /.modal-dialog -->
                        </div>
                      <!-- /.modal -->
                      </tr>

                <?php } ?>
                  
                </tbody>
              </table><br>  
              <!-- <a class="btn btn-primary btn-block" data-placement="bottom" title="Menambahkan data guru" data-toggle="modal" data-target="#modalinputsiswa">TAMBAH DATA SISWA</a> -->
              <!-- <a class="btn btn-primary btn-block" data-placement="bottom" title="Menambahkan data guru" data-toggle="tooltip" href="<?php echo base_url('admin/C_datasiswa/form'); ?>">TAMBAH DATA SISWA</a> -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>  
      <!-- /.row -->
    </section>
    
    <!-- /.content -->       
  </div>
  <!-- /.content-wrapper -->
  
  <!-- Footer -->
  <?php $this->load->view('admin/bagian/footer'); ?>
  <!-- /Footer -->
</div>
<!-- ./wrapper -->

<!-- Modal -->
<?php $this->load->view('admin/bagian/modal'); ?>
<!-- /Modal -->

<!-- Javascript -->
<?php $this->load->view('admin/bagian/javascript'); ?>
<!-- /Javascript -->

</body>
</html>
