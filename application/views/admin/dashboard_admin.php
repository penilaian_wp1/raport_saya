<!DOCTYPE html>
<html>
<head>
  <!-- Header -->
  <?php $this->load->view('admin/bagian/header'); ?>
  <!-- /header -->
</head>
<body class="hold-transition skin-blue fixed">
<div class="wrapper">

  <!-- Navbar -->
  <?php $this->load->view('admin/bagian/navbar'); ?>
  <!-- /Navbar -->

  <!-- Sidebar -->
  <?php $this->load->view('admin/bagian/sidebar'); ?>
  <!-- /Sidebar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Panel Kendali</small>
      </h1>
      <ol class="breadcrumb">
        <li><a><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active"><a href="<?= base_url('admin/C_dashboard'); ?>">Dashboard</a></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $total_guru ?></h3>

              <p>Jumlah Guru</p>
            </div>
            <div class="icon">
              <i class="ion ion-person"></i>
            </div>
            <a href="<?php echo base_url('admin/C_dataguru'); ?>" class="small-box-footer">Info Selengkapnya  <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $total_siswa ?></h3>

              <p>Jumlah Siswa</p>
            </div>
            <div class="icon">
              <i class="ion ion-person"></i>
            </div>
            <a href="<?php echo base_url('admin/C_datasiswa'); ?>" class="small-box-footer">Info Selengkapnya  <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $total_kelas ?></h3>

              <p>Jumlah Kelas</p>
            </div>
            <div class="icon">
              <i class="ion ion-edit"></i>
            </div>
            <a href="<?php echo base_url('admin/C_kelas'); ?>" class="small-box-footer">Info Selengkapnya  <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $total_mapel ?></h3>

              <p>Jumlah Mata Pelajaran</p>
            </div>
            <div class="icon">
              <i class="ion ion-star"></i>
            </div>
            <a href="<?php echo base_url('admin/C_mapel'); ?>" class="small-box-footer">Info Selengkapnya  <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-6 connectedSortable">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Data Guru</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-striped">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>NIP</th>
                  <th>NAMA GURU</th>
                  <th>ALAMAT</th>
                </tr>
                <?php $no=1; ?>
                <?php foreach($dataguru as $g) { ?>
                <tr>
                  <td><?php echo $no++ ?></td>
                  <td><?php echo $g->nip ?></td>
                  <td><?php echo $g->nama ?></td>
                  <td><?php echo $g->alamat ?></td>
                </tr>
                <?php } ?>
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <!-- <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">&raquo;</a></li>
              </ul> -->
            </div>
          </div>
          <!-- /.box -->

          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Data Mata Pelajaran</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-striped-bordered">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>KODE MAPEL</th>
                  <th>NAMA MAPEL</th>
                  <th>GURU PELAJARAN</th>
                </tr>
                <?php $no=1; ?>
                <?php foreach($datamapel as $g) { ?>
                <tr>
                  <td><?php echo $no++ ?></td>
                  <td><?php echo $g->kd_mapel ?></td>
                  <td><?php echo $g->nm_mapel ?></td>
                  <td><?php echo $g->nama ?></td>
                </tr>
                <?php } ?>
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <!-- <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">&raquo;</a></li>
              </ul> -->
            </div>
          </div>
          <!-- /.box -->
        </section>
        <!-- /.Left col -->

        <!-- right col -->
        <section class="col-lg-6 connectedSortable">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Data Siswa</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-hover">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>NIS</th>
                  <th>NAMA SISWA</th>
                  <th>ALAMAT</th>
                </tr>
                <?php $no=1; ?>
                <?php foreach($datasiswa as $g) { ?>
                <tr>
                  <td><?php echo $no++ ?></td>
                  <td><?php echo $g->nis ?></td>
                  <td><?php echo $g->nama_siswa ?></td>
                  <td><?php echo $g->alamat ?></td>
                </tr>
                <?php } ?>
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <!-- <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">&raquo;</a></li>
              </ul> -->
            </div>
          </div>
          <!-- /.box -->

          <div class="box box-secondary">
            <div class="box-header with-border">
              <h3 class="box-title">Data Kelas</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-responsive table-bordered">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>KODE KELAS</th>
                  <th>NAMA KELAS</th>
                  <th>NAMA WALI KELAS</th>
                </tr>
                <?php $no=1; ?>
                <?php foreach($datakelas as $g) { ?>
                <tr>
                  <td><?php echo $no++ ?></td>
                  <td><?php echo $g->kd_kelas ?></td>
                  <td><?php echo $g->nm_kelas ?></td>
                  <td><?php echo $g->nama ?></td>
                </tr>
                <?php } ?>
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <!-- <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">&raquo;</a></li>
              </ul> -->
            </div>
          </div>
          <!-- /.box -->
        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Footer -->
  <?php $this->load->view('admin/bagian/footer'); ?>
  <!-- /Footer -->
</div>
<!-- ./wrapper -->

<!-- Modal -->
  <?php $this->load->view('admin/bagian/modal'); ?>
<!-- /Modal -->

<!-- Javascript -->
<?php $this->load->view('admin/bagian/javascript'); ?>
<!-- /Javascript -->
</body>
</html>
