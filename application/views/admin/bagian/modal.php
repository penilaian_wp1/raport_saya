<!-- Logout Modal-->
<div class="modal modal-primary fade" id="modal-primary">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Peringatan Keluar</h4>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin ingin keluar dari Form admin&hellip; ?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Batal</button>
        <a class="btn btn-outline" href="<?php echo base_url('admin/C_dashboard/keluar'); ?>">Oke</a>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Input Modal Guru-->
<div class="modal modal-primary fade" id="modalinput">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Tambah Data Guru</h3>
      </div>
      <div class="modal-body">
      <!-- Form -->
      <div>
        <div class="box-header with-border">
          </div>
          <!-- /.box-header -->
            <!-- form start -->
            <form  class="form-horizontal" action="<?php echo base_url('admin/C_dataguru/input'); ?>" method="post" enctype="multipart/form-data">
              <div class="box-body">
                
                <div class="form-group">
                  <label for="inputName" class="col-sm-2 control-label">NIP</label>
                    <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputNip" name="NIP" placeholder="NIP" required >
                    </div>
                </div>

                <div class="form-group">
                  <label for="inputName" class="col-sm-2 control-label">Nama</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputName" name="NAMA" placeholder="Nama" required> 
                    </div>
                </div>

                <div class="form-group">
                  <label for="inputTmptlahir" class="col-sm-2 control-label">Tempat Lahir</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputTmptlahir" name="TEMPATLAHIR" placeholder="Tempat Lahir" required>
                  </div>

                  <label for="inputTgllahir" class="col-sm-2 control-label">Tanggal Lahir</label>
                    <div class="col-sm-4">
                      <div class="input-group date" >
                        <input type="text" class="form-control pull-right" id="datepicker" name="TANGGALLAHIR" placeholder="Tanggal Lahir" required>
                      </div>
                    </div>
                </div>

                <div class="form-group">
                  <label for="inputAgama" class="col-sm-2 control-label">Agama</label>
                    <div class="col-sm-4">
                      <select type="text" class="form-control select2" style="width: 100%;" id="inputAgama" name="AGAMA" required>
                        <option>ISLAM</option>
                        <option>Kristen</option>
                        <option>Hindu</option>
                        <option>Budha</option>
                        <option>Konghucu</option>
                        <option>Yahudi</option>
                      </select> 
                    </div>

                  <label for="optionsRadios1" class="col-sm-2 control-label">Jenis Kelamin</label>
                  <div class="col-sm-4">
                    <div class="radio">
                      <label>
                          <input type="radio" name="JEKEL" id="optionsRadios1" value="Laki-laki" required>Laki-laki
                      </label>
                      <label>
                          <input type="radio" name="JEKEL" id="optionsRadios1" value="Perempuan" required>Perempuan
                      </label>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputTelepon" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-4">
                      <input type="email" class="form-control" id="inputEmail" name="EMAIL" placeholder="Email" required>
                    </div>

                  <label for="inputTelepon" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-4">
                      <input type="password" class="form-control" id="inputPassword" name="PASSWORD" placeholder="Password" required>
                    </div>
                </div>
                
                <div class="form-group">
                  <label for="inputTelepon" class="col-sm-2 control-label">Telepon</label>
                    <div class="col-sm-4">
                      <input type="text" class="form-control" id="inputTelepon" name="TELEPON" placeholder="Telepon" required>
                    </div>

                  <label for="inputStatus" class="col-sm-2 control-label">Status</label>
                    <div class="col-sm-4">
                      <select type="text" class="form-control select2" style="width: 100%;" id="inputStatus" name="STATUS" required >
                        <option>Guru</option>
                        <option>Wali Kelas</option>
                      </select> 
                    </div>
                </div>

                <div class="form-group">
                  <label for="GAMBAR" class="col-sm-2 control-label">Foto</label>
                    <div class="col-sm-4">
                      <input type="file" name="GAMBAR" >
                    </div>
                </div>

                <div class="form-group">
                  <label for="inputAlamat" class="col-sm-2 control-label">Alamat</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" id="inputAlamat" name="ALAMAT" rows="3" placeholder="Masukan Alamat" required></textarea>
                    </div>
                </div>

              </div>
              <div class="box-header with-border">
              </div>
              <div class="modal-footer modal-primary">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Batal</button>
              </div>
            </form>
          </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<!-- /.modal -->

<!-- Input Modal Siswa-->
<div class="modal modal-primary fade" id="modalinputsiswa">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Tambah Data Siswa</h3>
      </div>
      <div class="modal-body">
      <!-- Form -->
      <div>
        <div class="box-header with-border">
        </div>
        <!-- /.box-header -->
            <!-- form start -->
            <form action="<?php echo base_url('admin/C_datasiswa/input'); ?>" class="form-horizontal" method="post" enctype="multipart/form-data">
              <div class="box-body">
                
              <div class="form-group">
                  <label for="inputName" class="col-sm-2 control-label">NIS</label>
                    <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputNip" name="NIS" placeholder="NIS" required >
                    </div>
                </div>

                <div class="form-group">
                  <label for="inputName" class="col-sm-2 control-label">Nama</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputName" name="NAMA" placeholder="Nama" required> 
                    </div>
                </div>

                <div class="form-group">
                  <label for="inputTmptlahir" class="col-sm-2 control-label">Tempat Lahir</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputTmptlahir" name="TEMPATLAHIR" placeholder="Tempat Lahir" required>
                  </div>

                  <label for="inputTgllahir" class="col-sm-2 control-label">Tanggal Lahir</label>
                    <div class="col-sm-4">
                      <div class="input-group date" >
                        <input type="text" class="form-control pull-right" id="datepicker" name="TANGGALLAHIR" placeholder="Tanggal Lahir" required>
                      </div>
                    </div>
                </div>

                <div class="form-group">
                  <label for="inputAgama" class="col-sm-2 control-label">Agama</label>
                    <div class="col-sm-4">
                      <select type="text" class="form-control select2" style="width: 100%;" id="inputAgama" name="AGAMA" required>
                        <option>ISLAM</option>
                        <option>Kristen</option>
                        <option>Hindu</option>
                        <option>Budha</option>
                        <option>Konghucu</option>
                        <option>Yahudi</option>
                      </select> 
                    </div>

                  <label for="optionsRadios1" class="col-sm-2 control-label">Jenis Kelamin</label>
                  <div class="col-sm-4">
                    <div class="radio">
                      <label>
                          <input type="radio" name="JEKEL" id="optionsRadios1" value="Laki-laki" required>Laki-laki
                      </label>
                      <label>
                          <input type="radio" name="JEKEL" id="optionsRadios1" value="Perempuan" required>Perempuan
                      </label>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputTelepon" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-4">
                      <input type="email" class="form-control" id="inputEmail" name="EMAIL" placeholder="Email" required>
                    </div>

                  <label for="inputTelepon" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-4">
                      <input type="password" class="form-control" id="inputPassword" name="PASSWORD" placeholder="Password" required>
                    </div>
                </div>
                
                <div class="form-group">
                  <label for="inputTelepon" class="col-sm-2 control-label">Telepon</label>
                    <div class="col-sm-4">
                      <input type="text" class="form-control" id="inputTelepon" name="TELEPON" placeholder="Telepon" required>
                    </div>

                  <label for="inputStatus" class="col-sm-2 control-label">Status</label>
                    <div class="col-sm-4">
                      <select type="text" class="form-control select2" style="width: 100%;" id="inputStatus" name="STATUS" required >
                        <option>Aktif</option>
                        <option>Non Aktif</option>
                      </select> 
                    </div>
                </div>

                <div class="form-group">
                  <label for="GAMBAR" class="col-sm-2 control-label">Foto</label>
                    <div class="col-sm-4">
                      <input type="file" name="GAMBAR" >
                    </div>
                </div>

                <div class="form-group">
                  <label for="inputAlamat" class="col-sm-2 control-label">Alamat</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" id="inputAlamat" name="ALAMAT" rows="3" placeholder="Masukan Alamat" required></textarea>
                    </div>
                </div>
                </div>

              </div>
              <div class="box-header with-border">
              </div>
              <div class="modal-footer modal-primary">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Batal</button>
              </div>
            </form>
            </div>
      </div>
      
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Input Modal Kelas -->
 <div class="modal modal-primary fade" id="modalinputkelas">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Tambah Data Kelas</h3>
      </div>
      <div class="modal-body">
      <!-- Form -->
      <div>
        <div class="box-header with-border">
        </div>
        <!-- /.box-header -->
            <!-- form start -->
            <form action="<?php echo base_url('admin/C_kelas/input'); ?>" class="form-horizontal" method="POST" enctype="multipart/data-form">
              <div class="box-body">
                
                <div class="form-group">
                  <label for="inputKdkelas" class="col-sm-2 control-label">Kode Kelas</label>
                    <div class="col-sm-4">
                      <input type="text" class="form-control" id="inputKdkelas" name="KDKELAS" placeholder="Kode Kelas" required>
                    </div>  

                  <label for="inputNmkelas" class="col-sm-2 control-label">Nama Kelas</label>
                    <div class="col-sm-4">
                      <input type="text" class="form-control" id="inputNamakelas" name="NMKELAS" placeholder="Nama Kelas" required> 
                    </div>
                </div>

                <div class="form-group">
                  <label for="inputWalikelas" class="col-sm-2 control-label">Nama Wali Kelas</label>
                    <div class="col-sm-6">
                      <select type="text" class="form-control select2" style="width: 100%;" id="inputAgama" name="NAMA" required>
                        <?php foreach($dataguru as $w) {?>
                          <option><?php echo $w->nip ?></option>
                        <?php }?>
                      </select>
                    </div>

                  <label for="inputThnajaran" class="col-sm-1  control-label">Tahun Ajaran</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control" id="inputTmptlahir" name="THNAJARAN" placeholder="Tahun Ajaran" required>
                  </div>
                </div><br><br>

                

              </div>
              <div class="box-header with-border">
              </div>
              <div class="modal-footer modal-primary">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Batal</button>
              </div>
            </form>
            </div>
      </div>
      
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /modal -->

<!-- Input Modal Mapel -->
<div class="modal modal-primary fade" id="modalinputmapel">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Tambah Data Mata Pelajaran</h3>
      </div>
      <div class="modal-body">
      <!-- Form -->
      <div>
        <div class="box-header with-border">
        </div>
        <!-- /.box-header -->
            <!-- form start -->
            <form action="<?php echo base_url('admin/C_mapel/input'); ?>" class="form-horizontal" method="POST" enctype="multipart/form-data">
              <div class="box-body">
                
                <div class="form-group">
                  <label for="inputName" class="col-sm-2 control-label">Kode Mapel</label>
                    <div class="col-sm-4">
                      <input type="text" class="form-control" id="inputKdmapel" name="KDMAPEL" placeholder="Kode Mapel" required>
                    </div>

                  <label for="inputNama" class="col-sm-2 control-label">Guru Pelajaran</label>
                    <div class="col-sm-4">
                      <select type="text" class="form-control select2" style="width: 100%;" id="inputAgama" name="NAMA" required>
                      <?php foreach($dataguru as $u) {?>
                        <option><?php echo $u->nip ?></option>
                      <?php }?>
                      </select> <br><br>
                    </div>
                </div>

                <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Nama Mapel</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputNmmapel" name="NMMAPEL" placeholder="Nama Mata Pelajaran"  required> 
                  </div>
                </div><br><br>

                

              </div>
              <div class="box-header with-border">
              </div>
              <div class="modal-footer modal-primary">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Batal</button>
              </div>
            </form>
            </div>
      </div>
      
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /modal -->

<!-- Input Modal Kelas siswa -->
<div class="modal modal-primary fade" id="modalinputkelassiswa">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Tambah Data Kelas Siswa</h3>
      </div>
      <div class="modal-body">
      <!-- Form -->
      <div>
        <div class="box-header with-border">
        </div>
        <!-- /.box-header -->
            <!-- form start -->
            <form action="<?php echo base_url('admin/C_kelassiswa/input'); ?>" class="form-horizontal" method="POST" enctype="multipart/form-data">
              <div class="box-body">
                
                <div class="form-group">
                  <label for="inputName" class="col-sm-2 control-label">Kode Data Kelas</label>
                    <div class="col-sm-4">
                      <input type="text" class="form-control" id="inputKdmapel" name="KDDATAKELAS" placeholder="Kode Data Kelas" required>
                    </div>

                  <label for="inputNama" class="col-sm-2 control-label">Nama Kelas</label>
                    <div class="col-sm-4">
                      <select type="text" class="form-control select2" style="width: 100%;" id="inputAgama" name="NMKELAS" required>
                      <?php foreach($datakelas as $u) {?>
                        <option><?php echo $u->kd_kelas ?></option>
                      <?php }?>
                      </select>
                    </div>
                </div>

                <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Nama Siswa</label>
                  <div class="col-sm-10">
                    <select type="text" class="form-control select2" style="width: 100%;" id="inputNama" name="NAMA" required>
                      <?php foreach($datasiswa as $u) {?>
                        <option><?php echo $u->nis ?></option>
                      <?php }?>
                    </select>
                  </div>
                </div><br><br>

                

              </div>
              <div class="box-header with-border">
              </div>
              <div class="modal-footer modal-primary">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Batal</button>
              </div>
            </form>
            </div>
      </div>
      
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /modal -->

<!-- Import Modal-->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="modal-import">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Import Data dari Excel</h4>
      </div>
      <div class="modal-body">
  <hr>
  
  <a href="<?php echo base_url("excel/format.xlsx"); ?>">Download Format</a>
  <br>
  <br>
  
  <!-- Buat sebuah tag form dan arahkan action nya ke controller ini lagi -->
  <form method="post" action="<?php echo base_url("admin/C_datasiswa/form"); ?>" enctype="multipart/form-data">
    <!-- 
    -- Buat sebuah input type file
    -- class pull-left berfungsi agar file input berada di sebelah kiri
    -->
    <input type="file" name="file">
    
    <!--
    -- BUat sebuah tombol submit untuk melakukan preview terlebih dahulu data yang akan di import
    -->
    <input type="submit" name="preview" value="Pratinjau">
  </form>
  
  <?php
  if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form 
    if(isset($upload_error)){ // Jika proses upload gagal
      echo "<div style='color: red;'>".$upload_error."</div>"; // Muncul pesan error upload
      die; // stop skrip
    }
    
    // Buat sebuah tag form untuk proses import data ke database
    echo "<form method='post' action='".base_url("admin/C_dataguru/import")."'>";
    
    // Buat sebuah div untuk alert validasi kosong
    echo "<div style='color: red;' id='kosong'>
    Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
    </div>";
    
    echo "<table border='1' cellpadding='8'>
    <tr>
      <th colspan='5'>Preview Data</th>
    </tr>
    <tr>
      <th>NIP</th>
      <th>Nama</th>
      <th>TEMPAT LAHIR</th>
      <th>TANGGAL LAHIR</th>
      <th>JENIS KELAMIN</th>
      <th>AGAMA</th>
      <th>TELEPON</th>
      <th>EMAIL</th>
      <th>PASSWORD</th>
      <th>Alamat</th>
      <th>STATUS</th>
      <th>FOTO</th>
    </tr>";
    
    $numrow = 1;
    $kosong = 0;
    
    // Lakukan perulangan dari data yang ada di excel
    // $sheet adalah variabel yang dikirim dari controller
    foreach($sheet as $row){ 
      // Ambil data pada excel sesuai Kolom
      $nis = $row['A']; // Ambil data NIS
      $nama = $row['B']; // Ambil data nama
      $tempat_lahir = $row['C']; // Ambil data jenis kelamin
      $tanggal_lahir = $row['D']; // Ambil data alamat
      $jekel = $row['E']; // Ambil data alamat
      $agama = $row['F']; // Ambil data alamat
      $telp = $row['G']; // Ambil data alamat
      $email = $row['H']; // Ambil data alamat
      $password = $row['I']; // Ambil data alamat
      $alamat = $row['J']; // Ambil data alamat
      $status = $row['K']; // Ambil data alamat
      $foto = $row['L']; // Ambil data alamat
      
      // Cek jika semua data tidak diisi
      if(empty($nis)
        && empty($nama) 
        && empty($tempat_lahir) 
        && empty($tanggal_lahir) 
        && empty($jekel)
        && empty($agama)
        && empty($telp)
        && empty($email)
        && empty($password)
        && empty($alamat)
        && empty($status)
        && empty($foto))
        continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)
      
      // Cek $numrow apakah lebih dari 1
      // Artinya karena baris pertama adalah nama-nama kolom
      // Jadi dilewat saja, tidak usah diimport
      if($numrow > 1){
        // Validasi apakah semua data telah diisi
        $nis_td = ( ! empty($nis))? "" : " style='background: #E07171;'"; // Jika NIS kosong, beri warna merah
        $nama_td = ( ! empty($nama))? "" : " style='background: #E07171;'"; // Jika Nama kosong, beri warna merah
        $tempatlahir_td = ( ! empty($tempat_lahir))? "" : " style='background: #E07171;'"; // Jika Jenis Kelamin kosong, beri warna merah
        $tanggallahir_td = ( ! empty($tanggal_lahir))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
        $jekel_td = ( ! empty($jekel))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
        $agama_td = ( ! empty($agama))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
        $telp_td = ( ! empty($telp))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
        $email_td = ( ! empty($email))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
        $password_td = ( ! empty($password))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
        $alamat_td = ( ! empty($alamat))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
        $status_td = ( ! empty($status))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
        $foto_td = ( ! empty($foto))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
        
        // Jika salah satu data ada yang kosong
        if(empty($nis) 
          or empty($nama) 
          or empty($tempat_lahir) 
          or empty($tanggal_lahir)
          or empty($jekel)
          or empty($agama)
          or empty($telp)
          or empty($email)
          or empty($password)
          or empty($alamat)
          or empty($status)
          or empty($foto)){
          $kosong++; // Tambah 1 variabel $kosong
        }
        
        echo "<tr>";
        echo "<td".$nis_td.">".$nis."</td>";
        echo "<td".$nama_td.">".$nama."</td>";
        echo "<td".$tempatlahir_td.">".$tempat_lahir."</td>";
        echo "<td".$tanggallahir_td.">".$tanggal_lahir."</td>";
        echo "<td".$jekel_td.">".$jekel."</td>";
        echo "<td".$agama_td.">".$agama."</td>";
        echo "<td".$telp_td.">".$telp."</td>";
        echo "<td".$email_td.">".$email."</td>";
        echo "<td".$password_td.">".$password."</td>";
        echo "<td".$alamat_td.">".$alamat."</td>";
        echo "<td".$status_td.">".$status."</td>";
        echo "<td".$foto_td.">".$foto."</td>";
        echo "</tr>";
      }
      
      $numrow++; // Tambah 1 setiap kali looping
    }
    
    echo "</table>";
    
    // Cek apakah variabel kosong lebih dari 0
    // Jika lebih dari 0, berarti ada data yang masih kosong
    if($kosong > 0){
    ?>  
      <script>
      $(document).ready(function(){
        // Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
        $("#jumlah_kosong").html('<?php echo $kosong; ?>');
        
        $("#kosong").show(); // Munculkan alert validasi kosong
      });
      </script>
    <?php
    }else{ // Jika semua data sudah diisi
      echo "<hr>";
      
      // Buat sebuah tombol untuk mengimport data ke database
      echo "<button type='submit' name='import'>Import</button>";
      echo "<a href='".base_url("admin/C_datasiswa")."'>Cancel</a>";
    }
    
    echo "</form>";
  }
  ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <a class="btn btn-primary" href="<?php echo base_url('admin/C_dashboard/keluar'); ?>">Oke</a>
      </div>
    </div>
  </div>
</div>