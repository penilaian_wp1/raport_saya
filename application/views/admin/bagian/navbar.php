<header class="main-header">
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
       <!-- Logo -->
      <a href="<?php echo base_url('admin/C_dashboard') ?>" class="logo">
        <!-- logo for regular state and mobile devices -->
        <b>Raport </b>Saya
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url('assets/');?>img/foto/<?php echo $this->session->userdata('ses_foto'); ?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $this->session->userdata('ses_nama'); ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url('assets/');?>img/foto/<?php echo $this->session->userdata('ses_foto'); ?>" class="img-circle" alt="User Image">

                <p>
                  <?php echo $this->session->userdata('ses_nama'); ?>
                  <small><?php echo $this->session->userdata('ses_telp'); ?></small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">  
                <div class="submit">    
                  <button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#modal-primary">
                    Keluar
                  </button>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>