<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"  align="center"><b>Sidebar Menu</b></li>
        <li class="<?php echo $this->uri->segment(2) == 'C_dashboard' ? 'active': '' ?>">
          <a href="<?php echo base_url('admin/C_dashboard'); ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
        <li class="<?php echo $this->uri->segment(2) == 'C_dataguru' ? 'active': '' ?>">
          <a href="<?php echo base_url('admin/C_dataguru'); ?>">
            <i class="fa fa-user"></i> <span>Data Guru</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
        <li class="<?php echo $this->uri->segment(2) == 'C_datasiswa' ? 'active': '' ?>">
          <a href="<?php echo base_url('admin/C_datasiswa'); ?>">
            <i class="fa fa-users"></i> <span>Data Siswa</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-university"></i> <span>Data Sekolah</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php echo $this->uri->segment(2) == 'C_mapel' ? 'active': '' ?>"><a href="<?php echo base_url('admin/C_mapel'); ?>"><i class="fa fa-edit"></i> Mata Pelajaran</a></li>
            <li class="<?php echo $this->uri->segment(2) == 'C_kelas' ? 'active': '' ?>"><a href="<?php echo base_url('admin/C_kelas'); ?>"><i class="fa fa-home"></i> Kelas</a></li>
            <li class="<?php echo $this->uri->segment(2) == 'C_kelassiswa' ? 'active': '' ?>"><a href="<?php echo base_url('admin/C_kelassiswa'); ?>"><i class="fa fa-user-circle"></i> Kelas Siswa</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-print"></i>
            <span>Cetak</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('C_lapguru'); ?>" target="_blank"><i class="fa fa-print"></i> Laporan Guru</a></li>
            <li><a href="<?php echo base_url('C_lapguru/laporansiswa'); ?>" target="_blank"><i class="fa fa-print"></i> Laporan Siswa</a></li>
            <li><a href="<?php echo base_url('C_lapguru/laporanpelajaran'); ?>" target="_blank"><i class="fa fa-print"></i> Laporan Mata Pelajaran</a></li>
            <li><a href="<?php echo base_url('C_lapguru/laporankelas'); ?>" target="_blank"><i class="fa fa-print"></i> Laporan Kelas</a></li>
            <li><a href="<?php echo base_url('C_lapguru/laporankelassiswa'); ?>" target="_blank"><i class="fa fa-print"></i> Laporan Kelas Siswa</a></li>
            <li><a href="<?php echo base_url('C_lapguru/laporanabsen'); ?>" target="_blank"><i class="fa fa-print"></i> Laporan Absen</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>