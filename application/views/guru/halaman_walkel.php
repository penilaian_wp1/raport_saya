<!doctype html>
<html>
<head lang="en">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	
	<title>Raport Saya - Wali Kelas</title> 
	<meta name="description" content="BlackTie.co - Free Handsome Bootstrap Themes" />	    
	<meta name="keywords" content="themes, bootstrap, free, templates, bootstrap 3, freebie,">
	<meta property="og:title" content="">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/');?>theme/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/');?>theme/fancybox/jquery.fancybox-v=2.1.5.css" type="text/css" media="screen">
    <link rel="stylesheet" href="<?php echo base_url('assets/');?>theme/css/font-awesome.min.css" rel="stylesheet">
	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/');?>theme/css/style.css">	
	
	<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,600,300,200&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
	
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>dist/css/animate.css">
	<link rel="shortcut icon" href="<?php echo base_url('assets/');?>gambar/favicon.png" />
		
</head>

<body>

	<div class="navbar navbar-fixed-top" data-activeslide="1">
		<div class="container">
		
			<!-- .navbar-toggle is used as the toggle for collapsed navbar content -->
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			
			
			<div class="nav-collapse collapse navbar-responsive-collapse">
				<ul class="nav row">
					<li data-slide="1" class="col-12 col-sm-2 wow animated fadeInDown" data-wow-offset="0" data-wow-delay="0s"><a id="menu-link-1" href="#slide-1" title="Beranda"><span class="icon icon-home"></span> <span class="text">BERANDA</span></a></li>

					<li data-slide="2" class="col-12 col-sm-2 wow animated fadeInDown" data-wow-offset="0" data-wow-delay="0.4s"><a id="menu-link-2" href="#slide-2" title="Data Nilai Siswa"><span class="icon icon-file"></span> <span class="text">DATA NILAI</span></a></li>

          <li data-slide="3" class="col-12 col-sm-2 wow animated fadeInDown" data-wow-offset="0" data-wow-delay="0.4s"><a id="menu-link-3" href="#slide-3" title="Data Nilai Siswa"><span class="icon icon-file"></span> <span class="text">DATA ABSEN</span></a></li>

          <li data-slide="4" class="col-12 col-sm-2 wow animated fadeInDown" data-wow-offset="0" data-wow-delay="0.4s"><a id="menu-link-4" href="#slide-4" title="Data Nilai Siswa"><span class="icon icon-file"></span> <span class="text">CETAK RAPORT</span></a></li>
					
					<li data-slide="5" class="col-12 col-sm-2 wow animated fadeInDown" data-wow-offset="0" data-wow-delay="0.6s"><a id="menu-link-5" href="#slide-5" title="Bantuan"><span class="icon icon-user"></span> <span class="text">RaportSaya</span></a></li>
				
					</li>
					<li class="bg-rd col-12 col-sm-2 wow animated fadeInDown" data-wow-offset="0" data-wow-delay="1s"><a href="" class="dropdown-toggle" data-toggle="dropdown" title="Keluar"><span class="icon icon-arrow-right"></span> <span class="text">KELUAR</span></a>
				<ul class="dropdown-menu wow animated slideInDown" data-wow-offset="0" data-wow-delay="0">
              		<!-- User image -->
              		<li class="user-header wow animated fadeIn" data-wow-offset="0" data-wow-delay="0.1s">
                  	<small>Yakin Ingin Keluar?</small>
                
              		</li>
              <!-- Menu Body -->
              <!-- Menu Footer-->
              		<li class="user-footer">
               		 <div class=" with-hover-text wow animated bounce" data-wow-offset="0" data-wow-delay="0s" >
					<p><a href="<?php echo base_url('guru/C_dashboard/keluar'); ?>"><i><img src="<?php echo base_url('assets/')?>theme/images/s01.png"></i></a></p>
					<span class="hover-text font-light ">Yakin</span>
				</div>
              </li>
            </ul>
					</li>
					</ul>
					<div class="row">
					<div class="col-sm-2 active-menu"></div>
				</div>
			</div><!-- /.nav-collapse -->
		</div><!-- /.container -->
	</div><!-- /.navbar -->
	
	<!-- === Arrows === -->
	<div id="arrows">
		<div id="arrow-up" class="disabled"></div>
		<div id="arrow-down"></div>
	</div><!-- /.arrows -->
	
		 

	<!-- === MAIN Background === -->
	<div class="slide story" id="slide-1" data-slide="1">
		<div class="container">
			<div id="home-row-1" class="row clearfix">
				<div class="col-12">
					<h1 class="font-semibold"> <span class="wow animated bounceIn" data-wow-offset="0" data-wow-delay="0s">RAPORT</span> <span class="font-thin wow animated bounceIn" data-wow-offset="0" data-wow-delay="0.2s">SAYA</span></h1>
					<h4 class="font-thin"><span class="wow animated bounceIn" data-wow-offset="0" data-wow-delay="0.4s">HALAMAN</span> <span class="font-semibold wow animated bounceIn" data-wow-offset="0" data-wow-delay="0.6s"> WALI KELAS</span> </h4>
					<?php foreach($walkel as $b) {?><td ><h4 class="font-semibold"><?php echo $b->nm_kelas ?></h4></td><?php }?>
					<br>
					<br>
				</div><!-- /col-12 -->
			</div><!-- /row -->
			<div id="home-row-2" class="row clearfix">
				
				<div class="col-12 col-sm-12 with-hover-text  wow animated jackInTheBox" data-wow-offset="0" data-wow-delay="0.2s" >
					<p><i><img title="CETAK RAPORT" class="home-hover navigation-slide" src="<?php echo base_url('assets/')?>theme/images/s05.png"data-slide="4"></i></p>
					<span class="hover-text font-light ">CETAK RAPORT SISWA</span>
				</div>
			
			</div><!-- /row -->
		</div><!-- /container -->
	</div><!-- /slide1 -->
<div>

	<!-- === Slide 2 === -->
	
	
	
	<!-- === Slide 4 - Process === -->
	<div class="slide story" id="slide-2" data-slide="2">
		<div class="container">
			<div class="row title-row">
				<div class="col-12 font-thin wow animated rotateInDownLeft" data-wow-offset="0" data-wow-delay="0.6s">Data Nilai <span class="font-semibold">Siswa</span></div>
			</div><!-- /row -->
			<div class="row line-row">
				<div class="hr wow animated rotateInDownRight" data-wow-offset="0" data-wow-delay="0.6s">&nbsp;</div>
			</div><!-- /row -->
			<div class="row subtitle-row">
				
                    <div class="box-body table-responsive no-padding section-heading wow animated fadeIn" data-wow-offset="0" data-wow-delay="1.2s">
              <table id="example1"class="table table-hover">
                <thead class="text-left"> 
                <tr>    
                  <th>NO</th>
                  <th>NIS</th>
                  <th>NAMA</th>
                  <th>KELAS</th>
                  <th>MATA PELAJARAN</th>
                  <th>TUGAS 1</th>
                  <th>TUGAS 2</th>
                  <th>TUGAS 3</th>
                  <th>ULANGAN 1</th>
                  <th>ULANGAN 2</th>
                  <th>UTS</th>
                  <th>UAS</th>
                  <th>UBAH</th>
                  <th>CETAK</th>
                </tr>
              </thead>
                <tbody class="text-left"><?php $no=1; ?>
                  <?php foreach($datanya as $b) { 

                    ?>
                    <tr>

                  <td><?php echo $no++; ?></td>
                  <td><?php echo $b->nis ?></td>
                  <td><?php echo $b->nama_siswa ?></td>
                  <td><?php echo $b->nm_kelas ?></td>
                  <td><?php echo $b->nm_mapel ?></td>
                  <td><?php echo $b->tugas1 ?> </td>
                  <td><?php echo $b->tugas2 ?> </td>
                  <td><?php echo $b->tugas3 ?></td>
                  <td><?php echo $b->ulangan1 ?></td>
                  <td><?php echo $b->ulangan2 ?></td>
                  <td><?php echo $b->uts ?></td>
                  <td><?php echo $b->uas ?></td>
                  <td><div class=" with-hover-text wow animated bounce" data-wow-offset="0" data-wow-delay="0s" >
                  		<p>
                  			<?php echo anchor('guru/C_dashboard/edit_nilai_walkel/'.$b->kd_nilai,'<i class="btn btn-success pensil icon icon-pencil" style="font-size: 20px;"></i>'); ?>
                  			
                  		</p>
                  	</div></td>
                  	<td><div class=" with-hover-text wow animated bounce"  data-wow-offset="0" data-wow-delay="0s" >
                  		<p>
                  			<a href="<?php echo base_url('laporan/C_lapguru/lap_nilai_siswa_walkel_per/'.$b->kd_nilai) ?>" target="_blank"><i  class="btn btn-warning pensil icon icon-print" style="font-size: 20px;"></i></a>
                  		</p>
                  	</div></td>
                </tr>
                <?php 
                      
                    } ?>
                </tbody>
              </table>
              </div>

              <div class="col-sm-12 with-hover-text wow animated bounce" data-wow-offset="0" data-wow-delay="0.3s">
<br>		
<p><a title="Cetak Keseluruhan"class="btn btn-block btn-warning" target="_blank" href="<?php echo base_url('laporan/C_lapguru/lap_nilai_siswa_walkel') ?>">CETAK</a></p>
				</div>
			<!-- /row -->
		</div><!-- /container -->
	</div><!-- /slide4 -->
	</div>

<div class="slide story" id="slide-3" data-slide="3">
    <div class="container">
      <div class="row line-row">
        <section id="edit">
        <div class="row title-row">
        <div class="col-12 font-thin wow animated rotateInDownLeft" data-wow-offset="0" data-wow-delay="0.6s">Data <span class="font-semibold">Absensi</span></div>
      </div>
    </section>
        <div class="hr wow animated rotateInDownRight" data-wow-offset="0" data-wow-delay="0.6s">&nbsp;</div>
      </div>
      <div class="row subtitle-row ">
        <div class="col-sm-8 col-md-5 col-md-offset-2">
                    <div class="box-body table-responsive no-padding section-heading wow animated fadeIn" data-wow-offset="0" data-wow-delay="1.2s">
              <table id="example3"class="table table-hover">
                <thead> 
                <tr>    
                  <th>NO</th>        
                  <th>NIS</th>
                  <th>NAMA</th>
                  <th>IZIN</th>
                  <th>SAKIT</th>
                  <th>TANPA KETERANGAN</th>
                  <th>UBAH</th>
                  <th>CETAK</th>
                </tr>
              </thead>
                <tbody class="text-left"><?php $noo=1; ?>
                  <?php foreach($absen as $b) { ?>
                    <tr>

                  <td><?php echo $noo++; ?></td>
                  <td><?php echo $b->nis ?></td>
                  <td><?php echo $b->nama_siswa ?></td>
                  <th><?php echo $b->ijin ?></th>
                  <td><?php echo $b->sakit ?> </td>
                  <td><?php echo $b->tanpa_ket ?> </td>
                  <td>
                    <div class=" with-hover-text wow animated bounce" data-wow-offset="0" data-wow-delay="0s" >
                      
                        <a href="<?php echo base_url('guru/C_dashboard/walikelas/'.$b->kd_absen.'#edit') ?>"><i  class="btn btn-success pensil icon icon-pencil" style="font-size: 20px;"></i></a>

                    </div></td>
                    <td><div class=" with-hover-text wow animated bounce"  data-wow-offset="0" data-wow-delay="0s" >
                      <p>

                        <a href="<?php echo base_url('laporan/C_lapguru/lap_absen_persiswa/'.$b->kd_absen) ?>" target="_blank"><i  class="btn btn-warning pensil icon icon-print" style="font-size: 20px;"></i></a>
                      </p>
                      
                    </div></td>
                </tr>
                <?php } ?>
                </tbody>
              </table>
            <div class="col-12 col-sm-12 col-md-3 with-hover-text wow animated bounce" data-wow-offset="0" data-wow-delay="0.3s">
          <p><a title="Cetak Keseluruhan" class="btn btn-block btn-warning" target="_blank" href="<?php echo base_url('laporan/C_lapguru/lap_absen') ?>">CETAK</a></p>
          
        </div>
              </div>
             
            </div>

            <div class="col-sm-4 col-md-3 col-md-offset-2">
              <div class="box box-primary">
            
            <!-- /.box-header -->
          <?php echo form_open('guru/C_dashboard/edit_simpan_absen');?>
      <?php echo form_hidden('id',$this->uri->segment(4));?>
            
            <form role="form">
              <div class="box-body">

                <div class="form-group">

                  <label for="exampleInputPassword1" class=" wow animated bounceIn" data-wow-offset="0" data-wow-delay="0s">NIS</label>
                  <input type="text" class="form-control wow animated bounceIn" data-wow-offset="0" data-wow-delay="0.2s" name="nis" disabled  title="klik tombol ubah" placeholder="NIS" value="<?php echo $data['nis'] ?>">
                </div>
        
                <div class="form-group">
                  <label for="exampleInputPassword1"class=" wow animated bounceIn" data-wow-offset="0" data-wow-delay="0.4s">Nama Siswa</label>
                  <input type="text" class="form-control wow animated bounceIn" data-wow-offset="0" data-wow-delay="0.6s" name="nama" disabled title="klik tombol ubah" placeholder="Nama Siswa" value="<?php echo $data['nama_siswa'] ?>">
                </div>
                <div class="form-group">
                  <label for=""class=" wow animated bounceIn" data-wow-offset="0" data-wow-delay="0.8s">Izin</label>
                  <input type="text" name="ijin" class="form-control wow animated bounceIn" data-wow-offset="0" data-wow-delay="1s" id="izin"  placeholder="Jumlah Izin" value="<?php echo $data['ijin'] ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1"class=" wow animated bounceIn" data-wow-offset="0" data-wow-delay="1.2s">Sakit</label>
                  <input type="text" name="sakit" class="form-control wow animated bounceIn" data-wow-offset="0" data-wow-delay="1.4s" id="sakit" placeholder="Jumlah Sakit" value="<?php echo $data['sakit'] ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1"class=" wow animated bounceIn" data-wow-offset="0" data-wow-delay="1.6s">Tanpa Keterangan</label>
                  <input type="text" name="tanpa_ket" class="form-control wow animated bounceIn" data-wow-offset="0" data-wow-delay="1.8s" id="exampleInputPassword1" placeholder="Jumlah Tidak Masuk / Tanpa Keterangan" value="<?php echo $data['tanpa_ket'] ?>">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="col-12 col-sm-12 with-hover-text wow animated bounceIn" data-wow-offset="0" data-wow-delay="2s">
          <p><a><button class="btn btn-block btn-success" type="submit">Simpan</button></a></p>
         </form>    
       
        </div>
              
            
                     
        </div>   

   <?php echo form_close();?>
            </div>
        <!-- /col12 -->
        
          </div>
</div>
</div>

<div class="slide story" id="slide-4" data-slide="4">
    <div class="container">
     <div class="row line-row">
        <section id="edit">
        <div class="row title-row">
        <div class="col-12 font-thin wow animated rotateInDownLeft" data-wow-offset="0" data-wow-delay="0.6s">Cetak <span class="font-semibold">Raport</span></div>
      </div>
        <div class="hr wow animated rotateInDownRight" data-wow-offset="0" data-wow-delay="0.6s">&nbsp;</div>
      </div>
          <div class="row subtitle-row ">
           <div class="box-body table-responsive no-padding section-heading wow animated fadeIn" data-wow-offset="0" data-wow-delay="1.2s">
              <table id="example4"class="table table-hover">
                <thead> 
                <tr>  
                  <th>NO</th>          
                  <th>NIS</th>
                  <th>NAMA</th>
                  <th>CETAK</th>
                </tr>
              </thead>
                <tbody class="text-left"><?php $nooo=1; ?>
                  <?php foreach($cari as $b) { ?>
                    <tr>

                <td><?php echo $nooo++; ?></td>
                  <td><?php echo $b->nis ?></td>
                  <td><?php echo $b->nama_siswa ?></td>
                    <td><div class=" with-hover-text wow animated bounce"  data-wow-offset="0" data-wow-delay="0s" >
                      <p>

                        <a href="<?php echo base_url('laporan/C_lapguru/lap_nilai_siswa_walkel_persiswa/'.$b->nis) ?>" target="_blank"><i  class="btn btn-warning pensil icon icon-print" style="font-size: 20px;"></i></a>
                      </p>
                    </div></td>
                </tr>
                <?php } ?>
                </tbody>
              </table>
              </div>
            </div>
    </div><!-- /container -->
  </div>



	<div class="slide story" id="slide-5" data-slide="5">
		<div class="container">
			<div class="row title-row">
				<div class="col-12 font-light wow animated rotateInDownLeft" data-wow-offset="0" data-wow-delay="0.6s">Raport <span class="font-semibold">Saya</span></div>
			</div><!-- /row -->
			<div class="row line-row ">
				<div class="hr wow animated rotateInDownRight" data-wow-offset="0" data-wow-delay="0.6s">&nbsp;</div>
			</div><!-- /row -->
			<div class="row subtitle-row">
				<div class="col-sm-1 hidden-sm">&nbsp;</div>
				<div class="col-12 col-sm-10 font-light wow animated fadeInDown" data-wow-offset="0" data-wow-delay="1s">Jika Ada Kendala atau kesalahan hubungi :</div>
				<div class="col-sm-1 hidden-sm">&nbsp;</div>
			</div><!-- /row -->
			<div id="contact-row-4" class="row">
				<div class="col-sm-1 hidden-sm">&nbsp;</div>
				<div class="col-12 col-sm-2 with-hover-text wow animated bounce" data-wow-offset="0" data-wow-delay="0s">
					<p><a target="_blank" href="#"><i class="icon icon-phone"></i></a></p>
					<span class="hover-text font-light ">0251 111 22 333</span></a>
				</div><!-- /col12 -->
				<div class="col-12 col-sm-2 with-hover-text wow animated bounce" data-wow-offset="0" data-wow-delay="0.2s">
					<p><a target="_blank" href="#"><i class="icon icon-envelope"></i></a></p>
					<span class="hover-text font-light ">raportsaya@gmail.com</span></a>
				</div><!-- /col12 -->
				<div class="col-12 col-sm-2 with-hover-text wow animated bounce" data-wow-offset="0" data-wow-delay="0.4s">
					<p><a target="_blank" href="#"><i class="icon icon-home"></i></a></p>
					<span class="hover-text font-light ">UBSI<br>BOGOR</span></a>
				</div><!-- /col12 -->
				<div class="col-12 col-sm-2 with-hover-text wow animated bounce" data-wow-offset="0" data-wow-delay="0.6s">
					<p><a target="_blank" href="#"><i class="icon icon-facebook"></i></a></p>
					<span class="hover-text font-light ">facebook/raportsaya</span></a>
				</div><!-- /col12 -->
				<div class="col-12 col-sm-2 with-hover-text wow animated bounce" data-wow-offset="0" data-wow-delay="0.8s">
					<p><a target="_blank" href="#"><i class="icon icon-twitter"></i></a></p>
					<span class="hover-text font-light ">@Raportsaya</span></a>
				</div><!-- /col12 -->
				<div class="col-sm-1 hidden-sm">&nbsp;</div>
			</div><!-- /row -->
		</div><!-- /container -->
	</div><!-- /Slide 6 -->
</body>

	<!-- SCRIPTS -->
	<script src="<?php echo base_url('assets/');?>theme/js/html5shiv.js"></script>
	<script src="<?php echo base_url('assets/');?>theme/js/jquery-1.10.2.min.js"></script>
	<script src="<?php echo base_url('assets/');?>theme/js/jquery-migrate-1.2.1.min.js"></script>
	<script src="<?php echo base_url('assets/');?>theme/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url('assets/');?>theme/js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/');?>theme/fancybox/jquery.fancybox.pack-v=2.1.5.js"></script>
	<script src="<?php echo base_url('assets/');?>theme/js/script.js"></script>
	
	<!-- fancybox init -->
	<script>
	$(document).ready(function(e) {
		var lis = $('.nav > li');
		menu_focus( lis[0], 1 );
		
		$(".fancybox").fancybox({
			padding: 10,
			helpers: {
				overlay: {
					locked: false
				}
			}
		});
	
	});
	</script>
<script>
  $(function () {
    $('#example1').DataTable()

    $('#example3').DataTable()
    
    $('#example4').DataTable()
    
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
<script src="<?php echo base_url('assets/');?>dist/js/wow.min.js"></script>
  <script>
    wow = new WOW({}).init();
  </script>
<script src="<?php echo base_url('assets/');?>bower_components/datatables.net/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url('assets/');?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
</html>