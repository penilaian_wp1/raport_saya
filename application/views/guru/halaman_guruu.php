<!doctype html>
<html>
<head lang="en">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	
	<title>Raport Saya - Guru</title> 
	<meta name="description" content="BlackTie.co - Free Handsome Bootstrap Themes" />	    
	<meta name="keywords" content="themes, bootstrap, free, templates, bootstrap 3, freebie,">
	<meta property="og:title" content="">

  <link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/font-awesome/css/font-awesome.css">

  <link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/Ionicons/css/ionicons.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/');?>theme/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/');?>theme/fancybox/jquery.fancybox-v=2.1.5.css" type="text/css" media="screen">
    <link rel="stylesheet" href="<?php echo base_url('assets/');?>theme/css/font-awesome.min.css" rel="stylesheet">
	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/');?>theme/css/style.css">	
	
	<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,600,300,200&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
	
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>dist/css/animate.css">
	<link rel="shortcut icon" href="<?php echo base_url('assets/');?>gambar/favicon.png" />
</head>

<body>

	<div class="navbar navbar-fixed-top" data-activeslide="1">
		<div class="container">
		
			<!-- .navbar-toggle is used as the toggle for collapsed navbar content -->
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			
			
			<div class="nav-collapse collapse navbar-responsive-collapse">
				<ul class="nav row">
					<li data-slide="1" class="col-12 col-sm-2 wow animated fadeInDown" data-wow-offset="0" data-wow-delay="0s"><a id="menu-link-1" href="#slide-1" title="Beranda"><span class="icon icon-home"></span> <span class="text">BERANDA</span></a></li>
					
					<li data-slide="2" class="col-12 col-sm-2 wow animated fadeInDown" data-wow-offset="0" data-wow-delay="0.2s"><a id="menu-link-2" href="#slide-2" title="Data Diri Anda"><span class="icon icon-user"></span> <span class="text">BIODATA</span></a></li>
					
					<li data-slide="3" class="col-12 col-sm-2 wow animated fadeInDown" data-wow-offset="0" data-wow-delay="0.4s"><a id="menu-link-3" href="#slide-3" title="Data Nilai Siswa"><span class="icon icon-file"></span> <span class="text">DATA NILAI</span></a></li>
					
					<li data-slide="4" class="col-12 col-sm-2 wow animated fadeInDown" data-wow-offset="0" data-wow-delay="0.6s"><a id="menu-link-4" href="#slide-4" title="Bantuan"><span class="icon icon-user"></span> <span class="text">RaportSaya</span></a></li>
					
					<li  class="bg-rd col-12 col-sm-2  wow animated fadeInDown" data-wow-offset="0" data-wow-delay="0.8s"><a href="" title="Masuk ke halaman walikelas" class="dropdown-toggle" data-toggle="dropdown"><span class="icon icon-user"></span> <span class="text">WALIKELAS</span></a>
					<ul class="dropdown-menu wow animated slideInDown" data-wow-offset="0" data-wow-delay="0">
              		<!-- User image -->
              		<li class="user-header wow animated fadeIn" data-wow-offset="0" data-wow-delay="0.1s">
                  	<small>Ke Halaman Wali Kelas?</small>
                
              		</li>
              <!-- Menu Body -->
              <!-- Menu Footer-->
              		<li class="user-footer">
               		 <div class=" with-hover-text wow animated bounce" data-wow-offset="0" data-wow-delay="0s" >
					<p><a target="_blank" href="<?php echo base_url('guru/C_dashboard/walikelas'); ?>"><i><img src="<?php echo base_url('assets/')?>theme/images/s02.png"></i></a></p>
					<span class="hover-text font-light ">HALAMAN WALI KELAS</span>
				</div>
              </li>
            </ul>
					</li>
					<li  class="bg-rd col-12 col-sm-2 wow animated fadeInDown" data-wow-offset="0" data-wow-delay="1s"><a href="" class="dropdown-toggle" data-toggle="dropdown" title="Keluar"><span class="icon icon-arrow-right"></span> <span class="text">Keluar</span></a>
				<ul class="dropdown-menu wow animated slideInDown" data-wow-offset="0" data-wow-delay="0">
              		<!-- User image -->
              		<li class="user-header wow animated fadeIn" data-wow-offset="0" data-wow-delay="0.1s">
                  	<small>Yakin Ingin Keluar?</small>
                
              		</li>
              <!-- Menu Body -->
              <!-- Menu Footer-->
              		<li class="user-footer">
               		 <div class=" with-hover-text wow animated bounce" data-wow-offset="0" data-wow-delay="0s" >
					<p><a href="<?php echo base_url('guru/C_dashboard/keluar'); ?>"><i><img src="<?php echo base_url('assets/')?>theme/images/s01.png"></i></a></p>
					<span class="hover-text font-light ">Yakin</span>
				</div>
              </li>
            </ul>
					</li>
					</ul>
					<div class="row">
					<div class="col-sm-2 active-menu"></div>
				</div>
			</div><!-- /.nav-collapse -->
		</div><!-- /.container -->
	</div><!-- /.navbar -->
	
	<!-- === Arrows === -->
	<div id="arrows">
		<div id="arrow-up" class="disabled"></div>
		<div id="arrow-down"></div>
	</div><!-- /.arrows -->
	
		 

	<!-- === MAIN Background === -->
	<div class="slide story" id="slide-1" data-slide="1">
		<div class="container">
			<div id="home-row-1" class="row clearfix">
				<div class="col-12">
					<h1 class="font-semibold"> <span class="wow animated bounceIn" data-wow-offset="0" data-wow-delay="0s">RAPORT</span> <span class="font-thin wow animated bounceIn" data-wow-offset="0" data-wow-delay="0.2s">SAYA</span></h1>
					<h4 class="font-thin"><span class="wow animated bounceIn" data-wow-offset="0" data-wow-delay="0.4s">HALAMAN</span> <span class="font-semibold wow animated bounceIn" data-wow-offset="0" data-wow-delay="0.6s"> GURU</span> </h4>
					<br>
					<br>
				</div><!-- /col-12 -->
			</div><!-- /row -->
			<div id="home-row-2" class="row clearfix">
				<div class="col-12 col-sm-4 with-hover-text  navigation-slide wow animated jackInTheBox" data-wow-offset="0" data-wow-delay="0s" data-slide="2">
					<p><i><div class="home-hover"><img title="Lihat Biodata" src="<?php echo base_url('assets/')?>theme/images/s04.png"></div></i></p>
					<span class="hover-text font-light ">BIODATA</span>
				</div>
				<div class="col-12 col-sm-4 with-hover-text navigation-slide wow animated jackInTheBox" data-wow-offset="0" data-wow-delay="0.2s" data-slide="3">
					<p><i><div class="home-hover"><img title="Lihat Nilai Siswa" src="<?php echo base_url('assets/')?>theme/images/s03.png"></div></i></p>
					<span class="hover-text font-light ">DATA NILAI SISWA</span>
				</div>
				<div class="col-12 col-sm-4 with-hover-text wow animated jackInTheBox" data-wow-offset="0" data-wow-delay="0.4s" >
					<p><a target="_blank" href="<?php echo base_url('guru/C_dashboard/walikelas'); ?>"><i><div class="home-hover"><img title="Masuk Ke Halam Wali Kelas " src="<?php echo base_url('assets/')?>theme/images/s02.png"></div></i></a></p>
					<span class="hover-text font-light ">HALAMAN WALI KELAS</span>
				</div>
			</div><!-- /row -->
		</div><!-- /container -->
	</div><!-- /slide1 -->
<div>

	<!-- === Slide 2 === -->
	<div class="slide story" id="slide-2" data-slide="2">
		<div class="container">
			<div class="row title-row">
				<div class="team-member">
					<div class="profile-picture wow animated bounceIn" data-wow-offset="0" data-wow-delay="0s">
                <figure><img src="<?php echo base_url('assets/');?>img/foto/<?php echo $this->session->userdata('ses_foto')?>" alt=""></figure>
                </div>
              </div>
				<div class="col-12 font-thin wow animated fadeInUp" data-wow-offset="0" data-wow-delay="0.3s"><span class="font-semibold"><?php echo $this->session->userdata('ses_nama')?></span></div>
			</div><!-- /row -->
			<div class="row line-row">
				<div class="hr wow animated fadeIn" data-wow-offset="0" data-wow-delay="0s">&nbsp;</div>
			</div><!-- /row -->
			<div class="row subtitle-row ">
				<div class="col-12 font-thin wow animated fadeInDown" data-wow-offset="0" data-wow-delay="0.3s">NIP.<span class="font-semibold"><?php echo $this->session->userdata('ses_id')?></span></div>
			</div><!-- /row -->
			<div class="row content-row">
				<div class="col-12 col-lg-3 col-sm-6 wow animated bounceIn" data-wow-offset="0" data-wow-delay="0.5s">
					<p><i class="icon icon-heart	"></i></p>
					<h2 class="font-semibold">Agama</h2>
					<h4 class="font-thin">Saya beragama <span class="font-semibold"><?php echo $this->session->userdata('ses_agama')?></span>.</h4>
				</div><!-- /col12 -->
				<div class="col-12 col-lg-3 col-sm-6 wow animated bounceIn" data-wow-offset="0" data-wow-delay="0.7s">
					<p><i class="icon icon-calendar"></i></p>
					<h2 class="font-semibold">Tanggal Lahir</h2>
					<h4 class="font-thin">Saya Lahir pada tanggal <span class="font-semibold"><?php echo $this->session->userdata('ses_tgl_lahir')?></span>.</h4>
					</div><!-- /col12 -->
				<div class="col-12 col-lg-3 col-sm-6 wow animated bounceIn" data-wow-offset="0" data-wow-delay="0.9s">
					<p><i class="icon icon-home"></i></p>
					<h2 class="font-semibold">Tempat Lahir</h2>
					<h4 class="font-thin">Saya Lahir Di <span class="font-semibold"><?php echo $this->session->userdata('ses_tmpt_lahir')?></span>.</h4>
				</div><!-- /col12 -->
				<div class="col-12 col-lg-3 col-sm-6 wow animated bounceIn" data-wow-offset="0" data-wow-delay="1.1s">
					<p><i class="icon icon-pencil"></i></p>
					<h2 class="font-semibold">Mengajar</h2>
					<h4 class="font-thin">Saya meengajar <span class="font-semibold"> <?php foreach($mapel as $b) {?><td><?php echo $b->nm_mapel ?>, </td><?php }?></span></h4>
				</div><!-- /col12 -->
			</div><!-- /row -->
			<div class="row content-row">
				<div class="col-12 col-lg-3 col-sm-6 wow animated bounceIn" data-wow-offset="0" data-wow-delay="1.3s">
					<p><i class="icon icon-user"></i></p>
					<h2 class="font-semibold">Jenis Kelamin</h2>
					<h4 class="font-thin">Saya seorang <span class="font-semibold"><?php echo $this->session->userdata('ses_jekel')?></span>.</h4>
				</div><!-- /col12 -->
				<div class="col-12 col-lg-3 col-sm-6 wow animated bounceIn" data-wow-offset="0" data-wow-delay="1.5s">
					<p><i class="icon icon-map-marker"></i></p>
					<h2 class="font-semibold">Alamat</h2>
					<h4 class="font-thin">Saya tinggal di <span class="font-semibold"><?php echo $this->session->userdata('ses_alamat')?></span>.</h4>
					</div><!-- /col12 -->
				<div class="col-12 col-lg-3 col-sm-6 wow animated bounceIn" data-wow-offset="0" data-wow-delay="1.7s">
					<p><i class="icon icon-phone"></i></p>
					<h2 class="font-semibold">Nomer Telepon</h2>
					<h4 class="font-semibold"><?php echo $this->session->userdata('ses_telp')?></h4>
				</div><!-- /col12 -->
				<div class="col-12 col-lg-3 col-sm-6 wow animated bounceIn" data-wow-offset="0" data-wow-delay="1.9s">
					<p><i class="icon icon-ok-sign"></i></p>
					<h2 class="font-semibold">Status</h2>
					<h4 class="font-thin"><span class="font-semibold"><?php echo $this->session->userdata('ses_status')?> <?php foreach($walkel as $b) {?><td><?php echo $b->nm_kelas ?>, </td><?php }?></span></h4>
				</div><!-- /col12 -->
			</div><!-- /row -->
	
		</div><!-- /container -->
	</div><!-- /slide2 -->
	
	
	<!-- === Slide 4 - Process === -->
	<div class="slide story" id="slide-3" data-slide="3">
		<div class="container">
			<div class="row title-row">
				<div class="col-12 font-thin wow animated rotateInDownLeft" data-wow-offset="0" data-wow-delay="0.6s">Data Nilai <span class="font-semibold">Siswa</span></div>
			</div><!-- /row -->
			<div class="row line-row">
				<div class="hr wow animated rotateInDownRight" data-wow-offset="0" data-wow-delay="0.6s">&nbsp;</div>
			</div><!-- /row -->
			<div class="row subtitle-row">
				
                    <div class="box-body table-responsive no-padding section-heading wow animated fadeIn" data-wow-offset="0" data-wow-delay="1.2s">
              <table id="example1"class="table table-hover">
                <thead> 
                <tr>    
               		<th>NO</th>
                  <th>NIS</th>
                  <th>NAMA</th>
                  <th>Kelas</th>
                  <th>MATA PELAJARAN</th>
                  <th>TUGAS 1</th>
                  <th>TUGAS 2</th>
                  <th>TUGAS 3</th>
                  <th>ULANGAN 1</th>
                  <th>ULANGAN 2</th>
                  <th>UTS</th>
                  <th>UAS</th>
                  <th>UBAH</th>
                  <th>CETAK</th>
                </tr>
              </thead>
                <tbody class="text-left">
                	<?php $no=1; ?>
                  <?php foreach($datanya as $b) { ?>
                <tr>
                <td><?php echo $no++; ?></td>
                  <td><?php echo $b->nis ?></td>
                  <td><?php echo $b->nama_siswa ?></td>
                  <td><?php echo $b->nm_kelas ?></t>
                  <td><?php echo $b->nm_mapel ?></td>
                  <td><?php echo $b->tugas1 ?> </td>
                  <td><?php echo $b->tugas2 ?> </td>
                  <td><?php echo $b->tugas3 ?></td>
                  <td><?php echo $b->ulangan1 ?></td>
                  <td><?php echo $b->ulangan2 ?></td>
                  <td><?php echo $b->uts ?></td>
                  <td><?php echo $b->uas ?></td>
                  <td> 
                  	<div class=" with-hover-text wow animated bounce" data-wow-offset="0" data-wow-delay="0s" >
                  		<p>
                  			<?php echo anchor('guru/C_dashboard/edit_nilai/'.$b->kd_nilai,'<i class="btn btn-warning pensil icon icon-pencil" style="font-size: 20px;"></i>'); ?>
                  			
                  		</p>
                  	</div>
                  </td>
                  <td><div class=" with-hover-text wow animated bounce"  data-wow-offset="0" data-wow-delay="0s" >
                  		<a href="<?php echo base_url('laporan/C_lapguru/lap_nilai_persiswa/'.$b->kd_nilai) ?>" target="_blank"><i  class="btn btn-warning pensil icon icon-print" style="font-size: 20px;"></i></a>
                  		</p>

                  	</div></td>
                </tr>
                <?php } ?>
                </tbody>
              </table>
              </div>
            	
				<div class="col-12 col-sm-2 with-hover-text wow animated bounce" data-wow-offset="0" data-wow-delay="0s">
					<p><a target="_blank" href="<?php echo base_url('laporan/C_lapguru') ?>"><i class="icon icon-print"></i></a></p>
					<span class="hover-text font-light ">CETAK</span></a>
				</div><!-- /col12 -->
			<!-- /row -->
			
            <!-- /.modal-content -->
          </div>
      
		</div><!-- /container -->
	</div><!-- /slide4 -->
	</div>
	<!-- === Slide 5 === -->
	
	<!-- === Slide 6 / Contact === -->
	<div class="slide story" id="slide-4" data-slide="4">
		<div class="container">
			<div class="row title-row">
				<div class="col-12 font-light wow animated rotateInDownLeft" data-wow-offset="0" data-wow-delay="0.6s">Raport <span class="font-semibold">Saya</span></div>
			</div><!-- /row -->
			<div class="row line-row ">
				<div class="hr wow animated rotateInDownRight" data-wow-offset="0" data-wow-delay="0.6s">&nbsp;</div>
			</div><!-- /row -->
			<div class="row subtitle-row">
				<div class="col-sm-1 hidden-sm">&nbsp;</div>
				<div class="col-12 col-sm-10 font-light wow animated fadeInDown" data-wow-offset="0" data-wow-delay="1s">Jika Ada Kendala atau kesalahan hubungi :</div>
				<div class="col-sm-1 hidden-sm">&nbsp;</div>
			</div><!-- /row -->
			<div id="contact-row-4" class="row">
				<div class="col-sm-1 hidden-sm">&nbsp;</div>
				<div class="col-12 col-sm-2 with-hover-text wow animated bounce" data-wow-offset="0" data-wow-delay="0s">
					<p><i class="icon icon-phone"></i></p>
					<span class="hover-text font-light ">0251 111 22 333</span></a>
				</div><!-- /col12 -->
				<div class="col-12 col-sm-2 with-hover-text wow animated bounce" data-wow-offset="0" data-wow-delay="0.2s">
					<p><i class="icon icon-envelope"></i></p>
					<span class="hover-text font-light ">raportsaya@gmail.com</span></a>
				</div><!-- /col12 -->
				<div class="col-12 col-sm-2 with-hover-text wow animated bounce" data-wow-offset="0" data-wow-delay="0.4s">
					<p><i class="icon icon-home"></i></p>
					<span class="hover-text font-light ">UBSI<br>BOGOR</span></a>
				</div><!-- /col12 -->
				<div class="col-12 col-sm-2 with-hover-text wow animated bounce" data-wow-offset="0" data-wow-delay="0.6s">
					<p><i class="icon icon-facebook"></i></p>
					<span class="hover-text font-light ">facebook/raportsaya</span></a>
				</div><!-- /col12 -->
				<div class="col-12 col-sm-2 with-hover-text wow animated bounce" data-wow-offset="0" data-wow-delay="0.8s">
					<p><i class="icon icon-twitter"></i></p>
					<span class="hover-text font-light ">@Raportsaya</span></a>
				</div><!-- /col12 -->
				<div class="col-sm-1 hidden-sm">&nbsp;</div>
			</div><!-- /row -->
		</div><!-- /container -->
	</div><!-- /Slide 6 -->
	
</body>

	<!-- SCRIPTS -->
	<script src="<?php echo base_url('assets/');?>theme/js/html5shiv.js"></script>
	<script src="<?php echo base_url('assets/');?>theme/js/jquery-1.10.2.min.js"></script>
	<script src="<?php echo base_url('assets/');?>theme/js/jquery-migrate-1.2.1.min.js"></script>
	<script src="<?php echo base_url('assets/');?>theme/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url('assets/');?>theme/js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/');?>theme/fancybox/jquery.fancybox.pack-v=2.1.5.js"></script>
	<script src="<?php echo base_url('assets/');?>theme/js/script.js"></script>
	
	<!-- fancybox init -->
	<script>
	$(document).ready(function(e) {
		var lis = $('.nav > li');
		menu_focus( lis[0], 1 );
		
		$(".fancybox").fancybox({
			padding: 10,
			helpers: {
				overlay: {
					locked: false
				}
			}
		});
	
	});
	</script>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
<script src="<?php echo base_url('assets/');?>dist/js/wow.min.js"></script>
  <script>
    wow = new WOW({}).init();
  </script>
<script src="<?php echo base_url('assets/');?>bower_components/datatables.net/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url('assets/');?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
</html>