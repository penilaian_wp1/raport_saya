<!doctype html>
<html>
<head lang="en">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  
  <title>Raport Saya - Ubah Nilai Siswa</title> 
  <meta name="description" content="BlackTie.co - Free Handsome Bootstrap Themes" />      
  <meta name="keywords" content="themes, bootstrap, free, templates, bootstrap 3, freebie,">
  <meta property="og:title" content="">

   <link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/font-awesome/css/font-awesome.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/Ionicons/css/ionicons.min.css">
     <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>dist/css/AdminLTEs.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>dist/css/skins/_all-skinsss.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<!-- ngasih animasi -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>dist/css/animate.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 <link rel="shortcut icon" href="<?php echo base_url('assets/');?>gambar/favicon.png" />
    
</head>

<body class="bg-warning">

  <br><br>
    <div class="container">

<?php echo form_open('guru/C_dashboard/edit_simpan_nilai_walkel');?>
<?php echo form_hidden('id',$this->uri->segment(4));?>
      <div class="row subtitle-row ">
        
        <div class="col-sm-6 col-md-6 col-md-offset-3">
       
          <!-- Horizontal Form -->
          <div class="box box-warning wow animated fadeInDown" data-wow-offset="0" data-wow-delay="0s">

<form >
            
              <div class="box-body">
                 <div class="header font-thin wow animated fadeInUp" data-wow-offset="0" data-wow-delay="0.5s">
          <h1 class="label-warning text-center">Ubah Data Nilai Siswa</h1></div>
                <div class="form-group">
                  <label  class="col-sm-3 control-label wow animated fadeInDown" data-wow-offset="0" data-wow-delay="0.8s">NIS</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control wow animated fadeInUp" data-wow-offset="0" data-wow-delay="1.8s" disabled name="nis" placeholder="NIS" value="<?php echo $datanya['nis'] ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label  class="col-sm-3 control-label wow animated fadeInDown" data-wow-offset="0" data-wow-delay="1s">Nama Siswa</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control wow animated fadeInUp" data-wow-offset="0" data-wow-delay="1.6s" disabled name="nama" placeholder="Nama Siswa" value="<?php echo $datanya['nama_siswa']?>">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label wow animated fadeInDown" data-wow-offset="0" data-wow-delay="1.2s">Kelas</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control wow animated fadeInUp" data-wow-offset="0" data-wow-delay="1.4s" disabled nmae="kelas" placeholder="Kelas" value="<?php echo $datanya['nm_kelas'] ?>">
                  </div>
                </div
                >
                <div class="form-group">
                  <label  class="col-sm-3 control-label wow animated fadeInDown" data-wow-offset="0" data-wow-delay="1.4s">Mata Pelajaran</label>
                  
                  <div class="col-sm-9">
                    <input type="text" class="form-control wow animated fadeInUp" data-wow-offset="0" data-wow-delay="1.2s" disabled name="matapelajaran" placeholder="Mata Pelajaran" value="<?php echo $datanya['nm_mapel'] ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label wow animated fadeInDown" data-wow-offset="0" data-wow-delay="1.4s">Tugas 1</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control wow animated fadeInUp" data-wow-offset="0" data-wow-delay="1s" name="tugas1" placeholder="Tugas 1" value="<?php echo $datanya['tugas1'] ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label wow animated fadeInDown" data-wow-offset="0" data-wow-delay="1.6s">Tugas 2</label>
                  
                  <div class="col-sm-9">
                    <input type="text" class="form-control wow animated fadeInUp" data-wow-offset="0" data-wow-delay="0.8s" name="tugas2" placeholder="Tugas 2" value="<?php echo $datanya['tugas2'] ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label  class="col-sm-3 control-label wow animated fadeInUp" data-wow-offset="0" data-wow-delay="1.6s">Tugas 3</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control wow animated fadeInUp" data-wow-offset="0" data-wow-delay="0.8s" name="tugas3" placeholder="Tugas 3" value="<?php echo $datanya['tugas3'] ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label wow animated fadeInUp" data-wow-offset="0" data-wow-delay="1.4s">Ulangan 1</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control wow animated fadeInUp" data-wow-offset="0" data-wow-delay="1s" name="ulangan1" placeholder="Ulangan 1" value="<?php echo $datanya['ulangan1'] ?>">
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="col-sm-3 control-label wow animated fadeInUp" data-wow-offset="0" data-wow-delay="1.2s">Ulangan 2</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control wow animated fadeInUp" data-wow-offset="0" data-wow-delay="1.2s" name="ulangan2" placeholder="Ulangan 2"value="<?php echo $datanya['ulangan2']?>">
                  </div>
                </div>
                
                <div class="form-group">
                  <label  class="col-sm-3 control-label wow animated fadeInUp" data-wow-offset="0" data-wow-delay="1s">UTS</label>
                  
                  <div class="col-sm-9">
                    <input type="text" class="form-control wow animated fadeInUp" data-wow-offset="0" data-wow-delay="1.4s" name="uts" placeholder="UTS" value="<?php echo $datanya['uts'] ?>">
                  </div>
                </div>
                
                <div class="form-group">
                  <label f class="col-sm-3 control-label wow animated fadeInUp" data-wow-offset="0" data-wow-delay="0.8s">UAS</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control wow animated fadeInUp" data-wow-offset="0" data-wow-delay="1.6s" name="uas" placeholder="UAS" value="<?php echo $datanya['uas'] ?>">
                  </div>
                </div>
                
                <button type="submit" class="btn btn-warning btn-block wow animated fadeInUp" data-wow-offset="0" data-wow-delay="2s">SIMPAN</button>
              

              </div>
              <!-- /.box-body -->
              
              <!-- /.box-footer -->
            
          </div>
      
            </div>
        </form>
        <!-- /col12 -->
        
         

    </div><!-- /container -->

  </div><!-- /slide4 -->

  <!-- === Slide 5 === -->
  
  <!-- === Slide 6 / Contact === -->
  <?php echo form_close();?>
  
</body>

  <!-- SCRIPTS -->
  <script src="<?php echo base_url('assets/');?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url('assets/');?>bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assets/');?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url('assets/');?>bower_components/datatables.net/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url('assets/');?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url('assets/');?>bower_components/raphael/raphael.min.js"></script>
<script src="<?php echo base_url('assets/');?>bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('assets/');?>bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('assets/');?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url('assets/');?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('assets/');?>bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url('assets/');?>bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url('assets/');?>bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url('assets/');?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('assets/');?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url('assets/');?>bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/');?>bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/');?>dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url('assets/');?>dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assets/');?>dist/js/demo1.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
<script src="<?php echo base_url('assets/');?>dist/js/wow.min.js"></script>
  <script>
    wow = new WOW({}).init();
  </script>
 <script>
   function nilai(vol) {
        document.querySelector('#nil').value = vol;
    }
    function nilai2(vol) {
        document.querySelector('#nil2').value = vol;
    }
    function nilai3(vol) {
        document.querySelector('#nil3').value = vol;
    }
    function ulangan1(vol) {
        document.querySelector('#nil4').value = vol;
    }
    function ulangan2(vol) {
        document.querySelector('#nil5').value = vol;
    }

    function uts(vol) {
        document.querySelector('#nil6').value = vol;
    }
    function uas(vol) {
        document.querySelector('#nil7').value = vol;
    }
  </script>
</html>