<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Raport Saya | Cetak Nilai Siswa</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<link rel="shortcut icon" href="<?php echo base_url('assets/');?>gambar/favicon.png" />
    
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body onload="window.print();">
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <img src="<?php echo base_url('assets/');?>gambar/favicon.png" height="50px"> Raport Saya
          <small class="pull-right"><?php
            
            echo date('H:i:s');
          ?></small>
        </h2>

<div class="pull-left">
        <table>
          <thead>
        <tr>
        <th></th> 
        <th></th></thead>
        </tr>
        <tbody>
            <?php echo form_hidden('id',$this->uri->segment(4));?>
        <tr>
          <td>Nama Siswa </td> 
        <td>: <?php echo $data['nama_siswa'] ?></td>
        </tr>
        <tr>
        <td>Nomor Induk Siswa </td> 
        <td>: <?php echo $data['nis'] ?> </td>
        </tr>
        </tbody>
        </table>
      </div>
 
               
<div class="pull-right">
        <table>
          <thead>
        <tr>
        <th></th> 
        <th></th></thead>
        </tr>
        <tbody>
        <tr>
        <td>Kelas </td> 
        <td>: <?php echo $kls['nm_kelas']?></td>
        </tr>
        <tr>
        <td>Tahun Ajaran </td> 
        <td>: <?php echo $kls['thn_ajaran'] ?> </td>
        </tr>
        </tbody>
        </table>
      </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      
        <div class="box-header text-center">
          <img class="img-responsive" src="<?php echo base_url('assets/gambar/list.png') ?>">
          
      </div>
      
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-bordered">
         <thead> 
                <th>MATA PELAJARAN</th>
                  <th>TUGAS</th>
                  <th>ULANGAN</th>
                  <th>UTS</th>
                  <th>UAS</th>
                  <th>JUMLAH</th>
                  <th>GRADE</th>
                </tr>
              </thead>
                <tbody>
                  <?php foreach($datanya as $b) { 
                    $tugas= $b->tugas1 + $b->tugas2 + $b->tugas3;
                    $tot_tugas = $tugas / 3;
                    $sep_tugas = number_format($tot_tugas,2);
                    $ulangan = $b->ulangan1+ $b->ulangan2;
                    $tot_ulangan = $ulangan / 2;
                    $sep_ulangan = number_format($tot_ulangan,2);
                    $total = $tot_tugas + $tot_ulangan + $b->uts + $b->uas;
                    $tot_total = $total / 4;
                    $sep_total = number_format($tot_total,2);
                    if($tot_total < 10){
                      $grade = "D";
                      }
                      elseif($tot_total < 20){
                        $grade = "D+";
                        if($tot_total >10){


                        $grade = "D+";
                        }
                      }
                      elseif($tot_total < 30){
                        $grade = "C-";
                        if($tot_total > 20){


                        $grade = "C-";
                        }
                      }
                      elseif($tot_total < 40){
                        $grade = "C";
                        if($tot_total >30){


                        $grade = "C";
                        }
                      }
                      elseif($tot_total <50){
                        $grade = "C+";
                        if($tot_total >50){


                        $grade = "C+";
                        }
                      }
                      elseif($tot_total <60){
                        $grade = "B-";
                        if($tot_total >50){


                        $grade = "B-";
                        }
                      }
                      elseif($tot_total <70){
                        $grade = "B";
                        if($tot_total >60){


                        $grade = "B";
                        }
                      }
                      elseif($tot_total <80){
                        $grade = "B+";
                        if($tot_total >70){


                        $grade = "B+";
                        }
                      }
                      elseif($tot_total <90){
                        $grade = "A-";
                        if($tot_total >80){


                        $grade = "A-";
                        }
                      }
                      else{
                        $grade = "A";
                      }
                    
                    ?>  

                      <tr>
                        <td><?php echo $b->nm_mapel ?> </td> 
                        <td><?php echo $sep_tugas ?> </td>
                        <td><?php echo $sep_ulangan ?></td>
                        <td><?php echo $b->uts ?></td>
                        <td><?php echo $b->uas ?></td>
                        <td><?php echo $sep_total ?></td>
                        <td><?php echo $grade ?></td>
                  
                  </tr>
                <?php } ?>
                </tbody>
        </table>
      </div>


        <div class="pull-left">
          <div class="col-xs-12">
              <table class="table table-bordered">
                <thead> 
                <tr>            
                  <th>IZIN</th>
                  <th>SAKIT</th>
                  <th>TANPA KETERANGAN</th>
                </tr>
              </thead>
                <tbody>

                      <tr>
                  <td><?php echo $absen['sakit'] ?></td>
                  <td><?php echo $absen['ijin'] ?></td>
                  <td><?php echo $absen['tanpa_ket'] ?></td>
                  </tr>
          </tbody>
              </table>
              </div>
              </div>
              <div class="pull-right">
          <div class="col-xs-12 text-center">
                      
                  <p><?php echo $this->session->userdata('ses_nama'); ?></p>
                <br><br>
                  <p>(NIP.<?php echo $this->session->userdata('ses_id'); ?>)</p>
              
              </div>
              </div>
    <!-- /.row -->

      <!-- /.col -->
    </div>
   
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>
