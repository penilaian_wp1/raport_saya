<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Raport Saya | Cetak Nilai Siswa</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body onload="window.print();">
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <img src="<?php echo base_url('assets/');?>gambar/favicon.png" height="50px"> Raport Saya
          <small class="pull-right"><?php
            $wkt=date('h:i:s');
            echo $wkt;
          ?></small>
        </h2>
        <p class="pull-right"> <?php echo $this->session->userdata('ses_nama')?> NIP.<?php echo $this->session->userdata('ses_id'); ?> </p>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      
        <div class="box-header text-center">
          <img class="img-responsive" src="<?php echo base_url('assets/gambar/list.png') ?>">
          
      </div>
      
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
         <thead> 
                <tr>    
                        
                  <th>NIS</th>
                  <th>NAMA</th>
                  <th>Kelas</th>
                  <th>MATA PELAJARAN</th>
                  <th>TUGAS 1</th>
                  <th>TUGAS 2</th>
                  <th>TUGAS 3</th>
                  <th>ULANGAN 1</th>
                  <th>ULANGAN 2</th>
                  <th>UTS</th>
                  <th>UAS</th>
                </tr>
              </thead>
                <tbody>
                  <?php foreach($datanya as $b) { ?>
                <tr>
                  <td><?php echo $b->nis ?></td>
                  <td><?php echo $b->nama_siswa ?></td>
                  <td><?php echo $b->nm_kelas ?></t>
                  <td><?php echo $b->nm_mapel ?></td>
                  <td><?php echo $b->tugas1 ?> </td>
                  <td><?php echo $b->tugas2 ?> </td>
                  <td><?php echo $b->tugas3 ?></td>
                  <td><?php echo $b->ulangan1 ?></td>
                  <td><?php echo $b->ulangan2 ?></td>
                  <td><?php echo $b->uts ?></td>
                  <td><?php echo $b->uas ?></td>
                  
                </tr>
                <?php } ?>
                </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

   
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>
