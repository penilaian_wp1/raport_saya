<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
  <!-- BASICS -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Raport Saya</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/');?>css/isotope.css" media="screen" />
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>js/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>css/bootstrap-theme.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>css/responsive-slider.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>css/animate.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>css/styles.css">

  <link rel="stylesheet" href="<?php echo base_url('assets/');?>bower_components/font-awesome/css/font-awesome.css">
  <!-- skin -->
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>skin/default.css">
  <!-- =======================================================
    Theme Name: Green
    Theme URL: https://bootstrapmade.com/green-free-one-page-bootstrap-template/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
  ======================================================= -->
  <link rel="shortcut icon" href="<?php echo base_url('assets/');?>gambar/favicon.png" />
   
</head>

<body>

<section id="header" class="appear">
  <div class="header">
    

      <div class="navbar navbar-fixed-top" role="navigation" data-0="line-height:100px; height:100px; background-color:rgba(0,0,0,0.3);" data-300="line-height:60px; height:60px; background-color:rgba(0,0,0,1);">

        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="fa fa-bars color-white"></span>
          </button>
          <h1><a class="navbar-brand " href="index.html" data-0="line-height:90px;" data-300="line-height:50px;"> Raport Saya
            </a></h1>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav" data-0="margin-top:20px;" data-300="margin-top:5px;">
            <li class="active"><a href="#header">Beranda</a></li>
            <li><a href="#team">Tim</a></li>
          </ul>
        </div>
        <!--/.navbar-collapse -->
      </div>


    
  </div>
</section>

  <div class="slider">
    <div id="about-slider">
      <div id="carousel-slider" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators visible-xs">
          <li data-target="#carousel-slider" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-slider" data-slide-to="1"></li>
          <li data-target="#carousel-slider" data-slide-to="2"></li>
        </ol>

        <div class="carousel-inner">
          <div class="item active" style="background-image: url(<?php echo base_url('assets/');?>img/baner.png);">
            <div class="carousel-caption">
              <div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.3s">
                <h2>Selamat Datang Di Raport Saya</h2>
              </div>
              <div class="col-md-10 col-md-offset-1">
                <div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.6s">
                  <p>Klik Dibawah Untuk Masuk</p>
                </div>
              </div>
              <div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.9s">
                <form class="form-inline">
                  <div class="form-group">
                   <button type="button" class="btn line-btn  light btn-lg" required="required" data-toggle="modal" data-target="#modal-default">Masuk</button>
                  </div>
                </form>
              </div>
            </div>
          </div>

          <div class="item" style="background-image: url(<?php echo base_url('assets/');?>img/bsner2.png);">
            <div class="carousel-caption">
              <div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="1.0s">
                <h2>Aplikasi Penilaian Raport</h2>
              </div>
              <div class="col-md-10 col-md-offset-1">
                <div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.6s">
                  <p>Klik Dibawah Untuk Masuk</p>
                </div>
              </div>
              <div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.9s">
                <form class="form-inline">
                  <div class="form-group">
                    <button type="button" class="btn line-btn  light btn-lg" required="required" data-toggle="modal" data-target="#modal-default">Masuk</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
          <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Masuk</h4>
              </div>
              <br>
              <div class="col-sm-2 col-md-3 col-md-offset-2">
              <img class="img-responsive" src="<?php echo base_url('assets/gambar/ic.png') ?>">
              </div>
              <div class="modal-body col-lg-offset-4">
                <form action="<?php echo base_url('C_login/proses_login')?>" method="POST" >
                <div class="text-danger text-bold" >
                <?php echo $this->session->flashdata('msg');?>
                </div>
                <div class="form-group">
                  <div class="input-group col-sm-6">
                    <input type="text" id="username" name="username" class="form-control" placeholder="Masukan Nama Pengguna...">
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-check-circle-outline"></i>
                      </span>
                    </div>
                  </div>
                </div>


                <div class="form-group">
               
                  <div class="input-group col-sm-6">
                    <input type="password" id="password" name="password" class="form-control" placeholder="Masukan Kata Sandi...">
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-check-circle-outline"></i>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left">Close</button>
                <div class="form-group">
                  <button class="btn btn-primary submit-btn">Lanjut</button>
              </div>
            </div>
            </div>
            </form>
            <!-- /.modal-content -->
          </div>

          
          <!-- /.modal-dialog -->
        </div>

        <a class="left carousel-control hidden-xs" href="#carousel-slider" data-slide="prev">
          <i class="fa fa-angle-left"></i>
        </a>

        <a class=" right carousel-control hidden-xs" href="#carousel-slider" data-slide="next">
          <i class="fa fa-angle-right"></i>
        </a>
      </div>
      <!--/#carousel-slider-->
    </div>
    <!--/#about-slider-->
  </div>
  <!--/#slider-->

  <!--about-->
  <section id="section-about" >
    <div class="container">
      <div class="about">
        <div class="row mar-bot40">
          <div class="col-md-offset-3 col-md-6">
            <div class="title">
              <div class="wow bounceIn">

                <h2 class="section-heading animated" data-animation="bounceInUp">Apa Itu Raport Saya ?</h2>


              </div>
            </div>
          </div>
        </div>
        <div class="row">

          <div class="row-slider">
            <div class="col-lg-6 mar-bot30">
              <div class="responsive-slider" data-spy="responsive-slider" data-autoplay="true">
                <div class="slides" data-group="slides">
                  <ul>

                    <div class="slide-body" data-group="slide">
                      <li>
                      <div >
                      <video class="video-responsive img-responsive" autoplay="" loop="">
                      <source src="<?php echo base_url('assets/img/main.mp4');?>" type="video/mp4">
                      <source src="<?php echo base_url('assets/img/main.ogg');?>" type="video/ogg">
                      </video>
                      </div>
                      </li>
                      <li><img alt="" class="img-responsive" src="<?php echo base_url('assets/');?>img/01.jpg" width="100%" height="350" /></li>
                      <li><img alt="" class="img-responsive" src="<?php echo base_url('assets/');?>img/02.jpg" width="100%" height="350" /></li>
                      <li><img alt="" class="img-responsive" src="<?php echo base_url('assets/');?>img/03.jpg" width="100%" height="350" /></li>

                    </div>
                  </ul>
                  <a class="slider-control left" href="#" data-jump="prev"><i class="fa fa-angle-left fa-2x"></i></a>
                  <a class="slider-control right" href="#" data-jump="next"><i class="fa fa-angle-right fa-2x"></i></a>

                </div>
              </div>
            </div>

            <div class="col-lg-6 ">
              <div class="company mar-left10">
                <h4>Raport saya dibuat pada tahun 2018, ini adalah aplikasi berbasis Web dan di buat untuk tugas <span>Web Pemograman1 </span> kampus UBSI.</h4>
                <p>Raport Saya adalah sebuah Aplikasi pengolahan nilai tingkat SMP.</p>
              </div>
              <div class="list-style">
                <div class="row">
                  <div class="col-lg-6 col-sm-6 col-xs-12">
                    <ul>
                      <li>Penginputan Nilai Tugas</li>
                      <li>Penginputan Nilai Ulangan</li>
                      <li>Penginputan Nilai UTS</li>
                      <li>Penginputan Nilai UAS</li>
                    </ul>
                  </div>
                  <div class="col-lg-6 col-sm-6 col-xs-12">
                    <ul>
                      <li>Sederhana</li>
                      <li>Praktis</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>

      </div>

    </div>
  </section>
  <!--/about-->

  <!-- spacer section:testimonial -->
 
  <!--/services-->

  <!-- spacer section:testimonial -->
  <section id="testimonials" class="section" data-stellar-background-ratio="0.5">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="align-center">
            <div class="testimonial pad-top40 pad-bot40 clearfix">
              <h5>
               " Hanya pendidikan yang bisa menyelamatkan masa depan, tanpa pendidikan Indonesia tak mungkin bertahan. "
              </h5>
            <h3>~Najwa Shihab~</h3>
            </div>

          </div>
        </div>
      </div>

    </div>

  </section>

  <!-- team -->
  <section id="team" class="team-section appear clearfix">
    <div class="container">

      <div class="row mar-bot10">
        <div class="col-md-offset-3 col-md-6">
          <div class="section-header">
            <div class="wow bounceIn">

              <h2 class="section-heading animated" data-animation="bounceInUp">Tim Kita</h2>
              <p>Universitas Bina Sarana Informatika</p>
              <img src="<?php echo base_url('assets/img/ubsi.jpg') ?>" height="150px">
            

            </div>
          </div>
        </div>
      </div>

      <div class="row align-center mar-bot45">
        <div class="col-lg-6">
          <div class="wow bounceIn" data-animation-delay="4.8s">
            <div class="team-member">
              <div class="profile-picture">
                <figure><img src="<?php echo base_url('assets/');?>img/_MG_5447.jpg" alt=""></figure>
                <div class="profile-overlay"></div>
                
              </div>
              <div class="team-detail">
                <br>
                <h4>SAEPUDIN</h4>
                <span>12162785</span>
              </div>
              <div class="team-bio">
                <p>Pujian Hanya Akan Membuat Kita Menjadi Sumbong dan Angkuh.</p>
                
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 ">

          <div class="wow bounceIn">
            <div class="team-member">
              <div class="profile-picture">
                <figure><img src="<?php echo base_url('assets/');?>img/_MG_5420.jpg" alt=""></figure>
                <div class="profile-overlay"></div>
                
              </div>
              <div class="team-detail">
                <br>
                <h4>AMIR MAHFUD</h4>
                <span>12161943</span>
              </div>
              <div class="team-bio">
                <p>Pendidikan adalah teman terbaik. Seorang yang terdidik akan dihargai di manapun. Pendidikan mengalahkan kecantikan dan masa muda.</p>
                  
              </div>
            </div>
          </div>
        </div>
      </div>
       <div class="row align-center mar-bot45">
        <div class="col-lg-6 ">
          <div class="wow bounceIn">
            <div class="team-member">
              <div class="profile-picture">
                <figure><img src="<?php echo base_url('assets/');?>img/_MG_5449.jpg" alt=""></figure>
                <div class="profile-overlay"></div>
              
              </div>
              <div class="team-detail">
                <br>
                <h4>M. ALIFUPPEDUAI S</h4>
                <span>12164187</span>
              </div>
              <div class="team-bio">
                <p>Siapapun yang terlalu banyak membaca dan terlalu sedikit menggunakan otaknya akan jatuh ke dalam kebiasaan malas berpikir.</p>
                  
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 ">

          <div class="wow bounceIn">
            <div class="team-member">
              <div class="profile-picture">
                <figure><img src="<?php echo base_url('assets/');?>img/_MG_5448.jpg" alt=""></figure>
                <div class="profile-overlay"></div>
                
              </div>
              <div class="team-detail">
                <br>
                <h4>SAHRUL RAMADHAN</h4>
                <span>12161547</span>
              </div>
              <div class="team-bio">
                <p>Satu-satunya orang yang terpelajar adalah yang telah belajar bagaimana cara mendengar dan merubah.</p>
                  
              </div>
            </div>
          </div>
        </div>
</div>
  </section>
  <!-- /team -->

  <!-- spacer section:stats -->
  

  <!-- spacer section:testimonial -->
  
  
  <!-- map -->
  

  <section id="footer" class="section footer">
    <div class="container">
        <div class="col-sm-12 align-center">
          <p>&copy; Raport Saya</p>
          <div class="credits"> 
            2018
        </div>
      </div>
    </div>

  </section>
  <a href="#header" class="scrollup"><i class="fa fa-chevron-up"></i></a>

  <script src="<?php echo base_url('assets/');?>js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  <script src="<?php echo base_url('assets/');?>js/jquery.js"></script>
  <script src="<?php echo base_url('assets/');?>js/jquery.easing.1.3.js"></script>
  <script src="<?php echo base_url('assets/');?>js/bootstrap.min.js"></script>
  <script src="<?php echo base_url('assets/');?>js/jquery.isotope.min.js"></script>
  <script src="<?php echo base_url('assets/');?>js/jquery.nicescroll.min.js"></script>
  <script src="<?php echo base_url('assets/');?>js/fancybox/jquery.fancybox.pack.js"></script>
  <script src="<?php echo base_url('assets/');?>js/skrollr.min.js"></script>
  <script src="<?php echo base_url('assets/');?>js/jquery.scrollTo.js"></script>
  <script src="<?php echo base_url('assets/');?>js/jquery.localScroll.js"></script>
  <script src="<?php echo base_url('assets/');?>js/stellar.js"></script>
  <script src="<?php echo base_url('assets/');?>js/responsive-slider.js"></script>
  <script src="<?php echo base_url('assets/');?>js/jquery.appear.js"></script>
  <script src="<?php echo base_url('assets/');?>js/grid.js"></script>
  <script src="<?php echo base_url('assets/');?>js/main.js"></script>
  <script src="<?php echo base_url('assets/');?>js/wow.min.js"></script>
  <script>
    wow = new WOW({}).init();
  </script>
  <script src="<?php echo base_url('assets/');?>contactform/contactform.js"></script>

</body>

</html>
