<?php 

class M_mapel extends CI_model{
	
    public $kd_mapel;
    public $nm_mapel;
    public $nip;

	
	public function query_tampilmapel(){
		$query= $this->db->query("SELECT * from tbl_mapel a, tbl_pegawai b 
								  			where (a.nip=b.nip) group by a.kd_mapel");
		return $query;
	}

	public function query_tampilnamaguru(){
		$query= $this->db->query("SELECT * from tbl_pegawai ");
		return $query;
	}

    public function tambah(){
        $post = $this->input->post();

		$this->kd_mapel		= $post["KDMAPEL"];
		$this->nip 			= $post["NAMA"];
		$this->nm_mapel		= $post["NMMAPEL"];

		$this->db->insert('tbl_mapel', $this);
	}

	function update_data(){
        $post = $this->input->post();
		$this->kd_mapel		= $post["KDMAPEL"];
		$this->nip 			= $post["NAMA"];
		$this->nm_mapel		= $post["NMMAPEL"];

		$kd_mapel	= $this->input->post('KDMAPEL');
		$this->db->update('tbl_mapel',$this, array("kd_mapel" => $kd_mapel) );
	}
	
	// Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
	public function insert_multiple($data){
		$this->db->insert_batch('tbl_mapel', $data);
	}
	
	function hapus_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}

	public function upload_file($filename){
		$this->load->library('upload'); // Load librari upload
		
		$config['upload_path'] = './excel/';
		$config['allowed_types'] = 'xlsx';
		$config['max_size']	= '2048';
		$config['overwrite'] = true;
		$config['file_name'] = $filename;
	
		$this->upload->initialize($config); // Load konfigurasi uploadnya
		if($this->upload->do_upload('file')){ // Lakukan upload dan Cek jika proses upload berhasil
			// Jika berhasil :
			$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
			return $return;
		}else{
			// Jika gagal :
			$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
			return $return;
		}
	}
    public function edit_data($where,$table){		
		return $this->db->get_where($table,$where);
	}	
	
    private function _uploadImage(){
		$config['upload_path']          = './assets/dist/img/';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['file_name']            = $this->nip;
		$config['overwrite']			= true;
		$config['max_size']             = 4096; // 1MB
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('image')) {
			return $this->upload->data("file_name");
		}
		
		return "default.jpg";	
	}

	
	// function edit_data(){		
		
 	//        $nip = $_POST['nip'];
 	//        $nama = $_POST['nama'];
 	//        $sex = $_POST['sex'];
        
	// 	$where = array('nip' => $nip);
	// 	return $this->db->get_where($table,$where);
	// }
	
    // public function delete($id){
    //     return $this->db->delete($this->_table, array("nip" => $id));
	// }
	// public function edit($nip)
    // {

    //     $query = $this->db->where("nip", $nip)
    //             ->get("tbl_karyawan");

    //     if($query){
    //         return $query->row();
    //     }else{
    //         return false;
    //     }

    // }

    // public function update($data, $id)
    // {

    //     $query = $this->db->update("tbl_karyawan", $data, $id);

    //     if($query){
    //         return true;
    //     }else{
    //         return false;
    //     }

    // }
	// public function edit($id){
	// 	$query = $this->db->get_where('tbl_karyawan', array('nip' => $id));
	// 	return $query;
	// }
	// public function update($kondisi, $data_update) {
    //     $this->db->where($kondisi);
    //     return $this->db->update('tbl_karyawan', $data_update);
	// }

	// function update_data($where,$data,$table){
	// 	$this->db->where($where);
	// 	$this->db->update($table,$data);
	// }	
	 
	// function update($id_nim){ //update data berdasarkan nim
	// 	$nip = $this->input->post('nip');
    //     $nama = $this->input->post('nama');
    //     $tmptlahir = $this->input->post('tempatlahir');
    //     $tgllahir = $this->input->post('tanggallahir');
    //     $jekel = $this->input->post('jekel');
    //     $agama = $this->input->post('agama');
    //     $alamat = $this->input->post('alamat');
    //     $telpon = $this->input->post('telp');
    //     $email = $this->input->post('email');
	// 	$password = md5($this->input->post('password'));
		
	// 	$data = array(
    //         'email'         => $email,
    //         'password'      => $password,
	// 		'nama' 			=> $nama,
	// 		'tempat_lahir' 	=> $tmptlahir,
	// 		'tanggal_lahir' => $tgllahir,
	// 		'jekel' 		=> $jekel,
	// 		'agama' 		=> $agama,
	// 		'alamat' 		=> $alamat,
    //         'telp'   		=> $telpon,
    //         'status'        => 'GURU'
	// 	);
		
	// 	$this->db->where('nip',$id_nim);
	// 	$this->db->update('tbl_karyawan',$data); //update data mahasiswa
	//    }

	// function getById($id_nim){ //mengambil data berdasarkan id (primary key)
	// return $this->db->get_where('tbl_karyawan',array('nip'=>$id_nim))->row();
	// }

	// public function model_edit($primarykey){
	// 	return $this->db->get_where('tbl_karyawan',array('nip'=>$primarykey));
	// }

	// public function ubah($data, $id){
    //     $this->db->where('nip',$id);
    //     $this->db->update($_table, $data);
    //     return TRUE;
	// }

	// public function getById($id){
    //     return $this->db->get_where($this->_table, ["nip" => $id])->row();
    // }

}

