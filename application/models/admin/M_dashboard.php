<?php 

class M_dashboard extends CI_model{
	
	public function data_user(){
	$query = $this->db->get('tbl_pegawai');

	return $query;
		
	}

	public function query_guru(){
	$query= $this->db->query("SELECT * from tbl_pegawai limit 4");
							
	return $query;
	}

	public function query_siswa(){
		$query= $this->db->query("SELECT * from tbl_siswa where status='Aktif' limit 4");
								
		return $query;
	}

	public function query_mapel(){
		$query= $this->db->query("SELECT * from tbl_mapel a, tbl_pegawai b 
								  			where (a.nip=b.nip) group by a.kd_mapel limit 4");
		return $query;
	}

	public function query_kelas(){
		$query= $this->db->query("SELECT * from tbl_kelas a, tbl_pegawai b 
											where (a.nip=b.nip) group by a.kd_kelas limit 4");
		
		return $query;
	}

	public function hitungjumlah_guru()
	{   
		$query = $this->db->get('tbl_pegawai');
		if($query->num_rows()>0)
		{
		return $query->num_rows();
		}
		else
		{
		return 0;
		}
	}

	public function hitungjumlah_siswa()
	{   
		$query = $this->db->get('tbl_siswa');
		if($query->num_rows()>0)
		{
		return $query->num_rows();
		}
		else
		{
		return 0;
		}
	}

	public function hitungjumlah_kelas()
	{   
		$query = $this->db->get('tbl_kelas');
		if($query->num_rows()>0)
		{
		return $query->num_rows();
		}
		else
		{
		return 0;
		}
	}

	public function hitungjumlah_mapel()
	{   
		$query = $this->db->get('tbl_mapel');
		if($query->num_rows()>0)
		{
		return $query->num_rows();
		}
		else
		{
		return 0;
		}
	}

}