<?php 

class M_dataguru extends CI_model{

    public $email;
    public $password;
    public $nip;
    public $nama;
    public $tempat_lahir;
    public $tanggal_lahir;
    public $jekel;
    public $agama;
    public $alamat;
    public $telp;
    public $status;
   	public $foto = "default.jpg";

	
	public function query_tampilguruwalikelas(){
		$query= $this->db->query("SELECT * FROM tbl_pegawai WHERE status=('Guru'and'Wali Kelas') ");
		return $query;
  	}
    public function tambah($upload){
        // $post = $this->input->post();

        // $this->email 		= $post["EMAIL"];
		// $this->password 	= md5($post["PASSWORD"]);
		// $this->nip 			= $post["NIP"];
		// $this->nama 		= $post["NAMA"];
		// $this->tempat_lahir = $post["TEMPATLAHIR"];
		// $this->tanggal_lahir= $post["TANGGALLAHIR"];
		// $this->jekel 		= $post["JEKEL"];
		// $this->agama 		= $post["AGAMA"];
		// $this->alamat 		= $post["ALAMAT"];
		// $this->telp 		= $post["TELEPON"];
		// $this->foto			= $post["image"];
		// $this->status 		= $post["STATUS"];

		$data = array(
			'nip'			=> $this->input->post('NIP'),
			'nama' 			=> $this->input->post('NAMA'),
			'tempat_lahir' 	=> $this->input->post('TEMPATLAHIR'),
			'tanggal_lahir' => $this->input->post('TANGGALLAHIR'),
			'jekel' 		=> $this->input->post('JEKEL'),
			'agama' 		=> $this->input->post('AGAMA'),
			'alamat' 		=> $this->input->post('ALAMAT'),
			'telp'   		=> $this->input->post('TELEPON'),
			'email'			=> $this->input->post('EMAIL'),
			'password'		=> md5($this->input->post('PASSWORD')),
			'foto'			=> $upload['file']['file_name'],
			'status'        => $this->input->post('STATUS')
		);

		$this->db->insert('tbl_pegawai', $data);
	}

	function update_data($upload){
        // $post = $this->input->post();
		// $this->nip			=$post["NIP"];
		// $this->nama			=$post["NAMA"];
		// $this->tempat_lahir	=$post["TEMPATLAHIR"];
		// $this->tanggal_lahir=$post["TANGGALLAHIR"];
		// $this->jekel		=$post["JEKEL"];
		// $this->agama		=$post["AGAMA"];
		// $this->alamat		=$post["ALAMAT"];
		// $this->telp			=$post["TELEPON"];
		// $this->email		=$post["EMAIL"];
		// $this->password		=md5($post["PASSWORD"]);
		// $this->status		=$post["STATUS"];
		// if (!empty($_FILES["image"]["file_name"])) {
		// 	$this->foto 	= $this->_uploadImage();
        // } else {
        //     $this->foto = $post["old_image"];
		// }
		
		// $nip	= $this->input->post('NIP');
		// if(!empty($_FILES["image"]["name"])) {
			
		// } else {
		// 	$this->foto = $post["old_image"];
		// }
		// $cek = if(!empty($_FILES["GAMBAR"]["name"])) {
		// 	'foto'			=> $upload['file']['file_name'],
		// } else {
		// 	'foto'			=> $this->input->post('gambar_lama'),
		// };

		$data = array(
			'nama' 			=> $this->input->post('NAMA'),
			'tempat_lahir' 	=> $this->input->post('TEMPATLAHIR'),
			'tanggal_lahir' => $this->input->post('TANGGALLAHIR'),
			'jekel' 		=> $this->input->post('JEKEL'),
			'agama' 		=> $this->input->post('AGAMA'),
			'alamat' 		=> $this->input->post('ALAMAT'),
			'telp'   		=> $this->input->post('TELEPON'),
			'email'			=> $this->input->post('EMAIL'),
			// 'password'		=> md5($this->input->post('PASSWORD')),
			'foto'			=> $upload['file']['file_name'],
			'status'        => $this->input->post('STATUS')
		);
		
		$this->db->where(array('nip' => $this->input->post('NIP'))
		);
		$this->db->update('tbl_pegawai',$data);
		// $this->db->where();
	}

	public function tampilkan(){
		return $this->db->get('siswa')->result(); // Tampilkan semua data yang ada di tabel siswa
	}
	
	// // Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
	// public function insert_multiple($data){
	// 	$this->db->insert_batch('tbl_pegawai', $data);
	// }

	function hapus_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}

	// public function upload_file($filename){
	// 	$this->load->library('upload'); // Load librari upload
		
	// 	$config['upload_path'] = './excel/';
	// 	$config['allowed_types'] = 'xlsx';
	// 	$config['max_size']	= '2048';
	// 	$config['overwrite'] = true;
	// 	$config['file_name'] = $filename;
	
	// 	$this->upload->initialize($config); // Load konfigurasi uploadnya
	// 	if($this->upload->do_upload('file')){ // Lakukan upload dan Cek jika proses upload berhasil
	// 		// Jika berhasil :
	// 		$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
	// 		return $return;
	// 	}else{
	// 		// Jika gagal :
	// 		$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
	// 		return $return; 
	// 	}
	// }

    public function edit_data($where,$table){		
		return $this->db->get_where($table,$where);
	}	
	
    public function uploadgambar()
	{
		$config['upload_path']          = './assets/img/foto/';
		$config['allowed_types']        = 'jpeg|jpg|png';
		$config['max_size'] 		 	= '10200';
		$config['max_width']  			= '5184';
		$config['max_height']  			= '3888';
		$config['remove_space'] 		= TRUE;
	  
		$this->load->library('upload', $config); // Load konfigurasi uploadnya
		if($this->upload->do_upload('GAMBAR')){ // Lakukan upload dan Cek jika proses upload berhasil
		  // Jika berhasil :
		  $return = array('result' => 'adagambar', 'file' => $this->upload->data(), 'error' => '');
		  return $return;
		}else{
		  // Jika gagal :
		  $return = array('result' => 'tanpagambar', 'file' => '', 'error' => $this->upload->display_errors());
		  return $return;
		}
	}

	
	// function edit_data(){		
		
 	//        $nip = $_POST['nip'];
 	//        $nama = $_POST['nama'];
 	//        $sex = $_POST['sex'];
        
	// 	$where = array('nip' => $nip);
	// 	return $this->db->get_where($table,$where);
	// }
	
    // public function delete($id){
    //     return $this->db->delete($this->_table, array("nip" => $id));
	// }
	// public function edit($nip)
    // {

    //     $query = $this->db->where("nip", $nip)
    //             ->get("tbl_karyawan");

    //     if($query){
    //         return $query->row();
    //     }else{
    //         return false;
    //     }

    // }

    // public function update($data, $id)
    // {

    //     $query = $this->db->update("tbl_karyawan", $data, $id);

    //     if($query){
    //         return true;
    //     }else{
    //         return false;
    //     }

    // }
	// public function edit($id){
	// 	$query = $this->db->get_where('tbl_karyawan', array('nip' => $id));
	// 	return $query;
	// }
	// public function update($kondisi, $data_update) {
    //     $this->db->where($kondisi);
    //     return $this->db->update('tbl_karyawan', $data_update);
	// }

	// function update_data($where,$data,$table){
	// 	$this->db->where($where);
	// 	$this->db->update($table,$data);
	// }	
	 
	// function update($id_nim){ //update data berdasarkan nim
	// 	$nip = $this->input->post('nip');
    //     $nama = $this->input->post('nama');
    //     $tmptlahir = $this->input->post('tempatlahir');
    //     $tgllahir = $this->input->post('tanggallahir');
    //     $jekel = $this->input->post('jekel');
    //     $agama = $this->input->post('agama');
    //     $alamat = $this->input->post('alamat');
    //     $telpon = $this->input->post('telp');
    //     $email = $this->input->post('email');
	// 	$password = md5($this->input->post('password'));
		
	// 	$data = array(
    //         'email'         => $email,
    //         'password'      => $password,
	// 		'nama' 			=> $nama,
	// 		'tempat_lahir' 	=> $tmptlahir,
	// 		'tanggal_lahir' => $tgllahir,
	// 		'jekel' 		=> $jekel,
	// 		'agama' 		=> $agama,
	// 		'alamat' 		=> $alamat,
    //         'telp'   		=> $telpon,
    //         'status'        => 'GURU'
	// 	);
		
	// 	$this->db->where('nip',$id_nim);
	// 	$this->db->update('tbl_karyawan',$data); //update data mahasiswa
	//    }

	// function getById($id_nim){ //mengambil data berdasarkan id (primary key)
	// return $this->db->get_where('tbl_karyawan',array('nip'=>$id_nim))->row();
	// }

	// public function model_edit($primarykey){
	// 	return $this->db->get_where('tbl_karyawan',array('nip'=>$primarykey));
	// }

	// public function ubah($data, $id){
    //     $this->db->where('nip',$id);
    //     $this->db->update($_table, $data);
    //     return TRUE;
	// }

	// public function getById($id){
    //     return $this->db->get_where($this->_table, ["nip" => $id])->row();
	// }
	// public function view(){
	// 	return $this->db->get('gambar')->result();
	//   }

	// public function upload(){
	// 	$config['upload_path']          = './gambar/';
	// 	$config['allowed_types']        = 'gif|jpg|png';
	// 	$config['max_size'] 		 	= '2048';
	// 	$config['remove_space'] 		= TRUE;
	  
	// 	$this->load->library('upload', $config); // Load konfigurasi uploadnya
	// 	if($this->upload->do_upload('input_gambar')){ // Lakukan upload dan Cek jika proses upload berhasil
	// 	  // Jika berhasil :
	// 	  $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
	// 	  return $return;
	// 	}else{
	// 	  // Jika gagal :
	// 	  $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
	// 	  return $return;
	// 	}
	//   }
	
	//   public function save($upload){
	// 	$data = array(
	// 	  'deskripsi'=>$this->input->post('input_deskripsi'),
	// 	  'nama_file' => $upload['file']['file_name'],
	// 	  'ukuran_file' => $upload['file']['file_size'],
	// 	  'tipe_file' => $upload['file']['file_type']
	// 	);
		
	// 	$this->db->insert('gambar', $data);
	//   }

}

