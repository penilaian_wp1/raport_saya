<?php 

class M_dashboard extends CI_model{
	
	public function data_user(){
	$query = $this->db->get('tbl_pegawai');

	return $query;
		
	}

	public function edit_data($where, $table){
		return $this->db->get_where($table,$where);
	}

	public function nilai($id)
	 {
	  return $this->db->query("
	 SELECT * from 
	 tbl_nilai a,tbl_siswa b ,tbl_mapel c,tbl_kelas d,tbl_pegawai f 
	 where (a.nis=b.nis) 
	 AND (a.kd_mapel=c.kd_mapel)
	 AND (a.kd_kelas=d.kd_kelas)
	 AND (a.nip='$id')
	 group by a.kd_nilai");
	 }

	 public function nilai_walkel($id)
	 {
	  return $this->db->query("
	 SELECT * from 
	 tbl_nilai a,tbl_siswa b ,tbl_mapel c,tbl_kelas d
	 where (a.nis=b.nis) 
	 AND (a.kd_mapel=c.kd_mapel)
	 AND (a.kd_kelas=d.kd_kelas)
	 AND (d.nip='$id')
	 group by a.kd_nilai");
	 }

	 

	function produkk($kode_barang){
	    	return $this->db->get_where('barang',array('id_barang'=>$kode_barang));
	    }

	 public function kelas_guru($id)
	 {
	  return $this->db->query("
	 SELECT * from 
	 tbl_kelas a,tbl_pegawai b 
	 where (a.nip='$id')
	 group by a.kd_kelas");
	 }

	  public function mapel($id)
	 {
	  return $this->db->query("
	 SELECT * from 
	 tbl_mapel a, tbl_pegawai b 
	 where (a.nip='$id')
	 group by a.kd_mapel");
	 }

	public function nilai_diguru($id)
	 {
	  return $this->db->query("
	 SELECT * from 
	 tbl_nilai a,tbl_siswa b ,tbl_mapel c,tbl_kelas d,tbl_pegawai f 
	 where (a.nis=b.nis) 
	 AND (a.kd_mapel=c.kd_mapel)
	 AND (a.kd_kelas=d.kd_kelas)
	 AND (a.nip=f.nip)
	 AND (a.kd_nilai='$id')
	 group by $id");
	 }

	 public function absensi($id)
	 {
	  return $this->db->query("
	 SELECT * from 
	 tbl_absen a, tbl_datakelassiswa b, tbl_kelas c, tbl_siswa d
	 where (a.kd_datakelas=b.kd_datakelas)
	 AND (b.nis=d.nis)
	 AND (b.kd_kelas=c.kd_kelas)
	 AND (c.nip='$id')
	 group by a.kd_absen");
	 }

	  public function edit_absensi($ed)
	 {
	  return $this->db->query("
	 SELECT * from 
	 tbl_absen a, tbl_datakelassiswa b, tbl_kelas c, tbl_siswa d
	 where (a.kd_datakelas=b.kd_datakelas)
	 AND (b.nis=d.nis)
	 AND (b.kd_kelas=c.kd_kelas)
	 AND (a.kd_absen='$ed')
	 group by '$ed'");
	 }

	 public function kelas($id)
	 {
	 return $this->db->query(" 
	 	SELECT * from 
	 	tbl_datakelassiswa a, tbl_siswa b, tbl_kelas c
	 	where (a.nis=b.nis)
	 	AND (a.kd_kelas=c.kd_kelas)
	 	AND (c.nip='$id')
	 	group by a.kd_datakelas");
	}
	
	 public function nilai_walkel_siswa($id)
	 {
	  return $this->db->query("
	 SELECT * from 
	 tbl_nilai a,tbl_siswa b ,tbl_mapel c,tbl_kelas d,tbl_pegawai f 
	 where (a.nis='id') 
	 AND (a.kd_mapel=c.kd_mapel)
	 AND (a.kd_kelas=d.kd_kelas)
	 AND (a.nip=f.nip)
	 group by a.kd_nilai");
	 }

}