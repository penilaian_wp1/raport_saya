<?php 

class C_importexcel extends CI_controller{
    private $filename = "import_data"; // Kita tentukan nama filenya

    public function __construct(){
		parent::__construct();
			$this->load->model('admin/M_importexcel');
	}

    public function formguru(){
        $data = array(); // Buat variabel $data sebagai array
        
        if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
            // lakukan upload file dengan memanggil function upload yang ada di SiswaModel.php
            $upload = $this->M_importexcel->upload_fileguru($this->filename);
            
            if($upload['result'] == "success"){ // Jika proses upload sukses
                // Load plugin PHPExcel nya
                include APPPATH.'third_party/PHPExcel/PHPExcel.php';
                
                $excelreader = new PHPExcel_Reader_Excel2007();
                $loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang tadi diupload ke folder excel
                $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true, true, true, true, true, true, true, true, true);
                
                // Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
                // Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
                $data['sheet'] = $sheet; 
            }else{ // Jika proses upload gagal
                $data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
            }
        }
        
        $this->load->view('admin/import/data_import_guru', $data);
    }
    
    public function importguru(){
        // Load plugin PHPExcel nya
        include APPPATH.'third_party/PHPExcel/PHPExcel.php';
        
        $excelreader = new PHPExcel_Reader_Excel2007();
        $loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang telah diupload ke folder excel
        $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true, true, true, true, true, true, true, true, true);
        
        // Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
        $data = array();
        
        $numrow = 1;
        foreach($sheet as $row){
            // Cek $numrow apakah lebih dari 1
            // Artinya karena baris pertama adalah nama-nama kolom
            // Jadi dilewat saja, tidak usah diimport
            if($numrow > 1){
                // Kita push (add) array data ke variabel data
                array_push($data, array(
                    'email'=>$row['H'], // Insert data alamat dari kolom D di excel
                    'password'=> md5($row['I']), // Insert data alamat dari kolom D di excel
                    'nip'=>$row['A'], // Insert data nis dari kolom A di excel
                    'nama'=>$row['B'], // Insert data nama dari kolom B di excel
                    'tempat_lahir'=>$row['C'], // Insert data jenis kelamin dari kolom C di excel
                    'tanggal_lahir'=>$row['D'], // Insert data alamat dari kolom D di excel
                    'jekel'=>$row['E'], // Insert data alamat dari kolom D di excel
                    'agama'=>$row['F'], // Insert data alamat dari kolom D di excel
                    'alamat'=>$row['J'], // Insert data alamat dari kolom D di excel
                    'telp'=>$row['G'], // Insert data alamat dari kolom D di excel
                    'status'=>$row['K'],
                    'foto'  =>$row['L']
                ));
            }
            
            $numrow++; // Tambah 1 setiap kali looping
        }

        // Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
        $this->M_importexcel->insert_multipleguru($data);
        $this->session->set_flashdata('sukses', '<div class="alert alert-success alert-dismissible">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    <h4><i class="icon fa fa-check"></i> SUKSES TAMBAH DATA</h4>
                                                    Data berhasil ditambahkan.
                                                </div>');
        redirect(base_url('admin/C_dataguru'));
    }

    public function formsiswa(){
        $data = array(); // Buat variabel $data sebagai array
        
        if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
            // lakukan upload file dengan memanggil function upload yang ada di SiswaModel.php
            $upload = $this->M_importexcel->upload_filesiswa($this->filename);
            
            if($upload['result'] == "success"){ // Jika proses upload sukses
                // Load plugin PHPExcel nya
                include APPPATH.'third_party/PHPExcel/PHPExcel.php';
                
                $excelreader = new PHPExcel_Reader_Excel2007();
                $loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang tadi diupload ke folder excel
                $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true, true, true, true, true, true, true, true, true);
                
                // Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
                // Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
                $data['sheet'] = $sheet; 
            }else{ // Jika proses upload gagal
                $data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
            }
        }
        
        $this->load->view('admin/import/data_import_siswa', $data);
    }
    
    public function importsiswa(){
        // Load plugin PHPExcel nya
        include APPPATH.'third_party/PHPExcel/PHPExcel.php';
        
        $excelreader = new PHPExcel_Reader_Excel2007();
        $loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang telah diupload ke folder excel
        $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true, true, true, true, true, true, true, true, true);
        
        // Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
        $data = array();
        
        $numrow = 1;
        foreach($sheet as $row){
            // Cek $numrow apakah lebih dari 1
            // Artinya karena baris pertama adalah nama-nama kolom
            // Jadi dilewat saja, tidak usah diimport
            if($numrow > 1){
                // Kita push (add) array data ke variabel data
                array_push($data, array(
                    'email'=>$row['H'], // Insert data alamat dari kolom D di excel
                    'password'=> md5($row['I']), // Insert data alamat dari kolom D di excel
                    'nis'=>$row['A'], // Insert data nis dari kolom A di excel
                    'nama_siswa'=>$row['B'], // Insert data nama dari kolom B di excel
                    'tempat_lahir'=>$row['C'], // Insert data jenis kelamin dari kolom C di excel
                    'tanggal_lahir'=>$row['D'], // Insert data alamat dari kolom D di excel
                    'jekel'=>$row['E'], // Insert data alamat dari kolom D di excel
                    'agama'=>$row['F'], // Insert data alamat dari kolom D di excel
                    'alamat'=>$row['J'], // Insert data alamat dari kolom D di excel
                    'telp'=>$row['G'], // Insert data alamat dari kolom D di excel
                    'status'=>$row['K'],
                    'foto'  =>$row['L']
                ));
            }
            
            $numrow++; // Tambah 1 setiap kali looping
        }

        // Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
        $this->M_importexcel->insert_multiplesiswa($data);
        $this->session->set_flashdata('sukses', '<div class="alert alert-success alert-dismissible">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    <h4><i class="icon fa fa-check"></i> SUKSES TAMBAH DATA</h4>
                                                    Data berhasil ditambahkan.
                                                </div>');
        redirect(base_url('admin/C_datasiswa'));
    }
}