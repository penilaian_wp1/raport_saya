<?php 

class C_lapguru extends CI_controller{

	public function __construct(){
		parent::__construct();
			// if($this->session->userdata('udhmasuk') != TRUE){
	  //           $url=base_url();
	  //           redirect($url);
	  //       }
			// elseif($this->session->userdata('ses_id') && $this->session->userdata('akses')=='2') {
			// 	redirect(base_url('guru/C_dashboard'));	
			// }
			$this->load->model('admin/M_dataguru');
      $this->load->model('guru/M_dashboard');
			// $this->load->library('form_validation');
	}

  public function index(){
    if($this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='3'){
    $id = $this->session->userdata('ses_id'); 
    $query['datanya'] = $this->M_dashboard->nilai($id)->result();
    $query['mapel'] = $this->M_dashboard->mapel($id)->result();
    $query['walkel'] = $this->M_dashboard->kelas_guru($id)->result();
    $data = $this->M_dashboard->nilai($id);
      $dt=$data->row_array();
    $this->load->view('laporan/laporan_nilai_siswa_guru',$query);
  }else{
      $this->load->view('kesalahan');
    }
  }

  public function lap_nilai_siswa_walkel(){
    if($this->session->userdata('akses')=='3'){
    $id = $this->session->userdata('ses_id'); 
    $query['datanya'] = $this->M_dashboard->nilai_walkel($id)->result();
    $query['mapel'] = $this->M_dashboard->mapel($id)->result();
    $query['walkel'] = $this->M_dashboard->kelas_guru($id)->result();
    // $data = $this->M_dashboard->nilai($id);
    //   $dt=$data->row_array();
    $this->load->view('laporan/laporan_nilai_siswa_walkel',$query);
  }else{
      $this->load->view('kesalahan');
    }
  }

   public function lap_nilai_siswa_walkel_per()
   {  
    if($this->session->userdata('akses')=='3'){
    $id = $this->uri->segment(4);
    $query['datanya'] = $this->M_dashboard->nilai_diguru($id)->row_array();  
    $this->load->view('laporan/laporan_nilai_siswa_guru2',$query);
  }else{
      $this->load->view('kesalahan');
    }
  }

   public function lap_nilai_siswa_walkel_persiswa(){
    if($this->session->userdata('akses')=='3'){
    $id = $this->uri->segment(4);
    $query['data'] = $this->M_dashboard->siswa($id)->row_array();
     $query['kls'] = $this->M_dashboard->datakelas($id)->row_array();
   
    $query['datanya'] = $this->M_dashboard->cetakraport($id)->result();
    $query['absen'] = $this->M_dashboard->absensi1($id)->row_array();
    $data = $this->M_dashboard->cetakraport($id);
    $dt=$data->row_array();
    $this->load->view('laporan/laporan_nilai_siswa_walkel_per',$query);
  }else{
      $this->load->view('kesalahan');
    }
  }


  public function lap_nilai_persiswa(){
    if($this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='3'){
    $id = $this->uri->segment(4);
    $query['datanya'] = $this->M_dashboard->nilai_diguru($id)->row_array();
    $this->load->view('laporan/laporan_nilai_siswa_guru2',$query);
  }else{
      $this->load->view('kesalahan');
    }
  }

  public function lap_nilai_persiswa_walkel(){
    if($this->session->userdata('akses')=='3'){
    $id = $this->uri->segment(4);
    $query['datanya'] = $this->M_dashboard->nilai_diguru($id)->row_array();
    $this->load->view('laporan/laporan_nilai_siswa_guru2',$query);
  }else{
      $this->load->view('kesalahan');
    }
  }
	
  public function lap_absen_persiswa(){
    if($this->session->userdata('akses')=='3'){
    $id = $this->uri->segment(4);
    $query['datanya'] = $this->M_dashboard->edit_absensi($id)->row_array();
    $this->load->view('laporan/laporan_absen',$query);

  }else{
      $this->load->view('kesalahan');
    }
  }

    public function lap_absen(){
    if($this->session->userdata('akses')=='3'){
    $id = $this->session->userdata('ses_id'); 
    $query['datanya'] = $this->M_dashboard->nilai_walkel($id)->result();
    $query['absen'] = $this->M_dashboard->absensi($id)->result();
    $data = $this->M_dashboard->nilai($id);
      $dt=$data->row_array();
    $this->load->view('laporan/laporan_absen_all',$query);
  }else{
      $this->load->view('kesalahan');
    }
  }

}
