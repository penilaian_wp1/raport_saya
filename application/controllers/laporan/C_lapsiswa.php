<?php 

class C_lapsiswa extends CI_controller{

	public function __construct(){
		parent::__construct();
			// if($this->session->userdata('udhmasuk') != TRUE){
	  //           $url=base_url();
	  //           redirect($url);
	  //       }
			// elseif($this->session->userdata('ses_id') && $this->session->userdata('akses')=='2') {
			// 	redirect(base_url('guru/C_dashboard'));	
			// }
      $this->load->model('siswa/M_dashboard');
			// $this->load->library('form_validation');
	}

  public function index(){
    if($this->session->userdata('akses')=='4'){
    $id = $this->session->userdata('ses_id'); 
    $query['datanya'] = $this->M_dashboard->nilai($id)->result();
    $data = $this->M_dashboard->nilai($id);
      $dt=$data->row_array();

    $this->session->set_userdata('ses_nama',$dt['nama_siswa']);
    $this->session->set_userdata('ses_kelas',$dt['nm_kelas']);
    $this->session->set_userdata('ses_thnajrn',$dt['thn_ajaran']);
    $this->load->view('laporan/laporan_nilai_siswa',$query);
  }else{
      $this->load->view('kesalahan');
    }
  }


  
  public function lap_nilai_permapel(){
    if($this->session->userdata('akses')=='4'){
    $id = $this->uri->segment(4);
    $query['datanya'] = $this->M_dashboard->nilai_diguru($id)->row_array();
    $this->load->view('laporan/laporan_nilai_siswa2',$query);
  }else{
      $this->load->view('kesalahan');
    }
  }
	
}
