<?php 

class C_dashboard extends CI_controller{

	public function __construct(){
		parent::__construct();
			if($this->session->userdata('ses_id') && $this->session->userdata('akses')=='2') {
				redirect(base_url('guru/C_dashboard'));	
			}
			elseif($this->session->userdata('ses_id') && $this->session->userdata('akses')=='3') {
				redirect(base_url('guru/C_dashboard'));	
			}
			elseif($this->session->userdata('ses_id') && $this->session->userdata('akses')=='4') {
				redirect(base_url('siswa/C_dashboard'));	
			}
			elseif($this->session->userdata('udhmasuk') != TRUE){
	            $url=base_url();
	            redirect($url);
	        }
			$this->load->model('admin/M_dashboard');
	}

	public function index(){
		$query['data'] 			= $this->M_dashboard->data_user();
		$query['total_siswa'] 	= $this->M_dashboard->hitungjumlah_siswa();
		$query['total_guru'] 	= $this->M_dashboard->hitungjumlah_guru();
		$query['total_kelas'] 	= $this->M_dashboard->hitungjumlah_kelas();
		$query['total_mapel'] 	= $this->M_dashboard->hitungjumlah_mapel();
		$query['dataguru'] 		= $this->M_dashboard->query_guru()->result();
		$query['datasiswa']		= $this->M_dashboard->query_siswa()->result();
		$query['datamapel']		= $this->M_dashboard->query_mapel()->result();
		$query['datakelas']		= $this->M_dashboard->query_kelas()->result();
		$this->load->view('admin/dashboard_admin',$query);
	}

	public function keluar(){
		session_destroy();
		redirect(base_url());
	}

}
