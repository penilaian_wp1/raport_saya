<?php 

class C_kelassiswa extends CI_controller{
    private $filename = "import_data"; // Kita tentukan nama filenya
	public function __construct(){
		parent::__construct();
			if($this->session->userdata('ses_id') && $this->session->userdata('akses')=='2') {
				redirect(base_url('guru/C_dashboard'));	
            }
            elseif($this->session->userdata('ses_id') && $this->session->userdata('akses')=='3') {
				redirect(base_url('guru/C_dashboard'));	
			}
			elseif($this->session->userdata('ses_id') && $this->session->userdata('akses')=='4') {
				redirect(base_url('siswa/C_dashboard'));	
			}
            elseif($this->session->userdata('udhmasuk') != TRUE){
	            $url=base_url();
	            redirect($url);
	        }
            // $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
			$this->load->model('admin/M_kelassiswa');
	}

	public function index(){
        $datakelassiswa['datanya'] = $this->M_kelassiswa->query_tampilkelassiswa()->result();
        $datakelassiswa['datasiswa'] = $this->M_kelassiswa->query_tampilsiswa()->result();
        $datakelassiswa['datakelas'] = $this->M_kelassiswa->query_tampilkelas()->result();
        $this->load->view('admin/data_kelassiswa',$datakelassiswa);
	}
  
	public function input(){
		$this->M_kelassiswa->tambah();
        $this->session->set_flashdata('sukses', '<div class="alert alert-success alert-dismissible">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    <h4><i class="icon fa fa-check"></i> SUKSES TAMBAH DATA</h4>
                                                    Data berhasil ditambahkan.
                                                </div>');
        redirect(base_url('admin/C_kelassiswa'));
    }

    function edit($id){
        $where = array('kd_datakelas' => $id);
        $data['user'] = $this->M_kelassiswa->edit_data($where,'tbl_datakelassiswa')->result();
        $this->load->view('admin/data_guru_edit',$data);
    }

	function update(){
        $this->M_kelassiswa->update_data();
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    <h4><i class="icon fa fa-check"></i> SUKSES UBAH</h4>
                                                   Data berhasil diubah.</div>');
		redirect(base_url('admin/C_kelassiswa'));
	}
    
    function hapus($id){
        $where = array('kd_datakelas' => $id);
        $this->M_kelassiswa->hapus_data($where,'tbl_datakelassiswa');
        $this->session->set_flashdata('hapus', '<div class="alert alert-success alert-dismissible">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                                    </button>
                                                    <h4>
                                                        <i class="icon fa fa-check">
                                                        </i> SUKSES HAPUS
                                                    </h4>
                                                    Data berhasil dihapus dari database.
                                                </div>');
        redirect(base_url('admin/C_kelassiswa'));
	}

    public function form(){
        $data = array(); // Buat variabel $data sebagai array
        
        if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
            // lakukan upload file dengan memanggil function upload yang ada di SiswaModel.php
            $upload = $this->M_kelassiswa->upload_file($this->filename);
            
            if($upload['result'] == "success"){ // Jika proses upload sukses
                // Load plugin PHPExcel nya
                include APPPATH.'third_party/PHPExcel/PHPExcel.php';
                
                $excelreader = new PHPExcel_Reader_Excel2007();
                $loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang tadi diupload ke folder excel
                $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true, true, true, true, true, true, true, true);
                
                // Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
                // Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
                $data['sheet'] = $sheet; 
            }else{ // Jika proses upload gagal
                $data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
            }
        }
        
        $this->load->view('admin/tes', $data);
    }
    
    public function import(){
        // Load plugin PHPExcel nya
        include APPPATH.'third_party/PHPExcel/PHPExcel.php';
        
        $excelreader = new PHPExcel_Reader_Excel2007();
        $loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang telah diupload ke folder excel
        $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true, true, true, true, true, true, true, true);
        
        // Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
        $data = array();
        
        $numrow = 1;
        foreach($sheet as $row){
            // Cek $numrow apakah lebih dari 1
            // Artinya karena baris pertama adalah nama-nama kolom
            // Jadi dilewat saja, tidak usah diimport
            if($numrow > 1){
                // Kita push (add) array data ke variabel data
                array_push($data, array(
                    'email'=>$row['H'], // Insert data alamat dari kolom D di excel
                    'password'=> md5($row['I']), // Insert data alamat dari kolom D di excel
                    'nip'=>$row['A'], // Insert data nis dari kolom A di excel
                    'nama'=>$row['B'], // Insert data nama dari kolom B di excel
                    'tempat_lahir'=>$row['C'], // Insert data jenis kelamin dari kolom C di excel
                    'tanggal_lahir'=>$row['D'], // Insert data alamat dari kolom D di excel
                    'jekel'=>$row['E'], // Insert data alamat dari kolom D di excel
                    'agama'=>$row['F'], // Insert data alamat dari kolom D di excel
                    'alamat'=>$row['J'], // Insert data alamat dari kolom D di excel
                    'telp'=>$row['G'], // Insert data alamat dari kolom D di excel
                    'status'=>$row['K']
                ));
            }
            
            $numrow++; // Tambah 1 setiap kali looping
        }

        // Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
        $this->M_kelassiswa->insert_multiple($data);
        
        redirect(base_url('admin/C_kelassiswa'));
    }

    public function keluar(){
        session_destroy();
        redirect(base_url());
    }

    // public function uploaddata(){
    //     $msg    = $this->uri->segment(3);
    //     $alert  = '';
    //     if($msg == 'success'){
    //         $alert  = 'Success!!';
    //     }
    //     $data['_alert'] = $alert;
    //     $data['siswa'] = $this->M_dataguru->tampilkan();
    //     $this->load->view('admin/import_data',$data);
    // }

    // public function upload() {
    //     $fileName = time() . $_FILES['fileImport']['name'];                     // Sesuai dengan nama Tag Input/Upload
    //     $config['upload_path'] = './fileExcel/';                                // Buat folder dengan nama "fileExcel" di root folder
    //     $config['file_name'] = $fileName;
    //     $config['allowed_types'] = 'xls|xlsx|csv';
    //     $config['max_size'] = 10000;
    //     $this->load->library('upload');
    //     $this->upload->initialize($config);
    //     if (!$this->upload->do_upload('fileImport'))
    //         $this->upload->display_errors();
    //     $media = $this->upload->data('fileImport');
    //     $inputFileName = './fileExcel/' . $media['file_name'];
    //     try {
    //         $inputFileType = IOFactory::identify($inputFileName);
    //         $objReader = IOFactory::createReader($inputFileType);
    //         $objPHPExcel = $objReader->load($inputFileName);
    //     } catch (Exception $e) {
    //         die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
    //     }
    //     $sheet = $objPHPExcel->getSheet(0);
    //     $highestRow = $sheet->getHighestRow();
    //     $highestColumn = $sheet->getHighestColumn();
    //     for ($row = 2; $row <= $highestRow; $row++) {                           // Read a row of data into an array                 
    //         $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            
    //         $data = array(                                                      // Sesuaikan sama nama kolom tabel di database
    //             "nama" => $rowData[0][1],
    //             "alamat" => $rowData[0][2],
    //             "kontak" => $rowData[0][3]
    //         );
            
    //         $insert = $this->db->insert("siswa", $data);                   // Sesuaikan nama dengan nama tabel untuk melakukan insert data
    //         delete_files($media['file_path']);                                  // menghapus semua file .xls yang diupload
    //     }
        
    //     redirect(base_url('admin/C_dataguru'));
    // }
    // public function edit($nip)
    // {
    //     $nip = $this->uri->segment(3);

    //     $data['datauser'] = array(

    //         'title'     => 'Edit Data Buku',
    //         'datauser' => $this->M_dataguru->edit($nip)

    //     );

    //     $this->load->view('v_edit', $data);
    // }

    // public function update()
    // {
    //     $id['nip'] = $this->input->post("nip");
    //     $nip = $this->input->post('nip');
    //     $nama = $this->input->post('nama');
    //     $tmptlahir = $this->input->post('tempatlahir');
    //     $tgllahir = $this->input->post('tanggallahir');
    //     $jekel = $this->input->post('jekel');
    //     $agama = $this->input->post('agama');
    //     $alamat = $this->input->post('alamat');
    //     $telpon = $this->input->post('telp');
    //     $email = $this->input->post('email');
    //     $password = md5($this->input->post('password'));

    //     $data = array(
    //         'email'         => $email,
    //         'password'      => $password,
    //         'nama' 			=> $nama,
    //         'tempat_lahir' 	=> $tmptlahir,
    //         'tanggal_lahir' => $tgllahir,
    //         'jekel' 		=> $jekel,
    //         'agama' 		=> $agama,
    //         'alamat' 		=> $alamat,
    //         'telp'   		=> $telpon,
    //         'status'        => 'GURU'
    //     );

    //     $this->M_dataguru->update($data, $id);

    //     $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"> Success! data berhasil diupdate didatabase.
    //                                             </div>');

    //     //redirect
    //     redirect(base_url('admin/C_dataguru'));

    // }
    // public function edit(){
    //     $id = $this->uri->segment(3);
    //     $data['user'] = $this->M_dataguru->edit($id)->result();
    //     $this->load->view('admin/data_guru_edit', $data);
    // }

    // public function update(){
    //     $id = $this->input->post('nip');
        
    //     $nip = $this->input->post('nip');
    //     $nama = $this->input->post('nama');
    //     $tmptlahir = $this->input->post('tempatlahir');
    //     $tgllahir = $this->input->post('tanggallahir');
    //     $jekel = $this->input->post('jekel');
    //     $agama = $this->input->post('agama');
    //     $alamat = $this->input->post('alamat');
    //     $telpon = $this->input->post('telp');
    //     $email = $this->input->post('email');
    //     $password = md5($this->input->post('password'));

    //     $data = array(
    //         'email'         => $email,
    //         'password'      => $password,
    //         'nama' 			=> $nama,
    //         'tempat_lahir' 	=> $tmptlahir,
    //         'tanggal_lahir' => $tgllahir,
    //         'jekel' 		=> $jekel,
    //         'agama' 		=> $agama,
    //         'alamat' 		=> $alamat,
    //         'telp'   		=> $telpon,
    //         'status'        => 'GURU'
    //     );
        
    //     $proses = $this->M_dataguru->update($id, $data);
    //     redirect(base_url('admin/C_dataguru'));
    // }
    // public function update($id=''){
    //     if(empty($id)) redirect(base_url('admin/C_dataguru'));
          
    //     $nip = $this->input->post('nip');
    //     $nama = $this->input->post('nama');
    //     $tmptlahir = $this->input->post('tempatlahir');
    //     $tgllahir = $this->input->post('tanggallahir');
    //     $jekel = $this->input->post('jekel');
    //     $agama = $this->input->post('agama');
    //     $alamat = $this->input->post('alamat');
    //     $telpon = $this->input->post('telp');
    //     $email = $this->input->post('email');
    //     $password = md5($this->input->post('password'));

    //     $data = array(
    //         'email'         => $email,
    //         'password'      => $password,
    //         'nama' 			=> $nama,
    //         'tempat_lahir' 	=> $tmptlahir,
    //         'tanggal_lahir' => $tgllahir,
    //         'jekel' 		=> $jekel,
    //         'agama' 		=> $agama,
    //         'alamat' 		=> $alamat,
    //         'telp'   		=> $telpon,
    //         'status'        => 'GURU'
    //     );

    //     $sql=$this->db->update('tbl_karyawan',$data,array('nip'=>$id));
    //     if($sql){
    //     $this->session->set_flashdata('pesan','Data Berhasil Di Update'); 
    //     redirect(base_url('admin/C_dataguru'));
    //     }else{
    //     buat_aleert('GAGAL');
    //     } 
    // }

    // public function edit() {
    //     if ($this->uri->segment(3) != null) {
    //         $id = $this->uri->segment(3);

    //         $where = array('nip' => $id);

    //         $layout['user'] = $this->M_dataguru->edit_data($where,'tbl_karyawan')->result();
    //         $this->load->view('admin/data_guru_edit', $layout);
    //     }
    //     else{
    //         redirect(base_url('admin/C_dataguru'));
    //     }
    // }

    // public function update($id) {
    //     $nip = $this->input->post('nip');
    //     $nama = $this->input->post('nama');
    //     $tmptlahir = $this->input->post('tempatlahir');
    //     $tgllahir = $this->input->post('tanggallahir');
    //     $jekel = $this->input->post('jekel');
    //     $agama = $this->input->post('agama');
    //     $alamat = $this->input->post('alamat');
    //     $telpon = $this->input->post('telp');
    //     $email = $this->input->post('email');
	// 	$password = md5($this->input->post('password'));

    //     $kondisi = array('nip' => $id);
    //     $data_update = array(
    //         'email'         => $email,
    //         'password'      => $password,
	// 		'nama' 			=> $nama,
	// 		'tempat_lahir' 	=> $tmptlahir,
	// 		'tanggal_lahir' => $tgllahir,
	// 		'jekel' 		=> $jekel,
	// 		'agama' 		=> $agama,
	// 		'alamat' 		=> $alamat,
    //         'telp'   		=> $telpon,
    //         'status'        => 'GURU'
	// 	);
    //     if ($this->M_dataguru->update($kondisi, $data_update)) {
    //         $this->session->set_flashdata('notif',
    //                                                 '<div class="alert alert-success" role="alert">Data Berhasil diubah 
    //                                                     <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
    //                                                     </button>
    //                                                 </div>');
    //     } else {
    //         echo "gagal mengubah data";
    //     }
    // }

    // function edit($id){
	// 	$where = array('nip' => $id);
	// 	$data['user'] = $this->M_dataguru->edit_data($where,'tbl_karyawan')->result();
	// 	$this->load->view('v_edit',$data);
    // }
    
	// public function edit(){
    //     $this->load->view('admin/data_guru_edit');
    //     // $this->M_dataguru->update();
    //     //
    //     // redirect(base_url('admin/C_dataguru'));
    // }

    // function update_data($id_nim){
    //     if($this->input->post('submit')){
    //      $this->M_dataguru->update($id_nim);
    //      redirect(base_url('admin/C_dataguru'));
    //     }
    //     $data['hasil']=$this->M_dataguru->getById($id_nim);
    //     $this->load->view('admin/data_guru_edit',$data);
    //    }

    // public function update(){
    //     $nip = $this->input->post('nip');
    //     $nama = $this->input->post('nama');
    //     $tmptlahir = $this->input->post('tempatlahir');
    //     $tgllahir = $this->input->post('tanggallahir');
    //     $jekel = $this->input->post('jekel');
    //     $agama = $this->input->post('agama');
    //     $alamat = $this->input->post('alamat');
    //     $telpon = $this->input->post('telp');
    //     $email = $this->input->post('email');
    //     $password = md5($this->input->post('password'));

	// 	$data = array(
    //         'email'         => $email,
    //         'password'      => $password,
	// 		'nama' 			=> $nama,
	// 		'tempat_lahir' 	=> $tmptlahir,
	// 		'tanggal_lahir' => $tgllahir,
	// 		'jekel' 		=> $jekel,
	// 		'agama' 		=> $agama,
	// 		'alamat' 		=> $alamat,
    //         'telp'   		=> $telpon,
    //         'status'        => 'GURU'
    //     );
		
        
    //     $where = array(
    //         'nip' => $nip
    //     );
     
    //     $this->M_dataguru->update_data($where,$data,'tbl_karyawan');

    //     // $this->session->set_flashdata('notif',
    //     // '<div class="alert alert-success" role="alert">Data Berhasil diubah 
    //     //     <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
    //     //     </button>
    //     // </div>');
    //     redirect(base_url('admin/C_dataguru'));
    //     //$this->db->update($this->_table, $this, array('nip' => $post['NIP']));
    // }

    // public function update($id){
    // 	$validation = $this->form_validation;
    //     $validation->set_rules($model->rules());
    //     if (!$data["datanya"]) show_404();
    //     if ($validation->run()) {
    //         $modal->update();
    //         $this->session->set_flashdata('success', 'Berhasil disimpan');
    //         redirect(base_url('admin/C_dataguru'));
    //     }
    // }

    // public function delete($id=null){
    //     if (!isset($id)) show_404();
        
    //     if ($this->M_dataguru->delete($id)) {
    //         redirect(base_url('admin/C_dataguru'));
    //     }
    // }
}
