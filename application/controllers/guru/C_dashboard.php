<?php 

class C_dashboard extends CI_controller{

	public function __construct(){
		parent::__construct();
		if($this->session->userdata('udhmasuk') != TRUE){
			$url=base_url();
			redirect($url);
			}
		
// if($this->session->userdata('ses_id') && $this->session->userdata('akses')=='1') {
			// 	redirect(base_url('admin/C_dashboard'));	
			// }
			// // elseif($this->session->userdata('ses_id') && $this->session->userdata('akses')=='2') {
			// // 	redirect(base_url('guru/C_dashboard'));	
			// // }
			// else{
		 //      echo "Anda tidak berhak mengakses halaman ini";
		 //    }
			// elseif($this->session->userdata('ses_id') && $this->session->userdata('akses')=='2') {
			// 	redirect(base_url('guru/C_dashboard'));	
			// }// if(!$this->session->userdata('nama')|| !$this->session->userdata('username')) {
			// 	redirect('C_login');	
			// }
			$this->load->model('guru/M_dashboard');
	}

	public function index(){
		$id = $this->session->userdata('ses_id'); 
		$query['datanya'] = $this->M_dashboard->nilai($id)->result();
		$query['mapel'] = $this->M_dashboard->mapel($id)->result();
		$query['walkel'] = $this->M_dashboard->kelas_guru($id)->result();
		$data = $this->M_dashboard->nilai($id);
	    $dt=$data->row_array();
		$this->session->set_userdata('ses_mapel',$dt['nm_mapel']);
		$this->load->view('guru/halaman_guruu', $query);
	}

	public function walikelas(){
		if($this->session->userdata('akses')=='3'){
		$id = $this->session->userdata('ses_id'); 
		$query['datanya'] = $this->M_dashboard->nilai_walkel($id)->result();
		$query['absen'] = $this->M_dashboard->absensi($id)->result();
		$query['nm'] = $this->M_dashboard->kelas($id)->result();
		$ed = $this->uri->segment(4);
		$query['data'] = $this->M_dashboard->edit_absensi($ed)->row_array();
		$query['walkel'] = $this->M_dashboard->kelas_guru($id)->result();
		$data = $this->M_dashboard->nilai($id);
	    $dt=$data->row_array();
		$this->session->set_userdata('ses_mapel',$dt['nm_mapel']);
      	$this->load->view('guru/halaman_walkel', $query);
    }else{
      $this->load->view('kesalahan');
    }
		
	}
	public function profil(){
		if($this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='3'){
      $this->load->view('guru/halaman_profil_guru');
    }else{
      $this->load->view('kesalahan');
    }
	}

	public function edit_nilai($nis){
		$id = $this->uri->segment(4);
		$query['datanya'] = $this->M_dashboard->nilai_diguru($id)->row_array();
		$this->load->view('guru/halaman_guru_input',$query);
	}

public function edit_nilai_walkel($nis){
		$id = $this->uri->segment(4);
		$query['datanya'] = $this->M_dashboard->nilai_diguru($id)->row_array();
		$this->load->view('guru/content_walkel',$query);
	}

	public function edit_simpan_nilai(){
    $id             = $this->input->post('id');
     $data    = array(
        'tugas1'     =>$this->input->post('tugas1'),
        'tugas2'   =>$this->input->post('tugas2'),
        'tugas3'  =>$this->input->post('tugas3'),
        'ulangan1'  =>$this->input->post('ulangan1'),
        'ulangan2'  =>$this->input->post('ulangan2'),
        'uts'  =>$this->input->post('uts'),
        'uas'  =>$this->input->post('uas'));
    $this->db->where('kd_nilai',$id);
    $this->db->update('tbl_nilai',$data);
    redirect('guru/C_dashboard');
}

public function edit_simpan_nilai_walkel(){
    if($this->session->userdata('akses')=='3'){
    $id             = $this->input->post('id');
     $data    = array(
        'tugas1'     =>$this->input->post('tugas1'),
        'tugas2'   =>$this->input->post('tugas2'),
        'tugas3'  =>$this->input->post('tugas3'),
        'ulangan1'  =>$this->input->post('ulangan1'),
        'ulangan2'  =>$this->input->post('ulangan2'),
        'uts'  =>$this->input->post('uts'),
        'uas'  =>$this->input->post('uas'));
    $this->db->where('kd_nilai',$id);
    $this->db->update('tbl_nilai',$data);
    redirect('guru/C_dashboard/walikelas');
    }else{
      $this->load->view('kesalahan');
    }
}

public function edit_absen($nis){
		$id = $this->uri->segment(4);
		$query['datanya'] = $this->M_dashboard->edit_absensi($id)->row_array();
		$this->load->view('guru/content_walkel',$query);
	}

public function edit_simpan_absen(){
    $id             = $this->input->post('id');
     $data    = array(
        'sakit'     =>$this->input->post('sakit'),
        'ijin'   =>$this->input->post('ijin'),
        'tanpa_ket'  =>$this->input->post('tanpa_ket'));
    $this->db->where('kd_absen',$id);
    $this->db->update('tbl_absen',$data);
    redirect('guru/C_dashboard/walikelas#edit');
}


	public function keluar(){
		session_destroy();
		redirect(base_url('C_login'));
	}

}
