<?php 
class C_login extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('M_login');

		if($this->session->userdata('akses')=='1') {
			echo "Anda harus lougout dulu";
			redirect(base_url('admin/C_dashboard'));
		}
		elseif($this->session->userdata('akses')=='2') {
			echo "Anda harus lougout dulu";
			redirect(base_url('guru/C_dashboard'));
		}
		
	}

	public function index(){
		$this->load->view('halaman_awal');
	}
	 
	public function login(){
		$this->load->view("login");
	}

	function proses_login(){
 		$username=htmlspecialchars($this->input->post('username',TRUE),ENT_QUOTES);
 		$password=htmlspecialchars($this->input->post('password',TRUE),ENT_QUOTES);
	
		$cek_login = $this->M_login->query_proses_login($username,$password);
		$cek_siswa = $this->M_login->query_login_siswa($username,$password);

		$data= $cek_login->row_array();
		$this->session->set_userdata('udhmasuk',TRUE);

		if ($cek_login->num_rows() > 0) {
         	if($data['status']=='admin'){ 
	            $this->session->set_userdata('akses','1');
	            $this->session->set_userdata('ses_id',$data['nip']);
	            $this->session->set_userdata('ses_nama',$data['nama']);
                $this->session->set_userdata('ses_tmpt_lahir',$data['tempat_lahir']);
                $this->session->set_userdata('ses_tgl_lahir',$data['tanggal_lahir']);
                $this->session->set_userdata('ses_agama',$data['agama']);
                $this->session->set_userdata('ses_jekel',$data['jekel']);
                $this->session->set_userdata('ses_alamat',$data['alamat']);
                $this->session->set_userdata('ses_email',$data['email']);
                $this->session->set_userdata('ses_telp',$data['telp']);
                $this->session->set_userdata('ses_status',$data['status']);
                redirect(base_url('admin/C_dashboard'));
         	}
         	else if($data['status']=='guru'){ 
                $this->session->set_userdata('akses','2');
                $this->session->set_userdata('ses_id',$data['nip']);
                $this->session->set_userdata('ses_nama',$data['nama']);
                $this->session->set_userdata('ses_tmpt_lahir',$data['tempat_lahir']);
                $this->session->set_userdata('ses_tgl_lahir',$data['tanggal_lahir']);
                $this->session->set_userdata('ses_agama',$data['agama']);
                $this->session->set_userdata('ses_jekel',$data['jekel']);
                $this->session->set_userdata('ses_alamat',$data['alamat']);
                $this->session->set_userdata('ses_email',$data['email']);
                $this->session->set_userdata('ses_telp',$data['telp']);
                $this->session->set_userdata('ses_status',$data['status']);
                redirect(base_url('guru/C_dashboard'));
         	}
         	else if($data['status']=='walikelas'){ 
                $this->session->set_userdata('akses','3');
                $this->session->set_userdata('ses_id',$data['nip']);
                $this->session->set_userdata('ses_nama',$data['nama']);
                $this->session->set_userdata('ses_tmpt_lahir',$data['tempat_lahir']);
                $this->session->set_userdata('ses_tgl_lahir',$data['tanggal_lahir']);
                $this->session->set_userdata('ses_agama',$data['agama']);
                $this->session->set_userdata('ses_jekel',$data['jekel']);
                $this->session->set_userdata('ses_alamat',$data['alamat']);
                $this->session->set_userdata('ses_email',$data['email']);
                $this->session->set_userdata('ses_telp',$data['telp']);
                $this->session->set_userdata('ses_status',$data['status']);
                redirect(base_url('guru/C_dashboard'));
         	}
        }
        else { //jika login sebagai siswa
			if($cek_siswa->num_rows() > 0){
     			$data=$cek_siswa->row_array();
	            $this->session->set_userdata('akses','4');
	            $this->session->set_userdata('ses_id',$data['nis']);
	            $this->session->set_userdata('ses_nama',$data['nama']);
                $this->session->set_userdata('ses_tmpt_lahir',$data['tempat_lahir']);
                $this->session->set_userdata('ses_tgl_lahir',$data['tanggal_lahir']);
                $this->session->set_userdata('ses_agama',$data['agama']);
                $this->session->set_userdata('ses_jekel',$data['jekel']);
                $this->session->set_userdata('ses_alamat',$data['alamat']);
                $this->session->set_userdata('ses_email',$data['email']);
                $this->session->set_userdata('ses_telp',$data['telp']);
                $this->session->set_userdata('ses_status',$data['status']);
                redirect(base_url('siswa/C_dashboard'));
         	}
         	else { // jika username dan password tidak ditemukan atau salah
				$url=base_url();
				echo $this->session->set_flashdata('msg','Username Atau Password Salah / belum diisi');
				redirect($url);
        	} 
		}
    }


     // //    else{ //jika login sebagai siswa
     // //        $cek_siswa=$this->M_login->proses_login_siswa($username,$password);
     // //        if($cek_siswa->num_rows() > 0){
	 			// // $data=$cek_siswa->row_array();
	    // //         $this->session->set_userdata('masuk',TRUE);
	    // //         $this->session->set_userdata('akses','3');
	    // //         $this->session->set_userdata('ses_id',$data['nis']);
	    // //         $this->session->set_userdata('ses_nama',$data['nama']);
	    // //         $this->session->set_userdata('ses_semester',$data['semester']);
	    // //         $this->session->set_userdata('ses_tmpt_lahir',$data['tempat_lahir']);
	    // //         $this->session->set_userdata('ses_tgl_lahir',$data['tanggal_lahir']);
	    // //         $this->session->set_userdata('ses_agama',$data['agama']);
	    // //         $this->session->set_userdata('ses_jekel',$data['jekel']);
	    // //         $this->session->set_userdata('ses_alamat',$data['alamat']);
	    // //         $this->session->set_userdata('ses_email',$data['email']);
	    // //         $this->session->set_userdata('ses_telp',$data['telp']);
	    // //         $this->session->set_userdata('ses_status',$data['status']);
     // //            redirect('C_dashboard/siswa');
     // //        }else{  // jika username dan password tidak ditemukan atau salah
     // //            $url=base_url();
     // //            echo $this->session->set_flashdata('msg','Username Atau Password Salah');
     // //            redirect($url);
     // //        }
    	// }
 
 	

   	

    // public function proses_login(){
	// 	$data=array(
	// 		'username' 	=> $this->security->xss_clean($this->input->post('Username')),
	// 		'password' 	=> md5($this->security->xss_clean($this->input->post('Password')))
	// 	);
	//  	$query = $this->M_login->proses_login($data);

	//  	// ngecek data atau tidak kalo 1 true, 0 false
	// 	//echo $query->num_rows();
		 
	// 	if ($query->num_rows() > 0) {
	// 	// jika berhasil

	// 	 	$data_login = $query->row_array();
	// 	 	$data_sesi = array(
	// 	 		'nama'		=> $data_login['nama'],
	// 	 		'username'	=> $data_login['username'],
	// 	 	);

	// 	 	$this->session->set_userdata($data_sesi);
	// 	 	// $this->session->flashdata('message');
	// 	 	//echo print_r($this->session->userdata());

	// 	 	redirect (base_url('C_dashboard'));
	// 	 	}else{
	// 	 		echo "<script>alert('Username atau password salah');</script>";
	// 	 	}
	//  	}
 // 	function proses_daftar(){
	// 	$data=array(
	// 		'nama'		=> $this->input->post('Nama'),
	// 		'username' 	=> $this->input->post('Username'),
	// 		'password' 	=> md5($this->input->post('Password')),
	// 	);
	// 	$this->M_login->proses_daftar($data);
	// 	redirect('C_login');
	// }
	// public function daftar(){

	// 	$this->load->view("daftar");
	// }
	
	// public function halaman_guru() {
	// 	$this->load->view("halaman_guru");
	// }

	// public function siswa() {
	// 	$this->load->view("dashboard_siswa");
	// }

	// public function walikelas() {
	// 	$this->load->view("dashboard_walikelas");
	// }
	// public function proses_daftar(){
	// 	$data=array(
	// 		'nama'		=> $this->input->post('Nama'),
	// 		'username' 	=> $this->input->post('Username'),
	// 		'password' 	=> md5($this->input->post('Password')),
	// 	);
	// 	$this->M_login->proses_daftar($data);
	// 	redirect('C_login');
	// }
}

	