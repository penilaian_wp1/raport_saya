<?php 

class C_dashboard extends CI_controller{

	public function __construct(){
		parent::__construct();
		if($this->session->userdata('udhmasuk') != TRUE){
			$url=base_url();
			redirect($url);
			}
		// // if(!$this->session->userdata('nama')|| !$this->session->userdata('username')) {
		// 	redirect('C_login');	
		// }
		$this->load->model('siswa/M_dashboard');
	}

	public function index(){
		$id = $this->session->userdata('ses_id'); 
		$query['datanya'] = $this->M_dashboard->nilai($id)->result();
		$data = $this->M_dashboard->nilai($id);
	    $dt=$data->row_array();
		$this->session->set_userdata('ses_kelas',$dt['nm_kelas']);
		$this->session->set_userdata('ses_thnajrn',$dt['thn_ajaran']);
		$this->load->view('siswa/halaman_siswaa', $query);
		
}
		
	
	public function profil(){
		if($this->session->userdata('akses')=='4'){
      $this->load->view('siswa/halaman_profil_siswa');
    }else{
      $this->load->view('kesalahan');
    }
	}
	public function keluar(){
		session_destroy();
		redirect(base_url('C_login'));
	}

}
