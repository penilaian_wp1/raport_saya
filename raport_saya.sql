-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 24 Des 2018 pada 09.37
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `raport_saya`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_absen`
--

CREATE TABLE `tbl_absen` (
  `kd_absen` int(11) NOT NULL,
  `kd_datakelas` varchar(10) NOT NULL,
  `sakit` varchar(3) NOT NULL,
  `ijin` varchar(3) NOT NULL,
  `tanpa_ket` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_absen`
--

INSERT INTO `tbl_absen` (`kd_absen`, `kd_datakelas`, `sakit`, `ijin`, `tanpa_ket`) VALUES
(1, 'dk00001', '2', '1', '0'),
(2, 'dk00002', '1', '2', '2'),
(3, 'dk00003', '', '', ''),
(4, 'dk00004', '', '', ''),
(5, 'dk00005', '', '', ''),
(6, 'dk00006', '', '', ''),
(7, 'dk00007', '', '', ''),
(8, 'dk00008', '', '', ''),
(9, 'dk00009', '', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_datakelassiswa`
--

CREATE TABLE `tbl_datakelassiswa` (
  `kd_datakelas` varchar(10) NOT NULL,
  `nis` varchar(10) NOT NULL,
  `kd_kelas` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_datakelassiswa`
--

INSERT INTO `tbl_datakelassiswa` (`kd_datakelas`, `nis`, `kd_kelas`) VALUES
('dk00001', '9999900001', 'k001'),
('dk00002', '9999900002', 'k001'),
('dk00003', '9999900003', 'k002'),
('dk00004', '9999900004', 'k002'),
('dk00005', '9999900005', 'k003'),
('dk00006', '9999900006', 'k003'),
('dk00007', '9999900007', 'k004'),
('dk00008', '9999900008', 'k004'),
('dk00009', '9999900009', 'k005'),
('dk00014', '9999900014', 'k002');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_kelas`
--

CREATE TABLE `tbl_kelas` (
  `kd_kelas` varchar(4) NOT NULL,
  `thn_ajaran` varchar(50) NOT NULL,
  `nm_kelas` varchar(20) NOT NULL,
  `nip` varchar(18) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_kelas`
--

INSERT INTO `tbl_kelas` (`kd_kelas`, `thn_ajaran`, `nm_kelas`, `nip`) VALUES
('k001', '2017/2018', '7(VII) A', '1122330001'),
('k002', '2017/2018', '7(VII) B', '1122330002'),
('k003', '2016/2017', '8(VIII) A', '1122330003'),
('k004', '2016/2017', '8(VIII) B', '1122330004'),
('k005', '2015/2016', '9(IX) A', '1122330005');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_mapel`
--

CREATE TABLE `tbl_mapel` (
  `kd_mapel` varchar(3) NOT NULL,
  `nip` varchar(18) NOT NULL,
  `nm_mapel` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_mapel`
--

INSERT INTO `tbl_mapel` (`kd_mapel`, `nip`, `nm_mapel`) VALUES
('m01', '1122330001', 'Pendidikan Agama Islam'),
('m02', '1122330002', 'Pendidikan Pancasila dan Kewarganegaraan'),
('m03', '1122330003', 'Matematika'),
('m04', '1122330004', 'Bahasa Indonesia'),
('m05', '1122330005', 'Ilmu Pengetahuan Alam'),
('m06', '1122330006', 'Ilmu Pengetahuan Sosial'),
('m07', '1122330007', 'Bahasa Inggris'),
('m08', '1122330009', 'Pendidikan Jasmani dan Rohani'),
('m09', '1122330010', 'Teknologi Informasi dan Komputer'),
('m10', '1122330001', 'Basa Sunda');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_nilai`
--

CREATE TABLE `tbl_nilai` (
  `kd_nilai` varchar(5) NOT NULL,
  `nis` varchar(10) NOT NULL,
  `nip` varchar(18) NOT NULL,
  `kd_kelas` varchar(4) NOT NULL,
  `kd_mapel` varchar(3) NOT NULL,
  `kd_datakelas` varchar(10) NOT NULL,
  `tugas1` float NOT NULL,
  `tugas2` float NOT NULL,
  `tugas3` float NOT NULL,
  `ulangan1` float NOT NULL,
  `ulangan2` float NOT NULL,
  `uts` float NOT NULL,
  `uas` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_nilai`
--

INSERT INTO `tbl_nilai` (`kd_nilai`, `nis`, `nip`, `kd_kelas`, `kd_mapel`, `kd_datakelas`, `tugas1`, `tugas2`, `tugas3`, `ulangan1`, `ulangan2`, `uts`, `uas`) VALUES
('00001', '9999900001', '1122330001', 'k001', 'm01', 'dk00001', 80, 99, 90, 98, 99, 99, 99),
('00002', '9999900001', '1122330003', 'k001', 'm02', 'dk00001', 69, 80, 89, 89, 88, 99, 89),
('00003', '9999900001', '1122330003', 'k001', 'm03', 'dk00001', 80, 98, 79, 89, 90, 87, 77),
('00004', '9999900001', '1122330004', 'k001', 'm04', 'dk00001', 80, 98, 78, 89, 87, 90, 89),
('00005', '9999900001', '1122330005', 'k001', 'm05', 'dk00001', 90, 89, 99, 88, 89, 90, 80),
('00006', '9999900001', '1122330006', 'k001', 'm06', 'dk00001', 68, 78, 89, 90, 89, 78, 0),
('00007', '9999900001', '1122330007', 'k001', 'm07', 'dk00001', 99, 88, 90, 89, 79, 89, 70),
('00008', '9999900001', '1122330009', 'k001', 'm08', 'dk00001', 80, 98, 78, 89, 87, 90, 89),
('00009', '9999900001', '1122330010', 'k001', 'm09', 'dk00001', 80, 89, 70, 80, 80, 80, 80),
('00010', '9999900001', '1122330001', 'k001', 'm10', 'dk00001', 90, 90, 90, 90, 90, 90, 90),
('00011', '9999900002', '1122330001', 'k001', 'm01', 'dk00002', 89, 90, 89, 79, 99, 98, 97),
('00012', '9999900002', '1122330003', 'k001', 'm02', 'dk00002', 68, 78, 89, 88, 87, 89, 90),
('00013', '9999900002', '1122330003', 'k001', 'm03', 'dk00002', 89, 79, 79, 89, 79, 90, 89),
('00014', '9999900002', '1122330004', 'k001', 'm04', 'dk00002', 80, 70, 90, 80, 70, 80, 90),
('00015', '9999900002', '1122330005', 'k001', 'm05', 'dk00002', 89, 79, 78, 68, 69, 69, 79),
('00016', '9999900002', '1122330006', 'k001', 'm06', 'dk00002', 68, 79, 79, 69, 79, 80, 70),
('00017', '9999900002', '1122330007', 'k001', 'm07', 'dk00002', 80, 75, 87, 90, 90, 70, 95),
('00018', '9999900002', '1122330009', 'k001', 'm08', 'dk00002', 80, 90, 80, 70, 70, 80, 70),
('00019', '9999900002', '1122330010', 'k001', 'm09', 'dk00002', 70, 70, 80, 80, 90, 90, 90),
('00020', '9999900002', '1122330001', 'k001', 'm10', 'dk00002', 80, 100, 100, 80, 79, 78, 68),
('00021', '9999900003', '1122330001', 'k002', 'm01', 'dk00003', 90, 79, 79, 79, 80, 80, 90),
('00022', '9999900003', '1122330002', 'k002', 'm02', 'dk00003', 80, 80, 80, 80, 80, 70, 90),
('00023', '9999900003', '1122330003', 'k002', 'm03', 'dk00003', 90, 90, 90, 90, 90, 98, 98),
('00024', '9999900003', '1122330004', 'k002', 'm04', 'dk00003', 98, 87, 78, 67, 67, 78, 78),
('00025', '9999900003', '1122330005', 'k002', 'm05', 'dk00003', 80, 90, 80, 80, 79, 78, 68);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_pegawai`
--

CREATE TABLE `tbl_pegawai` (
  `nip` varchar(18) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `jekel` varchar(10) NOT NULL,
  `agama` varchar(20) NOT NULL,
  `alamat` varchar(150) NOT NULL,
  `email` varchar(50) NOT NULL,
  `foto` varchar(50) NOT NULL,
  `telp` varchar(20) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_pegawai`
--

INSERT INTO `tbl_pegawai` (`nip`, `password`, `nama`, `tanggal_lahir`, `tempat_lahir`, `jekel`, `agama`, `alamat`, `email`, `foto`, `telp`, `status`) VALUES
('1122330001', '3174a97c571288c969e34cb042408e01', 'Saepudin', '1998-02-21', 'Bogor', 'Laki-laki', 'ISLAM', 'Jl. Arzimar 2 RT01/18 Bogor Utara', 'saepulstr@gmail.com', '_MG_54473.JPG', '089662307895', 'Wali Kelas'),
('1122330002', '9c7617f0cc3c81469910c16b684b5f68', 'Amir Mahfud', '1998-04-01', 'Situbondo', 'Laki-laki', 'ISLAM', 'Bukit Cimanggu City Blok AA 10/10 Greenland', 'amirvanmahfud@gmail.com', '', '087737365000', 'Wali Kelas'),
('1122330003', '099a147c0c6bcd34009896b2cc88633c', 'alif', '1998-02-21', 'papua', 'Laki-laki', 'ISLAM', 'Jl. Arzimar 2 RT01/18', 'alif@gmail.com', '', '089662307895', 'Wali Kelas'),
('1122330004', '5fcffe753293baa37f21db3a3eeca373', 'Sahrul', '2018-12-12', 'majenang', 'Laki-laki', 'ISLAM', 'ciluer', 'sahrul@gmail.com', '', '0896767574', 'Wali Kelas'),
('1122330005', '4008c6bb81d28cc63b84e1ee231ab944', 'mei', '1998-02-21', 'Bogor', 'Perempuan', 'ISLAM', 'Jl. Arzimar 2 RT01/18', 'mei@gmail.com', '', '089662307895', 'Wali Kelas'),
('1122330006', '0f72d527e3d66e5a71ea4fe21f0bf56d', 'dwi', '2018-12-12', 'majenang', 'Perempuan', 'ISLAM', 'bogor cimanggu city', 'dwi@gmail.com', '', '0896767574', 'Wali Kelas'),
('1122330007', 'fbfb0e3c85f8a189e36d3f778f0daa95', 'Mia Ulul Azmi', '1998-02-21', 'Bogor', 'Perempuan', 'ISLAM', 'Jl. Arzimar 2 RT01/18', 'alif@gmail.com', '2yam.jpg', '089662307895', 'Guru'),
('1122330009', '64f9fcba33b1be4cc802611697de070b', 'albert', '1998-02-21', 'Bogor', 'Laki-laki', 'ISLAM', 'Jl. Arzimar 2 RT01/18', 'albert@gmail.com', '', '089662307895', 'Guru'),
('1122330010', '3411aabbc5026809efef2684ea3e5480', 'ucok', '2018-12-12', 'majenang', 'Laki-laki', 'ISLAM', 'bogor cimanggu city', 'ucok@gmail.com', '', '0896767574', 'Guru'),
('1122330011', 'ba12a64c1687074bcecbdac228f6f7b4', 'baba', '1998-02-21', 'papua', 'Laki-laki', 'ISLAM', 'Jl. Arzimar 2 RT01/18', 'baba@gmail.com', '', '089662307895', 'Guru'),
('1122330012', '55ce76f63eb3ae24f3899e60d051472f', 'maman', '2018-12-12', 'majenang', 'Laki-laki', 'ISLAM', 'ciluer', 'maman@gmail.com', '', '0896767574', 'Guru'),
('1122330013', '48937f6c5c064c0b2b2636f6862da7e2', 'cipeng', '1998-02-21', 'Bogor', 'Laki-laki', 'ISLAM', 'Jl. Arzimar 2 RT01/18', 'cipeng@gmail.com', '', '089662307895', 'Guru'),
('1122330014', 'c824f58b21be71fbd87d944b6237e2d4', 'nana', '2018-12-12', 'majenang', 'Perempuan', 'ISLAM', 'bogor cimanggu city', 'nana@gmail.com', '', '0896767574', 'Guru'),
('1122330015', '7277daf98539bad42ccc82a0114c32e0', 'elsa', '1998-02-21', 'papua', 'Perempuan', 'ISLAM', 'Jl. Arzimar 2 RT01/18', 'elsa@gmail.com', '', '089662307895', 'Guru'),
('1122330016', '3ebe8bce3e9e91244acba4769aa44153', 'sutisna', '2018-12-12', 'majenang', 'Laki-laki', 'Kong Hu Cu', 'ciluer', 'sutisna@gmail.com', '', '0896767574', 'Guru'),
('admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', '2018-12-12', 'admin', 'Laki-laki', 'ISLAM', 'admin', 'admin', 'admin.png', 'admin', 'Admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_siswa`
--

CREATE TABLE `tbl_siswa` (
  `nis` varchar(10) NOT NULL,
  `nama_siswa` varchar(100) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `tempat_lahir` varchar(30) NOT NULL,
  `jekel` varchar(10) NOT NULL,
  `agama` varchar(20) NOT NULL,
  `alamat` varchar(150) NOT NULL,
  `email` varchar(50) NOT NULL,
  `foto` varchar(50) NOT NULL,
  `telp` varchar(20) NOT NULL,
  `status` varchar(10) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_siswa`
--

INSERT INTO `tbl_siswa` (`nis`, `nama_siswa`, `tanggal_lahir`, `tempat_lahir`, `jekel`, `agama`, `alamat`, `email`, `foto`, `telp`, `status`, `password`) VALUES
('9999900001', 'Epul', '2011-02-21', 'BOGOR', 'Laki-laki', 'ISLAM', 'JL. Arjimar 2 RT 01/18 Tegal Gundil Bogor Utara', 'saepulstr@gmail.com', '_MG_54474.JPG', '089662307895', 'Aktif', 'a42f37f00a27173d345578cc37799a1c'),
('9999900002', 'Amir', '2011-12-26', 'Majenang', 'Laki-laki', 'ISLAM', 'Bogor Cimanggu City', 'amirvanmahfud@gmail.com', '_MG_5420.jpg', '08951231233', 'Aktif', '869b0a539a7f82beca3b9bd48369ed6b'),
('9999900003', 'alip', '2011-02-21', 'PAPUA', 'Laki-laki', 'ISLAM', 'JL. Arjimar 2 RT 01/18 Tegal Gundil Bogor Utara', 'alif@gmail.com', '_MG_54492.JPG', '08971231231', 'Non Aktif', 'b483e444535cdbada34ebdb554f8382a'),
('9999900004', 'alul', '2011-12-26', 'Bogor', 'Laki-laki', 'ISLAM', 'Ciluer', 'sahrul24@gmail.com', '_MG_5448.jpg', '08981231231', 'Aktif', '287b71814a5613124a37294fd742a62c'),
('9999900005', 'dwi', '2011-02-21', 'BOGOR', 'Perempuan', 'ISLAM', 'JL. Arjimar 2 RT 01/18 Tegal Gundil Bogor Utara', 'dwi@gmail.com', '', '08571231231', 'Aktif', 'b6a28299a736ff0573d252a685f4ed21'),
('9999900006', 'mia', '2011-12-26', 'Majenang', 'Perempuan', 'ISLAM', 'Bogor Cimanggu City', 'mia@gmail.com', '', '08561231233', 'Aktif', '9a44343a97712ada761cda4e3cfb2dee'),
('9999900007', 'mei', '2011-02-21', 'PAPUA', 'Perempuan', 'ISLAM', 'JL. Arjimar 2 ', 'mei@gmail.com', '', '0851231233', 'Aktif', 'a29262a38934c3ecee49bc93f51a6ad9'),
('9999900008', 'bayu', '2011-12-26', 'Bogor', 'Laki-laki', 'ISLAM', 'Ciluer', 'bayu@gmail.com', '', '0891231288', 'Aktif', 'e6701703dde8c986db07093c84af719c'),
('9999900009', 'roni', '2011-02-21', 'BOGOR', 'Laki-laki', 'ISLAM', 'JL. Arjimar 2 ', 'roni@gmail.com', '', '08993818232', 'Aktif', 'f82ff3c136f6c2cbaf3707dc49e2decd'),
('9999900010', 'Kerry', '2011-12-26', 'Majenang', 'Laki-laki', 'ISLAM', 'Bogor Cimanggu City', 'kerry@gmail.com', '', '089612342', 'Aktif', 'a46927f510c9d93a33caee9781750de4'),
('9999900011', 'Chiko', '2011-02-21', 'PAPUA', 'Laki-laki', 'ISLAM', 'JL. Arjimar 2 ', 'chiko@gmail.com', '', '08968823573', 'Aktif', '14bc96b2c13dd7331562b01ce73e5967'),
('9999900012', 'ferdy', '2011-12-26', 'Bogor', 'Laki-laki', 'ISLAM', 'Ciluer', 'ferdy@gmail.com', '', '0896123711', 'Aktif', '2134778c294e189263e620b5ca88b199'),
('9999900014', 'fahmi', '2011-12-26', 'Majenang', 'Laki-laki', 'ISLAM', 'Bogor Cimanggu City', 'fahmi@gmail.com', '', '0898123231', 'Aktif', '3977966102e1f7e813b99c162cab4542'),
('9999900015', 'ipik', '2011-02-21', 'PAPUA', 'Laki-laki', 'ISLAM', 'JL. Arjimar 2 ', 'ipik@gmail.com', '', '08981237273', 'Aktif', '68b556d013ccfd11374afde4a9d967cc'),
('9999900016', 'Rehan', '2011-12-26', 'Bogor', 'Laki-laki', 'ISLAM', 'Ciluer', 'rehan@gmail.com', '', '0987272727', 'Aktif', 'ee20fdacc7c2f9aa567dbe3acaaaf631');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_absen`
--
ALTER TABLE `tbl_absen`
  ADD PRIMARY KEY (`kd_absen`),
  ADD KEY `kd_datakelas` (`kd_datakelas`);

--
-- Indexes for table `tbl_datakelassiswa`
--
ALTER TABLE `tbl_datakelassiswa`
  ADD PRIMARY KEY (`kd_datakelas`),
  ADD KEY `nis` (`nis`),
  ADD KEY `kd_kelas` (`kd_kelas`);

--
-- Indexes for table `tbl_kelas`
--
ALTER TABLE `tbl_kelas`
  ADD PRIMARY KEY (`kd_kelas`),
  ADD KEY `nip` (`nip`);

--
-- Indexes for table `tbl_mapel`
--
ALTER TABLE `tbl_mapel`
  ADD PRIMARY KEY (`kd_mapel`),
  ADD KEY `nip` (`nip`);

--
-- Indexes for table `tbl_nilai`
--
ALTER TABLE `tbl_nilai`
  ADD PRIMARY KEY (`kd_nilai`),
  ADD KEY `nis` (`nis`),
  ADD KEY `nip` (`nip`),
  ADD KEY `kd_kelas` (`kd_kelas`),
  ADD KEY `kd_mapel` (`kd_mapel`),
  ADD KEY `kd_datakelas` (`kd_datakelas`);

--
-- Indexes for table `tbl_pegawai`
--
ALTER TABLE `tbl_pegawai`
  ADD PRIMARY KEY (`nip`);

--
-- Indexes for table `tbl_siswa`
--
ALTER TABLE `tbl_siswa`
  ADD PRIMARY KEY (`nis`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_absen`
--
ALTER TABLE `tbl_absen`
  MODIFY `kd_absen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tbl_absen`
--
ALTER TABLE `tbl_absen`
  ADD CONSTRAINT `tbl_absen_ibfk_1` FOREIGN KEY (`kd_datakelas`) REFERENCES `tbl_datakelassiswa` (`kd_datakelas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tbl_datakelassiswa`
--
ALTER TABLE `tbl_datakelassiswa`
  ADD CONSTRAINT `tbl_datakelassiswa_ibfk_1` FOREIGN KEY (`kd_kelas`) REFERENCES `tbl_kelas` (`kd_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_datakelassiswa_ibfk_2` FOREIGN KEY (`nis`) REFERENCES `tbl_siswa` (`nis`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tbl_kelas`
--
ALTER TABLE `tbl_kelas`
  ADD CONSTRAINT `tbl_kelas_ibfk_1` FOREIGN KEY (`nip`) REFERENCES `tbl_pegawai` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tbl_mapel`
--
ALTER TABLE `tbl_mapel`
  ADD CONSTRAINT `tbl_mapel_ibfk_1` FOREIGN KEY (`nip`) REFERENCES `tbl_pegawai` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tbl_nilai`
--
ALTER TABLE `tbl_nilai`
  ADD CONSTRAINT `tbl_nilai_ibfk_1` FOREIGN KEY (`nis`) REFERENCES `tbl_siswa` (`nis`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_nilai_ibfk_2` FOREIGN KEY (`nip`) REFERENCES `tbl_pegawai` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_nilai_ibfk_3` FOREIGN KEY (`kd_kelas`) REFERENCES `tbl_kelas` (`kd_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_nilai_ibfk_4` FOREIGN KEY (`kd_mapel`) REFERENCES `tbl_mapel` (`kd_mapel`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_nilai_ibfk_5` FOREIGN KEY (`kd_datakelas`) REFERENCES `tbl_datakelassiswa` (`kd_datakelas`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
